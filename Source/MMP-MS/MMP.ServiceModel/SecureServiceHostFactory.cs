﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace MMP.ServiceModel
{
  /// <summary>
  ///   The <b>SecureServiceHostFactory</b> class implements a custom factory that provides
  ///   <b>ServiceHost</b> instances in managed hosting environments where the host instance is created
  ///   dynamically in response to incoming messages. The <b>ServiceHost</b> instances will be configured
  ///   to support the Web Service Security Model.
  /// </summary>
  [CLSCompliant(true)]
  public sealed class SecureServiceHostFactory : ServiceHostFactory
  {
    protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
    {
      return new SecureServiceHost(serviceType, baseAddresses);
    }
  }
}