﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Diagnostics;
using System.IdentityModel.Selectors;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Text;
using System.Xml;

namespace MMP.ServiceModel
{
  /// <summary>
  ///   The <b>SecureServiceClientFactory</b> is responsible for creating and initialising WCF-based web
  ///   service clients that can be used to invoke web services that have been secured using the
  ///   WS-Security or ClientSSL protocols.
  /// </summary>
  [CLSCompliant(true)]
  public static class SecureServiceClientFactory
  {
    static SecureServiceClientFactory()
    {
      // Note: The following switches were effective when using HTTP web services with .NET 2.0 and WSE
      //       http://basildoncoder.com/blog/2007/12/15/turbocharging-net-webservice-clients/
      ServicePointManager.Expect100Continue = false;
      ServicePointManager.CheckCertificateRevocationList = false;
      ServicePointManager.DefaultConnectionLimit = 10; // This is usually (2) by default
      ServicePointManager.UseNagleAlgorithm = false;
    }

    /// <summary>
    ///   Returns a secure service client that will use ClientSSL to securely invoke the service.
    /// </summary>
    /// <typeparam name="TClient">The WCF service client type for the service.</typeparam>
    /// <typeparam name="TInterface">The WCF service contract type for the service.</typeparam>
    /// <param name="serviceUri">The URI giving the service endpoint for the service.</param>
    /// <param name="clientCertificate">
    ///   The subject or thumbprint use to retrieve the client certificate from the
    ///   LocalMachine::TrustedPeople certificate store.
    /// </param>
    /// <returns>A secure service client that will use ClientSSL to securely invoke the service.</returns>
    public static TClient GetClientSslServiceClient<TClient, TInterface>(string serviceUri, string clientCertificate)
      where TClient : ClientBase<TInterface> where TInterface : class
    {
      try
      {
        // Cleanup the client certificate thumbprints removing any invalid non-printable characters 
        clientCertificate = CleanupCertificateName(clientCertificate);

        // Retrieve the client certificate from the LocalMachine::TrustedPeople certificate store
        var clientX509Certificate = GetCertificateFromStore(StoreLocation.LocalMachine, StoreName.TrustedPeople,
          clientCertificate);

        if (clientX509Certificate == null)
        {
          throw new SecureServiceClientException(
            $"Failed to find the client certificate with the subject or thumbprint ({clientCertificate}) in the" +
            " \"Trusted People\" certificate store for the \"Local Machine\"");
        }

        var uri = new Uri(serviceUri);

        // Construct the remote address for the service
        var remoteAddress = new EndpointAddress(uri);

        var binding = CreateClientSslBinding();

        // Create the client
        var constructorInfo = typeof (TClient).GetConstructor(new[] {typeof (Binding), typeof (EndpointAddress)});

        Debug.Assert(constructorInfo != null, "constructorInfo != null");
        var client = (TClient) constructorInfo.Invoke(new object[] {binding, remoteAddress});

        var clientCredentials = new ClientCredentials();
        clientCredentials.ClientCertificate.Certificate = clientX509Certificate;

        client.Endpoint.Behaviors.Clear();
        client.Endpoint.Behaviors.Add(clientCredentials);

        return client;
      }
      catch (Exception ex)
      {
        throw new SecureServiceClientException("Failed to create and initialise the secure service client", ex);
      }
    }

    /// <summary>Returns a service client that will insecurely invoke the service.</summary>
    /// <typeparam name="TClient">The WCF service client type for the service.</typeparam>
    /// <typeparam name="TInterface">The WCF service contract type for the service.</typeparam>
    /// <param name="serviceUri">The URI giving the service endpoint for the service.</param>
    /// <returns>A service client that will insecurely invoke the service.</returns>
    public static TClient GetInsecureServiceClient<TClient, TInterface>(string serviceUri)
      where TClient : ClientBase<TInterface> where TInterface : class
    {
      try
      {
        var uri = new Uri(serviceUri);

        var binding = new BasicHttpBinding();

        // Construct the remote address for the service
        var remoteAddress = new EndpointAddress(uri, EndpointIdentity.CreateDnsIdentity(uri.DnsSafeHost));

        // Create the client
        var constructorInfo = typeof (TClient).GetConstructor(new[] {typeof (Binding), typeof (EndpointAddress)});

        Debug.Assert(constructorInfo != null, "constructorInfo != null");
        var client = (TClient) constructorInfo.Invoke(new object[] {binding, remoteAddress});

        // Set the protection level
        client.Endpoint.Contract.ProtectionLevel = ProtectionLevel.None;

        foreach (var operation in client.Endpoint.Contract.Operations)
        {
          operation.ProtectionLevel = ProtectionLevel.None;

          foreach (var fault in operation.Faults)
          {
            fault.ProtectionLevel = ProtectionLevel.None;
          }
        }

        return client;
      }
      catch (Exception ex)
      {
        throw new SecureServiceClientException("Failed to create and initialise the service client", ex);
      }
    }

    /// <summary>
    ///   Returns a secure service client that will use WS-Security to securely invoke the service.
    /// </summary>
    /// <typeparam name="TClient">The WCF service client type for the service.</typeparam>
    /// <typeparam name="TInterface">The WCF service contract type for the service.</typeparam>
    /// <param name="serviceUri">The URI giving the service endpoint for the service.</param>
    /// <param name="serviceCertificate">
    ///   The subject or thumbprint use to retrieve the service certificate from the
    ///   LocalMachine::TrustedPeople certificate store.
    /// </param>
    /// <param name="clientCertificate">
    ///   The subject or thumbprint use to retrieve the client certificate from the
    ///   LocalMachine::TrustedPeople certificate store.
    /// </param>
    /// <returns>
    ///   A secure service client that will use WS-Security to securely invoke the service.
    /// </returns>
    public static TClient GetWsSecurityServiceClient<TClient, TInterface>(string serviceUri, string serviceCertificate,
      string clientCertificate) where TClient : ClientBase<TInterface> where TInterface : class
    {
      try
      {
        // Cleanup the certificate thumbprints removing any invalid non-printable characters 
        serviceCertificate = CleanupCertificateName(serviceCertificate);
        clientCertificate = CleanupCertificateName(clientCertificate);

        // Retrieve the service certificate from the LocalMachine::TrustedPeople certificate store
        var serviceX509Certificate = GetCertificateFromStore(StoreLocation.LocalMachine, StoreName.TrustedPeople,
          serviceCertificate);

        if (serviceX509Certificate == null)
        {
          throw new SecureServiceClientException(
            $"Failed to find the service certificate with the subject or thumbprint ({serviceCertificate}) in the" +
            " \"Trusted People\" certificate store for the \"Local Machine\"");
        }

        // Retrieve the client certificate from the LocalMachine::TrustedPeople certificate store
        var clientX509Certificate = GetCertificateFromStore(StoreLocation.LocalMachine, StoreName.TrustedPeople,
          clientCertificate);

        if (clientX509Certificate == null)
        {
          throw new SecureServiceClientException(
            $"Failed to find the client certificate with the subject or thumbprint ({clientCertificate}) in the" +
            " \"Trusted People\" certificate store for the \"Local Machine\"");
        }

        var uri = new Uri(serviceUri);

        var binding = CreateWsSecurityBinding(uri);

        // Construct the remote address for the service
        var dnsName = serviceX509Certificate.GetNameInfo(X509NameType.DnsName, false);
        Debug.Assert(dnsName != null, "dnsName != null");
        var remoteAddress = new EndpointAddress(uri, EndpointIdentity.CreateDnsIdentity(dnsName));

        // Create the client
        var constructorInfo = typeof (TClient).GetConstructor(new[] {typeof (Binding), typeof (EndpointAddress)});

        Debug.Assert(constructorInfo != null, "constructorInfo != null");
        var client = (TClient) constructorInfo.Invoke(new object[] {binding, remoteAddress});

        Debug.Assert(client.ClientCredentials != null, "client.ClientCredentials != null");

        client.ClientCredentials.ClientCertificate.Certificate = clientX509Certificate;

        // Setup the service credentials
        client.ClientCredentials.ServiceCertificate.DefaultCertificate = serviceX509Certificate;
        client.ClientCredentials.ServiceCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;
        client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
          X509CertificateValidationMode.PeerOrChainTrust;
        client.ClientCredentials.ServiceCertificate.Authentication.TrustedStoreLocation = StoreLocation.LocalMachine;

        // Set the protection level
        client.Endpoint.Contract.ProtectionLevel = ProtectionLevel.Sign;

        foreach (var operation in client.Endpoint.Contract.Operations)
        {
          operation.ProtectionLevel = ProtectionLevel.Sign;

          foreach (var fault in operation.Faults)
          {
            fault.ProtectionLevel = ProtectionLevel.Sign;
          }
        }

        return client;
      }
      catch (Exception ex)
      {
        throw new SecureServiceClientException("Failed to create and initialise the secure service client", ex);
      }
    }

    private static string CleanupCertificateName(string thumbprint)
    {
      var buffer = new StringBuilder();

      foreach (var t in thumbprint.Where(t => ((t >= 32) && (t <= 126))))
      {
        buffer.Append(t);
      }

      return buffer.ToString();
    }

    private static CustomBinding CreateClientSslBinding()
    {
      var binding = new CustomBinding();

      binding.Elements.Add(CreateEncoderBindingElement());

      var transportBindingElement = new HttpsTransportBindingElement
      {
        RequireClientCertificate = true,
        KeepAliveEnabled = true,
        MaxBufferSize = int.MaxValue,
        MaxReceivedMessageSize = int.MaxValue
      };

      binding.Elements.Add(transportBindingElement);

      var context = new BindingContext(binding, new BindingParameterCollection());

      var readerQuotas = context.GetInnerProperty<XmlDictionaryReaderQuotas>();

      if (readerQuotas != null)
      {
        readerQuotas.MaxStringContentLength = int.MaxValue;
        readerQuotas.MaxArrayLength = int.MaxValue;
        readerQuotas.MaxBytesPerRead = int.MaxValue;
      }

      return binding;
    }

    private static BindingElement CreateEncoderBindingElement()
    {
      return new TextMessageEncodingBindingElement(MessageVersion.Soap11, Encoding.UTF8);
    }

    // ReSharper disable once UnusedMember.Local
    private static CustomBinding CreateInsecureBinding(Uri uri)
    {
      var useHttps = (uri.Scheme.ToLower() == "https");

      var binding = new CustomBinding();

      binding.Elements.Add(CreateEncoderBindingElement());
      //binding.Elements.Add(CreateWSSecuritySecurityBindingElement());
      binding.Elements.Add(CreateInsecureTransportBindingElement(useHttps));

      var context = new BindingContext(binding, new BindingParameterCollection());

      var readerQuotas = context.GetInnerProperty<XmlDictionaryReaderQuotas>();

      if (readerQuotas != null)
      {
        readerQuotas.MaxStringContentLength = int.MaxValue;
        readerQuotas.MaxArrayLength = int.MaxValue;
        readerQuotas.MaxBytesPerRead = int.MaxValue;
      }

      return binding;
    }

    private static BindingElement CreateInsecureTransportBindingElement(bool useHttps)
    {
      HttpTransportBindingElement bindingElement;

      if (useHttps)
      {
        bindingElement = new HttpsTransportBindingElement();
        ((HttpsTransportBindingElement) bindingElement).RequireClientCertificate = false;
        //((HttpsTransportBindingElement) bindingElement).ExtendedProtectionPolicy =
        //  new ExtendedProtectionPolicy(PolicyEnforcement.Never);
      }
      else
      {
        bindingElement = new HttpTransportBindingElement();
      }

      bindingElement.KeepAliveEnabled = false;

      // By default enable large message support
      bindingElement.MaxBufferSize = int.MaxValue;
      bindingElement.MaxReceivedMessageSize = int.MaxValue;

      return bindingElement;
    }

    private static CustomBinding CreateWsSecurityBinding(Uri uri)
    {
      var useHttps = (uri.Scheme.ToLower() == "https");

      var binding = new CustomBinding();

      binding.Elements.Add(CreateEncoderBindingElement());
      binding.Elements.Add(CreateWsSecuritySecurityBindingElement());
      binding.Elements.Add(CreateWsSecurityTransportBindingElement(useHttps));

      var context = new BindingContext(binding, new BindingParameterCollection());

      var readerQuotas = context.GetInnerProperty<XmlDictionaryReaderQuotas>();

      if (readerQuotas != null)
      {
        readerQuotas.MaxStringContentLength = int.MaxValue;
        readerQuotas.MaxArrayLength = int.MaxValue;
        readerQuotas.MaxBytesPerRead = int.MaxValue;
      }

      return binding;
    }

    private static BindingElement CreateWsSecuritySecurityBindingElement()
    {
      // Create and set the security binding element for the binding
      var bindingElement =
        SecurityBindingElement.CreateMutualCertificateBindingElement(
          MessageSecurityVersion
            .WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10)
          as AsymmetricSecurityBindingElement;

      Debug.Assert(bindingElement != null, "bindingElement != null");

      bindingElement.MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;
      bindingElement.SetKeyDerivation(false);
      bindingElement.IncludeTimestamp = false;
      bindingElement.AllowSerializedSigningTokenOnReply = true;
      bindingElement.LocalClientSettings.DetectReplays = false;
      bindingElement.LocalServiceSettings.DetectReplays = false;
      //bindingElement.EnableUnsecuredResponse = true;
      //bindingElement.AllowInsecureTransport = true;

      return bindingElement;
    }

    private static BindingElement CreateWsSecurityTransportBindingElement(bool useHttps)
    {
      HttpTransportBindingElement bindingElement;

      if (useHttps)
      {
        bindingElement = new HttpsTransportBindingElement();
        ((HttpsTransportBindingElement) bindingElement).RequireClientCertificate = false;
      }
      else
      {
        bindingElement = new HttpTransportBindingElement();
      }

      bindingElement.KeepAliveEnabled = false;

      // By default enable large message support
      bindingElement.MaxBufferSize = int.MaxValue;
      bindingElement.MaxReceivedMessageSize = int.MaxValue;

      return bindingElement;
    }

    private static X509Certificate2 GetCertificateFromStore(StoreLocation storeLocation, StoreName storeName,
      string certificate)
    {
      if (certificate == null)
      {
        throw new ArgumentNullException(nameof(certificate), "certificate is null.");
      }

      var store = new X509Store(storeName, storeLocation);

      try
      {
        store.Open(OpenFlags.ReadOnly);

        foreach (var existingCertificate in store.Certificates)
        {
          if ((existingCertificate.Thumbprint == certificate) || (existingCertificate.SubjectName.Name == certificate))
          {
            if ((DateTime.Now < existingCertificate.NotBefore) || (DateTime.Now > existingCertificate.NotAfter))
            {
              throw new SecureServiceClientException(
                $"The service certificate ({certificate}) is invalid (Valid from {existingCertificate.NotBefore} to {existingCertificate.NotAfter})");
            }

            /* 
             * Choosing the first one and creating a new instance, the certificates in the original collection will be
             * reset below.
             */
            return new X509Certificate2(existingCertificate);
          }
        }
      }
      finally
      {
        foreach (var existingCertificate in store.Certificates)
        {
          existingCertificate.Reset();
        }

        store.Close();
      }

      return null;
    }
  }

  internal class NullCertificateValidator : X509CertificateValidator
  {
    public override void Validate(X509Certificate2 certificate)
    {
      //Debug.WriteLine("=========================> certificate.SubjectName = " + certificate.SubjectName.Name);
    }
  }
}