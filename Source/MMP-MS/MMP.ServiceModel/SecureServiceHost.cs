﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Diagnostics;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.ServiceModel.Security.Tokens;
using System.Text;
using System.Xml;
using Microsoft.Win32;

namespace MMP.ServiceModel
{
  /// <summary>
  ///   The <b>SecureServiceHost</b> class provides a custom <b>ServiceHost</b> that implements the Web
  ///   Services Security Model.
  /// </summary>
  [CLSCompliant(true)]
  public sealed class SecureServiceHost : ServiceHost
  {
    /// <summary>Constructs a new <b>SecureServiceHost</b>.</summary>
    /// <param name="contractType">The .NET type that defines the service contract.</param>
    /// <param name="serviceType">The .NET type that implements the service.</param>
    /// <param name="baseAddresses">The URIs for the addresses this service will be bound to.</param>
    public SecureServiceHost(Type contractType, Type serviceType, params Uri[] baseAddresses)
      : base(serviceType, baseAddresses)
    {
      // Perform the initialisation required when we are not hosting a WCF-based web service in IIS.

      if (Description.Endpoints.Count > 0)
      {
        throw new SecureServiceHostException(
          $"Failed to configure the secure service host for the service type ({serviceType.Name}):" +
          " Endpoints have already been configured using declarative configuration");
      }

      /*
       * Perform the initialisation required when we are hosting a WCF-based web service in IIS. 
       * In this case IIS will already have setup the endpoints using the configuration specified 
       * in the web.config configuration file. We only need to configure the endpoints so that they
       * suppport the Web Services Security Model.
       */
      var attributes = serviceType.GetCustomAttributes(typeof (SecureServiceAttribute), true);

      if (attributes.Length == 0)
      {
        throw new SecureServiceHostException(
          $"Failed to configure the secure service host for the service type ({serviceType.Name}):" +
          " No SecureService attribute was found");
      }

      var secureServiceAttribute = ((SecureServiceAttribute) attributes[0]);

      if (string.IsNullOrEmpty(secureServiceAttribute.ApplicationName))
      {
        throw new SecureServiceHostException(
          $"Failed to configure the secure service host for the service type ({serviceType.Name}):" +
          " No ApplicationName parameter for the SecureService attribute is invalid");
      }

      if (string.IsNullOrEmpty(secureServiceAttribute.ServiceName))
      {
        throw new SecureServiceHostException(
          $"Failed to configure the secure service host for the service type ({serviceType.Name}):" +
          " No ServiceName parameter for the SecureService attribute is invalid");
      }

      // Retrieve the service configuration
      var serviceConfiguration = GetServiceConfiguration(secureServiceAttribute.ProviderName,
        secureServiceAttribute.ApplicationName, secureServiceAttribute.ServiceName);

      try
      {
        foreach (var baseAddress in baseAddresses)
        {
          // Check the transport scheme
          var transportScheme = baseAddress.Scheme.ToLower();

          if ((transportScheme != "http") && (transportScheme != "https"))
          {
            throw new SecureServiceHostException(
              $"Unsupported transport scheme ({transportScheme}) for the endpoint ({baseAddress}) for the service type ({serviceType.Name})");
          }

          var binding = GetSecureServiceEndPointBinding(serviceConfiguration, baseAddress);

          var serviceEndpoint = AddServiceEndpoint(contractType, binding, baseAddress);

          // Set the protection level
          serviceEndpoint.Contract.ProtectionLevel = ProtectionLevel.Sign;

          foreach (var operation in serviceEndpoint.Contract.Operations)
          {
            operation.ProtectionLevel = ProtectionLevel.Sign;

            foreach (var fault in operation.Faults)
            {
              fault.ProtectionLevel = ProtectionLevel.Sign;
            }
          }
        }

        Credentials.ServiceCertificate.Certificate = serviceConfiguration.Certificate;

        Credentials.ClientCertificate.Authentication.CertificateValidationMode =
          X509CertificateValidationMode.PeerOrChainTrust;
        Credentials.ClientCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;
        Credentials.ClientCertificate.Authentication.TrustedStoreLocation = StoreLocation.LocalMachine;

        // Set the throttling behavior for the service
        var throttling = Description.Behaviors.Find<ServiceThrottlingBehavior>();
        if (throttling == null)
        {
          throttling = new ServiceThrottlingBehavior();
          Description.Behaviors.Add(throttling);
        }

        throttling.MaxConcurrentCalls = 4096;
        throttling.MaxConcurrentInstances = int.MaxValue;
        throttling.MaxConcurrentSessions = 4096;
      }
      catch (Exception ex)
      {
        throw new SecureServiceHostException(
          $"Failed to initialize the secure service host for the service type ({serviceType.Name})",
          ex);
      }
    }

    /// <summary>Constructs a new <b>SecureServiceHost</b>.</summary>
    /// <param name="serviceType">The .NET type that implements the service.</param>
    /// <param name="baseAddresses">The URIs for the addresses this service will be bound to.</param>
    public SecureServiceHost(Type serviceType, params Uri[] baseAddresses) : base(serviceType, baseAddresses)
    {
      if (Description.Endpoints.Count > 0)
      {
        throw new SecureServiceHostException(
          $"Failed to configure the secure service host for the service type ({serviceType.Name}):" +
          " Endpoints have already been configured using declarative configuration");
      }

      /*
       * Perform the initialisation required when we are hosting a WCF-based web service in IIS. 
       * In this case IIS will already have setup the endpoints using the configuration specified 
       * in the web.config configuration file. We only need to configure the endpoints so that they
       * suppport the Web Services Security Model.
       */
      var attributes = serviceType.GetCustomAttributes(typeof (SecureServiceAttribute), true);

      if (attributes.Length == 0)
      {
        throw new SecureServiceHostException(
          $"Failed to configure the secure service host for the service type ({serviceType.Name}):" +
          " No SecureService attribute was found");
      }

      var secureServiceAttribute = ((SecureServiceAttribute) attributes[0]);

      if (string.IsNullOrEmpty(secureServiceAttribute.ApplicationName))
      {
        throw new SecureServiceHostException(
          $"Failed to configure the secure service host for the service type ({serviceType.Name}):" +
          " No ApplicationName parameter for the SecureService attribute is invalid");
      }

      if (string.IsNullOrEmpty(secureServiceAttribute.ServiceName))
      {
        throw new SecureServiceHostException(
          $"Failed to configure the secure service host for the service type ({serviceType.Name}):" +
          " No ServiceName parameter for the SecureService attribute is invalid");
      }

      // Retrieve the service configuration
      var serviceConfiguration = GetServiceConfiguration(secureServiceAttribute.ProviderName,
        secureServiceAttribute.ApplicationName, secureServiceAttribute.ServiceName);

      try
      {
        foreach (var baseAddress in baseAddresses)
        {
          var relativeBaseAddress =
            baseAddress.MakeRelativeUri(
              (new UriBuilder(baseAddress.Scheme, baseAddress.Host, baseAddress.Port, "/")).Uri);

          // Check the transport scheme
          var transportScheme = baseAddress.Scheme.ToLower();

          if ((transportScheme != "http") && (transportScheme != "https"))
          {
            throw new SecureServiceHostException(
              $"Unsupported transport scheme ({transportScheme}) for the endpoint ({baseAddress}) for the service type ({serviceType.Name})");
          }

          var binding = GetSecureServiceEndPointBinding(serviceConfiguration, baseAddress);

          foreach (var serviceTypeInterface in serviceType.GetInterfaces())
          {
            if (IsServiceContract(serviceTypeInterface))
            {
              var serviceEndpoint = AddServiceEndpoint(serviceTypeInterface, binding, relativeBaseAddress);

              // Set the protection level
              serviceEndpoint.Contract.ProtectionLevel = ProtectionLevel.Sign;

              foreach (var operation in serviceEndpoint.Contract.Operations)
              {
                operation.ProtectionLevel = ProtectionLevel.Sign;

                foreach (var fault in operation.Faults)
                {
                  fault.ProtectionLevel = ProtectionLevel.Sign;
                }
              }
            }
          }
        }

        Credentials.ServiceCertificate.Certificate = serviceConfiguration.Certificate;

        Credentials.ClientCertificate.Authentication.CertificateValidationMode =
          X509CertificateValidationMode.ChainTrust;
        Credentials.ClientCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;
        Credentials.ClientCertificate.Authentication.TrustedStoreLocation = StoreLocation.LocalMachine;

        // Enable the debugging behaviour
        var serviceDebugBehavior = Description.Behaviors.Find<ServiceDebugBehavior>();
        if (serviceDebugBehavior == null)
        {
          serviceDebugBehavior = new ServiceDebugBehavior {IncludeExceptionDetailInFaults = true};
          Description.Behaviors.Add(serviceDebugBehavior);
        }
        else
        {
          serviceDebugBehavior.IncludeExceptionDetailInFaults = true;
        }

        // Set the throttling behavior for the service
        var throttling = Description.Behaviors.Find<ServiceThrottlingBehavior>();
        if (throttling == null)
        {
          throttling = new ServiceThrottlingBehavior();
          Description.Behaviors.Add(throttling);
        }

        throttling.MaxConcurrentCalls = 4096;
        throttling.MaxConcurrentInstances = int.MaxValue;
        throttling.MaxConcurrentSessions = 4096;

        // Enable AddressFilterMode.Any
        var serviceBehaviorAttribute = Description.Behaviors.Find<ServiceBehaviorAttribute>();
        if (serviceBehaviorAttribute != null)
        {
          serviceBehaviorAttribute.AddressFilterMode = AddressFilterMode.Any;
        }
      }
      catch (Exception ex)
      {
        throw new SecureServiceHostException(
          $"Failed to initialize the secure service host for the service type ({serviceType.Name})",
          ex);
      }
    }

    private static X509Certificate2 GetCertificateFromStore(StoreLocation storeLocation, StoreName storeName,
      string certificate)
    {
      if (certificate == null)
      {
        throw new ArgumentNullException(nameof(certificate), "certificate is null.");
      }

      var store = new X509Store(storeName, storeLocation);

      try
      {
        store.Open(OpenFlags.ReadOnly);

        foreach (var existingCertificate in store.Certificates)
        {
          if ((existingCertificate.Thumbprint == certificate) || (existingCertificate.SubjectName.Name == certificate))
          {
            if ((DateTime.Now < existingCertificate.NotBefore) || (DateTime.Now > existingCertificate.NotAfter))
            {
              throw new SecureServiceHostException(
                $"The service certificate ({certificate}) is invalid (Valid from {existingCertificate.NotBefore} to {existingCertificate.NotAfter})");
            }

            // Note: Choosing the first one and creating a new instance, the certificates in the original collection will be reset below.
            return new X509Certificate2(existingCertificate);
          }
        }
      }
      finally
      {
        foreach (var existingCertificate in store.Certificates)
        {
          existingCertificate.Reset();
        }

        store.Close();
      }

      return null;
    }

    private Binding GetSecureServiceEndPointBinding(ServiceConfiguration serviceConfiguration, Uri endPointUri)
    {
      try
      {
        var transportScheme = endPointUri.Scheme.ToLower();

        var binding = new CustomBinding {Name = serviceConfiguration.Name};

        // TODO: Enable support for compression -- MARCUS
        binding.Elements.Add(new TextMessageEncodingBindingElement(MessageVersion.Soap11, Encoding.UTF8));

        //if (serviceConfiguration.EnableCompression)
        //{
        //  binding.Elements.Add(
        //    new GZipMessageEncodingBindingElement(
        //      new TextMessageEncodingBindingElement(MessageVersion.Soap11, Encoding.UTF8)));
        //}
        //else
        //{
        //  binding.Elements.Add(new TextMessageEncodingBindingElement(MessageVersion.Soap11,
        //    Encoding.UTF8));
        //}

        // Create and set the security binding element for the binding
        var securityBindingElement =
          SecurityBindingElement.CreateMutualCertificateBindingElement(
            MessageSecurityVersion
              .WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10)
            as AsymmetricSecurityBindingElement;

        Debug.Assert(securityBindingElement != null, "securityBindingElement != null");
        securityBindingElement.MessageProtectionOrder = MessageProtectionOrder.SignBeforeEncrypt;
        securityBindingElement.SetKeyDerivation(false);
        securityBindingElement.IncludeTimestamp = false;
        securityBindingElement.AllowSerializedSigningTokenOnReply = true;
        securityBindingElement.LocalClientSettings.DetectReplays = false;
        securityBindingElement.LocalServiceSettings.DetectReplays = false;
        securityBindingElement.RecipientTokenParameters.InclusionMode = SecurityTokenInclusionMode.AlwaysToInitiator;

        binding.Elements.Add(securityBindingElement);

        // Setup the transport binding element
        HttpTransportBindingElement transportBindingElement = null;

        if (transportScheme == "http")
        {
          transportBindingElement = new HttpTransportBindingElement();
        }
        else if (transportScheme == "https")
        {
          transportBindingElement = new HttpsTransportBindingElement();
          ((HttpsTransportBindingElement) transportBindingElement).RequireClientCertificate = false;
        }

        Debug.Assert(transportBindingElement != null, "transportBindingElement != null");
        transportBindingElement.KeepAliveEnabled = serviceConfiguration.EnableKeepAlive;
        transportBindingElement.MaxBufferSize = int.MaxValue;
        transportBindingElement.MaxReceivedMessageSize = int.MaxValue;

        binding.Elements.Add(transportBindingElement);

        // TODO: Determine if we need this!!! - MARCUS
        var context = new BindingContext(binding, new BindingParameterCollection());
        var readerQuotas = context.GetInnerProperty<XmlDictionaryReaderQuotas>();
        if (readerQuotas != null)
        {
          readerQuotas.MaxStringContentLength = int.MaxValue;
          readerQuotas.MaxArrayLength = int.MaxValue;
          readerQuotas.MaxBytesPerRead = int.MaxValue;
        }

        // Enable the debugging behaviour
        var serviceDebugBehavior = Description.Behaviors.Find<ServiceDebugBehavior>();
        if (serviceDebugBehavior == null)
        {
          serviceDebugBehavior = new ServiceDebugBehavior {IncludeExceptionDetailInFaults = true};
          Description.Behaviors.Add(serviceDebugBehavior);
        }
        else
        {
          serviceDebugBehavior.IncludeExceptionDetailInFaults = true;
        }

        // Set the throttling behavior for the service
        var throttling = Description.Behaviors.Find<ServiceThrottlingBehavior>();
        if (throttling == null)
        {
          throttling = new ServiceThrottlingBehavior();
          Description.Behaviors.Add(throttling);
        }

        throttling.MaxConcurrentCalls = 4096;
        throttling.MaxConcurrentInstances = int.MaxValue;
        throttling.MaxConcurrentSessions = 4096;

        // Enable AddressFilterMode.Any
        var serviceBehaviorAttribute = Description.Behaviors.Find<ServiceBehaviorAttribute>();
        if (serviceBehaviorAttribute != null)
        {
          serviceBehaviorAttribute.AddressFilterMode = AddressFilterMode.Any;
        }

        return binding;
      }
      catch (Exception ex)
      {
        throw new SecureServiceHostException("Failed to configure the secure service HTTP endpoint", ex);
      }
    }

    private ServiceConfiguration GetServiceConfiguration(string providerName, string applicationName,
      string serviceName)
    {
      try
      {
        var serviceRegistryKeyName = $@"SOFTWARE\{providerName}\{applicationName}\Services\{serviceName}";

        using (var serviceRegistryKey = Registry.LocalMachine.OpenSubKey(serviceRegistryKeyName, true))
        {
          if (serviceRegistryKey == null)
          {
            throw new SecureServiceHostException(
              $"Failed to open the Registry key for the service ({serviceRegistryKeyName})");
          }

          var serviceCertificate = serviceRegistryKey.GetValue("ServiceCertificate") as string;

          var enableCompression = Convert.ToInt32(serviceRegistryKey.GetValue("EnableCompression")) == 1;

          var enableKeepAlive = Convert.ToInt32(serviceRegistryKey.GetValue("EnableKeepAlive")) == 1;

          if (string.IsNullOrEmpty(serviceCertificate))
          {
            throw new SecureServiceHostException(
              $"The service certificate thumbprint is invalid ({serviceCertificate})");
          }

          // Retrieve the service certificate from the LocalMachine::TrustedPeople certificate store
          var serviceX509Certificate = GetCertificateFromStore(StoreLocation.LocalMachine, StoreName.TrustedPeople,
            serviceCertificate);

          if (serviceX509Certificate == null)
          {
            throw new SecureServiceHostException(
              $"Failed to find the service certificate with the subject or thumbprint ({serviceCertificate}) in the \"Trusted People\" certificate store for the \"Local Machine\"");
          }

          return new ServiceConfiguration(serviceName, serviceCertificate, serviceX509Certificate, enableCompression,
            enableKeepAlive);
        }
      }
      catch (Exception ex)
      {
        throw new SecureServiceHostException(
          $"Failed to retrieve the configuration for the service {serviceName} for the application {applicationName}", ex);
      }
    }

    private bool IsServiceContract(Type type)
    {
      var attributes = type.GetCustomAttributes(typeof (ServiceContractAttribute), true);

      if (attributes.Length > 0)
      {
        return true;
      }
      return false;
    }
  }
}