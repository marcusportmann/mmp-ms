﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Security.Cryptography.X509Certificates;

namespace MMP.ServiceModel
{
  /// <summary>
  ///   The <b>ServiceConfiguration</b> class holds the configuration information for a service,
  ///   associated with a particular application, that is stored in the Windows Registry.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class ServiceConfiguration
  {
    private readonly X509Certificate2 _certificate;
    private readonly string _certificateSubjectOrThumbprint;
    private readonly bool _enableCompression;
    private readonly bool _enableKeepAlive;
    private readonly string _name;

    /// <summary>Constructs a new <b>ServiceConfiguration</b>.</summary>
    /// <param name="name">The name of the service.</param>
    /// <param name="certificateSubjectOrThumbprint">
    ///   The certificate subject or thumbprint identifying the services's certificate in the
    ///   LocalMachine::TrustedPeople X.509 certificate store.
    /// </param>
    /// <param name="certificate">The X.509 certificate for the service.</param>
    /// <param name="enableCompression">Should compression be enabled for the service?</param>
    /// <param name="enableKeepAlive">Should keep-alives be enabled for the service?</param>
    public ServiceConfiguration(string name, string certificateSubjectOrThumbprint, X509Certificate2 certificate,
      bool enableCompression, bool enableKeepAlive)
    {
      _name = name;
      _certificateSubjectOrThumbprint = certificateSubjectOrThumbprint;
      _certificate = certificate;
      _enableCompression = enableCompression;
      _enableKeepAlive = enableKeepAlive;
    }

    /// <summary>Returns the X.509 certificate for the service.</summary>
    public X509Certificate2 Certificate => _certificate;

    /// <summary>
    ///   The certificate subject or thumbprint identifying the services's certificate in the
    ///   LocalMachine::TrustedPeople X.509 certificate store.
    /// </summary>
    public string CertificateSubjectOrThumbprint => _certificateSubjectOrThumbprint;

    /// <summary>Should compression be enabled for the service?</summary>
    public bool EnableCompression => _enableCompression;

    /// <summary>Should keep-alives be enabled for the service?</summary>
    public bool EnableKeepAlive => _enableKeepAlive;

    /// <summary>The name of the service.</summary>
    public string Name => _name;
  }
}