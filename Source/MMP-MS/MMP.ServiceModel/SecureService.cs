﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IdentityModel.Claims;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;

namespace MMP.ServiceModel
{
  /// <summary>
  ///   The <b>SecureService</b> class provides the base class which all secure service classes should be
  ///   derived from.
  /// </summary>
  [CLSCompliant(true)]
  public abstract class SecureService
  {
    /// <summary>Gets the Distinguished Name (DN) of the caller invoking the service.</summary>
    /// <returns>
    ///   The Distinguished Name (DN) of the caller invoking the service or <b>null</b> if the identity of
    ///   the caller cannot be determined.
    /// </returns>
    protected X500DistinguishedName GetCallerDistinguishedName()
    {
      var authorizationContext = OperationContext.Current.ServiceSecurityContext.AuthorizationContext;

      if (authorizationContext == null)
        return null;

      return (from claimSet in authorizationContext.ClaimSets
        from claim in claimSet.FindClaims(ClaimTypes.X500DistinguishedName, Rights.PossessProperty)
        select claim.Resource as X500DistinguishedName).FirstOrDefault();
    }
  }
}

