﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Common.Utils
{
  /// <summary>
  ///   The <b>DateTimeExtensions</b> implements extension methods for the <b>DateTime</b> type.
  /// </summary>
  public static class DateTimeExtensions
  {
    private static readonly TimeSpan MILLISECOND = TimeSpan.FromMilliseconds(1);

    /// <summary>
    ///   Converts to UTC milliseconds (since epoch 1970-01-01 00:00:00) for compatibility with Java.
    /// </summary>
    /// <param name="dateTime">The <b>DateTime</b>.</param>
    /// <returns>The UTC Unix EPOCH time in milliseconds.</returns>
    public static long ToUnixTimeMilliseconds(this DateTime dateTime)
    {
      var timespan = (dateTime.ToUniversalTime() - DateTimeUtils.EPOCH);
      return (long) timespan.TotalMilliseconds;
    }

    /// <summary>Truncate the <b>DateTime</b> to the specified period of time.</summary>
    /// <param name="dateTime">The <b>DateTime</b> to truncate.</param>
    /// <param name="timeSpan">The <b>TimeSpan</b> to truncate to e.g. milliseconds, seconds, etc.</param>
    /// <returns>The truncated <b>DateTime</b>.</returns>
    public static DateTime Truncate(this DateTime dateTime, TimeSpan timeSpan)
    {
      return dateTime.AddTicks(-(dateTime.Ticks%timeSpan.Ticks));
    }

    /// <summary>Truncate the <b>DateTime</b> to milliseconds.</summary>
    /// <param name="dateTime">The <b>DateTime</b> to truncate.</param>
    /// <returns>The truncated <b>DateTime</b>.</returns>
    public static DateTime TruncateToMilliseconds(this DateTime dateTime)
    {
      return dateTime.AddTicks(-(dateTime.Ticks%MILLISECOND.Ticks));
    }
  }
}