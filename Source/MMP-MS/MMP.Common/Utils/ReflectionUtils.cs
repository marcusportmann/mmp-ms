﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Reflection;

namespace MMP.Common.Utils
{
  [CLSCompliant(true)]
  public static class ReflectionUtils
  {
    /// <summary>Returns a private field value using reflection.</summary>
    /// <typeparam name="T">The type of the field.</typeparam>
    /// <param name="obj">The object to retrieve the field for.</param>
    /// <param name="fieldName">The name of the field.</param>
    /// <returns>The value of the field.</returns>
    /// <exception cref="ArgumentOutOfRangeException">If the field is not found.</exception>
    public static T GetPrivateFieldValue<T>(this object obj, string fieldName)
    {
      if (obj == null)
        throw new ArgumentNullException(nameof(obj));
      var t = obj.GetType();
      FieldInfo fi = null;
      while (fi == null && t != null)
      {
        fi = t.GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        t = t.BaseType;
      }
      if (fi == null)
        throw new ArgumentOutOfRangeException(nameof(fieldName),
          $"Field {fieldName} was not found for Type {obj.GetType().FullName}");
      return (T) fi.GetValue(obj);
    }

    /// <summary>Returns a private property value using reflection.</summary>
    /// <typeparam name="T">The type of the property.</typeparam>
    /// <param name="obj">The object to retrieve the property for.</param>
    /// <param name="propertyName">The name of the property.</param>
    /// <returns>The value of the property.</returns>
    /// <exception cref="ArgumentOutOfRangeException">If the property is not found.</exception>
    public static T GetPrivatePropertyValue<T>(this object obj, string propertyName)
    {
      if (obj == null)
        throw new ArgumentNullException(nameof(obj));
      var pi = obj.GetType()
        .GetProperty(propertyName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
      if (pi == null)
        throw new ArgumentOutOfRangeException(nameof(propertyName),
          $"Property {propertyName} was not found for Type {obj.GetType().FullName}");
      return (T) pi.GetValue(obj, null);
    }

    /// <summary>Set a private field value using reflection.</summary>
    /// <typeparam name="T">The type of the field.</typeparam>
    /// <param name="obj">The object to set the field on.</param>
    /// <param name="fieldName">The name of the field.</param>
    /// <param name="val">The value to set.</param>
    /// <exception cref="ArgumentOutOfRangeException">If the field is not found.</exception>
    public static void SetPrivateFieldValue<T>(this object obj, string fieldName, T val)
    {
      if (obj == null)
        throw new ArgumentNullException(nameof(obj));
      var t = obj.GetType();
      FieldInfo fi = null;
      while (fi == null && t != null)
      {
        fi = t.GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        t = t.BaseType;
      }
      if (fi == null)
        throw new ArgumentOutOfRangeException(nameof(fieldName),
          $"Field {fieldName} was not found for Type {obj.GetType().FullName}");
      fi.SetValue(obj, val);
    }

    /// <summary>Set a private property value using reflection.</summary>
    /// <typeparam name="T">The type of the property</typeparam>
    /// <param name="obj">The object to set the property on.</param>
    /// <param name="propertyName">The name of the property.</param>
    /// <param name="val">The value to set.</param>
    /// <exception cref="ArgumentOutOfRangeException">If the property is not found.</exception>
    public static void SetPrivatePropertyValue<T>(this object obj, string propertyName, T val)
    {
      var t = obj.GetType();
      if (t.GetProperty(propertyName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance) == null)
        throw new ArgumentOutOfRangeException(nameof(propertyName),
          $"Property {propertyName} was not found for Type {obj.GetType().FullName}");
      t.InvokeMember(propertyName,
        BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.SetProperty | BindingFlags.Instance, null, obj,
        new object[] {val});
    }
  }
}