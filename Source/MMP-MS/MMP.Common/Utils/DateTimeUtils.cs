/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Common.Utils
{
  /// <summary>
  ///   The <b>DateTimeUtils</b> class provides utility methods related to the manipulation of dates and
  ///   times.
  /// </summary>
  [CLSCompliant(true)]
  public sealed class DateTimeUtils
  {
    /// <summary>The UTC Unix EPOCH time in milliseconds.</summary>
    public static readonly DateTime EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

    /// <summary>
    ///   Converts from UTC milliseconds (since epoch 1970-01-01 00:00:00) to local time for compatibility
    ///   with Java.
    /// </summary>
    /// <param name="unixTime">The UTC Unix EPOCH time in milliseconds.</param>
    /// <returns>The equivalent DateTime adjusted to local time.</returns>
    public static DateTime FromUnixTimeMilliseconds(long unixTime)
    {
      return EPOCH.AddMilliseconds(unixTime).ToLocalTime();
    }

    /// <summary>
    ///   Converts to UTC milliseconds (since epoch 1970-01-01 00:00:00) for compatibility with Java.
    /// </summary>
    /// <param name="dateTime">The local date and time.</param>
    /// <returns>The UTC Unix EPOCH time in milliseconds.</returns>
    public static long ToUnixTimeMilliseconds(DateTime dateTime)
    {
      var timespan = (dateTime.ToUniversalTime() - EPOCH);
      return (long) timespan.TotalMilliseconds;
    }
  }
}