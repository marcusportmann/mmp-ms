﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MMP.Common.Crypto
{
  /// <summary>The <b>CryptUtils</b> class provides cryptography related utility functions.</summary>
  [CLSCompliant(true)]
  public sealed class CryptoUtils
  {
    /// <summary>The length of the 128-bit AES KEY.</summary>
    public static readonly int AES128_BIT_KEY_LENGTH = 16;

    /// <summary>The secure random number generator.</summary>
    private static readonly RandomNumberGenerator RANDOM_NUMBER_GENERATOR = new RNGCryptoServiceProvider();

    /// <summary>Create a 128-bit AES key.</summary>
    /// <returns>The 128-bit AES key.</returns>
    public static byte[] Create128BitAesKey()
    {
      var key = new byte[AES128_BIT_KEY_LENGTH];

      RANDOM_NUMBER_GENERATOR.GetBytes(key);

      return key;
    }

    /// <summary>Create an AES key.</summary>
    /// <param name="keySize">The key size in bytes.</param>
    /// <returns>The AES key.</returns>
    public static byte[] CreateAesKey(int keySize)
    {
      var key = new byte[keySize];

      RANDOM_NUMBER_GENERATOR.GetBytes(key);

      return key;
    }

    /// <summary>Create a random AES IV.</summary>
    /// <returns>The random 128-bit AES IV.</returns>
    public static byte[] CreateRandom128BitAesIv()
    {
      var iv = new byte[AES128_BIT_KEY_LENGTH];

      RANDOM_NUMBER_GENERATOR.GetBytes(iv);

      return iv;
    }

    /// <summary>Create a random AES IV.</summary>
    /// <param name="keySize">The key size in bytes.</param>
    /// <returns>The random AES IV.</returns>
    public static byte[] CreateRandomAesIv(int keySize)
    {
      var iv = new byte[keySize];

      RANDOM_NUMBER_GENERATOR.GetBytes(iv);

      return iv;
    }

    /// <summary>Perform AES CFB encryption or decryption.</summary>
    /// <param name="key">The AES key.</param>
    /// <param name="iv">The initialisation vector.</param>
    /// <param name="data">The data to encrypt or decrypt.</param>
    /// <param name="decrypt">
    ///   <b>True</b> if the data should be encrypted or <b>False</b> if the data should be encrypted.
    /// </param>
    /// <returns>The encrypted or decrypted data.</returns>
    /// <exception cref="MMP.Common.Crypto.CryptoException"></exception>
    public static byte[] DoAesCfb(byte[] key, byte[] iv, byte[] data, bool decrypt)
    {
      if ((key == null) || ((key.Length%128) != 0))
      {
        throw new CryptoException("Invalid AES key");
      }

      if ((iv == null) || (iv.Length != key.Length))
      {
        throw new CryptoException("Invalid initialisation vector");
      }

      if (data == null)
      {
        throw new CryptoException("Invalid data");
      }

      if (decrypt)
      {
        // Create a RijndaelManaged object with the specified key and IV
        using (var aesAlg = new RijndaelManaged())
        {
          aesAlg.KeySize = key.Length*8;
          aesAlg.Mode = CipherMode.CFB;
          aesAlg.Padding = PaddingMode.None;
          aesAlg.FeedbackSize = 8;

          using (var decryptor = aesAlg.CreateDecryptor(key, iv))
          {
            using (var encryptedStream = new MemoryStream(data))
            {
              using (var crypto = new CryptoStream(encryptedStream, decryptor, CryptoStreamMode.Read))
              {
                var decryptedData = new byte[data.Length];

                crypto.Read(decryptedData, 0, decryptedData.Length);

                return decryptedData;
              }
            }
          }
        }
      }

      // Create a RijndaelManaged object with the specified key and IV
      using (var aesAlg = new RijndaelManaged())
      {
        aesAlg.KeySize = key.Length*8;
        aesAlg.Mode = CipherMode.CFB;
        aesAlg.Padding = PaddingMode.None;
        aesAlg.FeedbackSize = 8;

        // Create an encrytor to perform the stream transform
        using (var encryptor = aesAlg.CreateEncryptor(key, iv))
        {
          // Create the streams used for encryption.
          using (var encryptedStream = new MemoryStream())
          {
            using (var cryptoStream = new CryptoStream(encryptedStream, encryptor, CryptoStreamMode.Write))
            {
              // Write all data to the stream to encrypt it
              cryptoStream.Write(data, 0, data.Length);

              // Return the encrypted bytes from the memory stream
              return encryptedStream.ToArray();
            }
          }
        }
      }
    }

    /// <summary>
    ///   Convert the specified password to a 128-bit AES key that can be used with the AES cypher encrypt
    ///   and decrypt functions.
    /// </summary>
    /// <param name="password">The password to convert to an AES key.</param>
    /// <returns>The AES key.</returns>
    /// <exception cref="MMP.Common.Crypto.CryptoException"></exception>
    public static byte[] PasswordTo128BitAesKey(string password)
    {
      var salt = Encoding.UTF8.GetBytes("301bbca6-9471-412e-8933-79a9e3d9e06b");

      return PasswordToAesKey(Encoding.UTF8.GetBytes(password), salt, 4, AES128_BIT_KEY_LENGTH);
    }

    /// <summary>
    ///   Convert the specified password to an AES key that can be used with the AES cypher encrypt and
    ///   decrypt functions.
    /// </summary>
    /// <param name="password">The password to convert to an AES key.</param>
    /// <param name="salt">The salt.</param>
    /// <param name="iterations">The number of SHA-1 iterations to perform when generating the key.</param>
    /// <param name="keySize">The key size in bytes.</param>
    /// <returns>The AES key.</returns>
    /// <exception cref="MMP.Common.Crypto.CryptoException"></exception>
    public static byte[] PasswordToAesKey(byte[] password, byte[] salt, int iterations, int keySize)
    {
      if ((password.Length + salt.Length) < keySize)
      {
        throw new CryptoException("Failed to generate the AES key from the password: " +
                                  "The combination of the password length and the salt length " +
                                  "are less than the key length");
      }

      try
      {
        // Concatenate password and salt.
        var pwAndSalt = new byte[password.Length + salt.Length];

        Array.Copy(password, 0, pwAndSalt, 0, password.Length);
        Array.Copy(salt, 0, pwAndSalt, password.Length, salt.Length);


        SHA1 sha1 = new SHA1CryptoServiceProvider();

        // Create the key as sha1(sha1(sha1(sha1(...(pw+salt))...)
        for (var i = 0; i < iterations; i++)
        {
          var hash = sha1.ComputeHash(pwAndSalt);

          Array.Copy(hash, pwAndSalt, hash.Length);
        }

        var key = new byte[keySize];

        Array.Copy(pwAndSalt, 0, key, 0, keySize);

        return key;
      }
      catch (Exception ex)
      {
        throw new CryptoException("Failed to convert the password to an AES key", ex);
      }
    }
  }
}