﻿using System;
using System.Runtime.Serialization;

namespace MMP.Common.Persistence
{
  /// <summary>
  ///   The <b>IDGeneratorException</b> exception is thrown to indicate an error condition when working
  ///   with the
  ///   <b>IDGenerator</b>.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class IDGeneratorException : Exception
  {
    /// <summary>Constructs a new <b>IDGeneratorException</b> with the default message.</summary>
    public IDGeneratorException() : this("IDGenerator Error")
    {
    }

    /// <summary>Constructs a new <b>IDGeneratorException</b> using serialization.</summary>
    /// <param name="info">The serialization info.</param>
    /// <param name="context">The streaming context.</param>
    public IDGeneratorException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    /// <summary>
    ///   Constructs a new <b>IDGeneratorException</b> with the default message and specified cause.
    /// </summary>
    /// <param name="cause">The cause saved for later retrieval via the <b>InnerException</b> property.</param>
    public IDGeneratorException(Exception cause) : this("IDGenerator Error", cause)
    {
    }

    /// <summary>Constructs a new <b>IDGeneratorException</b> with the specified message.</summary>
    /// <param name="message">The message saved for later retrieval via <b>Message</b> property.</param>
    public IDGeneratorException(string message) : this(message, null)
    {
    }

    /// <summary>
    ///   Constructs a new <b>IDGeneratorException</b> with the specified message and cause.
    /// </summary>
    /// <param name="message">The message saved for later retrieval via the <b>Message</b> property.</param>
    /// <param name="cause">The cause saved for later retrieval via the <b>InnerException</b> property.</param>
    public IDGeneratorException(string message, Exception cause) : base(message, cause)
    {
    }
  }
}