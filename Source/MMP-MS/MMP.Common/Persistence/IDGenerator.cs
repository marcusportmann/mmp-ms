﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MMP.Common.Persistence
{
  /// <summary>
  ///   The <b>IDGenerator</b> class provides unique ID sequences for entities in the database.
  /// </summary>
  [CLSCompliant(true)]
  public sealed class IDGenerator
  {
    /// <summary>The database connection string.</summary>
    private readonly string _connectionString;

    /// <summary>Constructs a new <b>IDGenerator</b>.</summary>
    /// <param name="connectionString">The database connection string.</param>
    public IDGenerator(string connectionString)
    {
      _connectionString = connectionString;
    }

    /// <summary>Get the next unique <b>long</b> ID for the sequence with the specified name.</summary>
    /// <param name="name">The name of the sequence to retrieve the next ID for.</param>
    /// <returns>The next unique <b>long</b> ID for the sequence with the specified name.</returns>
    public long Next(string name)
    {
      using (var connection = new SqlConnection(_connectionString))
      {
        SqlTransaction transaction = null;

        try
        {
          connection.Open();

          transaction = connection.BeginTransaction();

          var id = GetCurrentId(connection, transaction, name);

          if (id == -1)
          {
            id = 1;

            using (
              var command =
                new SqlCommand("INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES (@Name, @Current)",
                  connection, transaction))
            {
              command.Parameters.Add("Name", SqlDbType.NVarChar).Value = name;
              command.Parameters.Add("Current", SqlDbType.BigInt).Value = id;

              if (command.ExecuteNonQuery() != 1)
              {
                throw new IDGeneratorException(
                  $"The incorrect number of rows were affected while inserting the ID for the sequence ({name}) into the [MMP].[IdGenerator] table");
              }
            }
          }
          else
          {
            id = id + 1;

            using (
              var command = new SqlCommand(
                "UPDATE [MMP].[IdGenerator] SET [Current] = @Current WHERE [Name] = @Name", connection, transaction))
            {
              command.Parameters.Add("Current", SqlDbType.BigInt).Value = id;
              command.Parameters.Add("Name", SqlDbType.NVarChar).Value = name;

              if (command.ExecuteNonQuery() != 1)
              {
                throw new IDGeneratorException(
                  $"The incorrect number of rows were affected while updating the ID for the sequence ({name}) in the [MMP].[IdGenerator] table");
              }
            }
          }

          transaction.Commit();

          return id;
        }
        catch (Exception ex)
        {
          transaction?.Rollback();

          throw new IDGeneratorException($"Failed to retrieve the next unique ID for the sequence ({name})", ex);
        }
      }
    }

    private long GetCurrentId(SqlConnection connection, SqlTransaction transaction, string name)
    {
      using (
        var command = new SqlCommand("SELECT [Current] FROM [MMP].[IdGenerator] WITH (UPDLOCK, INDEX([PK_MMP_IdGenerator_Name])) WHERE [Name] = @Name", connection, transaction))
      {
        command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = name;

        using (var reader = command.ExecuteReader())
        {
          if (reader.Read())
          {
            return reader.GetInt64(0);
          }
          return -1;
        }
      }
    }
  }
}