﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Data.SqlClient;

namespace MMP.Common.Persistence
{
  /// <summary>
  ///   The <b>DAO</b> class provides the base class from which all data access object (DAOs) classes
  ///   should be derived.
  /// </summary>
  public abstract class DAO
  {
    private readonly string _connectionString;

    /// <summary>Constructs a new <b>DAO</b>.</summary>
    /// <param name="connectionString">The ADO.NET connection string.</param>
    protected DAO(string connectionString)
    {
      _connectionString = connectionString;
    }

    /// <summary>Returns a new SQL connection for the DAO.</summary>
    /// <returns>The new SQL connection.</returns>
    protected SqlConnection GetConnection()
    {
      return new SqlConnection(_connectionString);
    }
  }
}