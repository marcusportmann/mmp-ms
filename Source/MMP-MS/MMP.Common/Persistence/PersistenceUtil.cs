﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace MMP.Common.Persistence
{
  /// <summary>The <b>PersistenceUtil</b> class provides persistence related utility methods.</summary>
  public class PersistenceUtil
  {
    /// <summary>Close the SQL connection.</summary>
    /// <param name="connection">The SQL connection to close.</param>
    public static void Close(SqlConnection connection)
    {
      try
      {
        connection?.Close();
      }
      catch (Exception)
      {
        // ignored
      }
    }

    /// <summary>Create a LocalDB database.</summary>
    /// <param name="databaseDirectory">The directory for the LocalDB database files.</param>
    /// <param name="databaseName">The name of the LocalDB database.</param>
    public static void CreateLocalDbDatabase(string databaseDirectory, string databaseName)
    {
      try
      {
        var startTime = DateTime.Now;

        using (
          var connection =
            new SqlConnection(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=Master;Integrated Security=True"))
        {
          connection.Open();

          var command = new SqlCommand {Connection = connection, CommandText = string.Format(@"
	          IF EXISTS(SELECT * FROM sys.databases WHERE name='{1}')
	            BEGIN
		            ALTER DATABASE [{1}]
		            SET SINGLE_USER
		            WITH ROLLBACK IMMEDIATE
		            DROP DATABASE [{1}]
	            END

	          DECLARE @FILENAME AS VARCHAR(255)

	          SET @FILENAME = '{0}\{1}';

	          EXEC ('CREATE DATABASE [{1}] ON PRIMARY (
		          NAME = [{1}], 
		          FILENAME =''' + @FILENAME + ''', 
		          SIZE = 25MB, 
		          MAXSIZE = 50MB, 
		          FILEGROWTH = 5MB )')", databaseDirectory, databaseName)};

          command.ExecuteNonQuery();
        }

        var elapsedTime = DateTime.Now - startTime;

        Debug.WriteLine($@"Created the LocalDB database {databaseName} in {elapsedTime.TotalSeconds} seconds");
      }
      catch (Exception ex)
      {
        throw new Exception($"Failed to create the LocalDB database ({databaseName})", ex);
      }
    }

    /// <summary>Drop a LocalDB database.</summary>
    /// <param name="databaseName">The name of the LocalDB database.</param>
    public static void DropLocalDbDatabase(string databaseName)
    {
      Debug.WriteLine($@"Dropping the LocalDB database {databaseName}");

      try
      {
        using (
          var connection =
            new SqlConnection(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=Master;Integrated Security=True"))
        {
          connection.Open();

          var command = new SqlCommand {Connection = connection, CommandText = string.Format(@"
	          IF EXISTS(SELECT * FROM sys.databases WHERE name='{0}')
	            BEGIN
		            ALTER DATABASE [{0}]
		            SET SINGLE_USER
		            WITH ROLLBACK IMMEDIATE
		            DROP DATABASE [{0}]
	            END", databaseName)};

          command.ExecuteNonQuery();
        }
      }
      catch (Exception ex)
      {
        throw new Exception($"Failed to drop the LocalDB database ({databaseName})", ex);
      }
    }

    /// <summary>
    ///   Initialise the LocalDB database using the SQL statements retrieved from the resource stream.
    /// </summary>
    /// <param name="databaseName">The name of the LocalDB database to initialise.</param>
    /// <param name="resourceStream">The resource stream to retrieve the SQL statements from.</param>
    public static void InitLocalDbDatabase(string databaseName, Stream resourceStream)
    {
      if (resourceStream == null)
      {
        throw new Exception("Failed to initialise the LocalDB database using the invalid resource stream");
      }

      var sqlStatements = LoadSQL(resourceStream);

      using (
        var connection =
          new SqlConnection(
            $@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog={databaseName};Integrated Security=SSPI;"))
      {
        connection.Open();

        var startTime = DateTime.Now;

        foreach (var sqlStatement in sqlStatements)
        {
          try
          {
            if (!sqlStatement.Trim().Equals("GO", StringComparison.CurrentCultureIgnoreCase))
            {
              var command = new SqlCommand(sqlStatement + ";", connection);

              command.ExecuteNonQuery();
            }
          }
          catch (Exception ex)
          {
            throw new Exception(
              $@"Failed to execute the SQL statement for the LocalDB database {databaseName}: {sqlStatement}", ex);
          }
        }

        var elapsedTime = DateTime.Now - startTime;

        Debug.WriteLine($@"The LocalDB database {databaseName} was initialised in {elapsedTime.TotalSeconds} seconds");
      }
    }

    /// <summary>Load the SQL statements from the specified stream.</summary>
    /// <param name="stream">The stream to load the SQL statements from.</param>
    /// <returns>The SQL statements loaded from the stream.</returns>
    public static List<string> LoadSQL(Stream stream)
    {
      var sqlStatements = new List<string>();

      if (stream == null)
      {
        return sqlStatements;
      }

      using (var streamReader = new StreamReader(stream))
      {
        StringBuilder multiLineBuffer = null;
        string line;

        while ((line = streamReader.ReadLine()) != null)
        {
          if (line.Contains("/*"))
          {
            while ((line = streamReader.ReadLine()) != null)
            {
              if (line.Contains("*/"))
              {
                line = line.Substring(line.IndexOf("*/", StringComparison.Ordinal) + 2);
                break;
              }
            }

            if (line == null)
            {
              break;
            }
          }

          /*
           * Remove any whitespace at the beginning or end of the line, any newline characters and
           * any multiple spaces.
           */
          line = CleanSQL(line);

          if (line.Length == 0)
          {
            continue;
          }

          // Only process the line if it is not a SQL comment
          if (line.StartsWith("--"))
          {
            continue;
          }

          if (line.Equals("GO", StringComparison.CurrentCultureIgnoreCase))
          {
            continue;
          }

          /*
           * If we have already built up part of the multi-line SQL statement then add spacing
           * between this and the next part of the SQL statement on the current line.
           */
          multiLineBuffer?.Append(" ");

          /*
           * If the line contains a ';' then one of the following is true:
           * - The line contains one or more single-line SQL statements
           * - The line contains the end of a multi-line SQL statement
           * - The line contains both of the above
           */
          if (line.Contains(";"))
          {
            var tokens = line.Split(';');

            for (var i = 0; i < tokens.Length; i++)
            {
              var token = tokens[i].Trim();

              // If we are currently processing a multi-line buffer
              if (multiLineBuffer != null)
              {
                multiLineBuffer.Append(token);
                sqlStatements.Add(multiLineBuffer.ToString());
                multiLineBuffer = null;
              }
              else
              {
                if (i < (tokens.Length - 1))
                {
                  sqlStatements.Add(token);
                }
                else
                {
                  if (line.EndsWith(";"))
                  {
                    sqlStatements.Add(token);
                  }
                  else
                  {
                    multiLineBuffer = new StringBuilder();
                    multiLineBuffer.Append(token);
                  }
                }
              }
            }
          }
          /*
           * The line does not contain the end of a SQL statement which means it is either
           * the start of a new multi-line SQL statement or the continuation of an existing
           * multi-line SQL statement.
           */
          else
          {
            /*
             * If this is a new multi-line SQL statement then initialise the buffer
             * that will be used to concatenate the individual lines of the statement into
             * a single-line SQL statement.
             */
            if (multiLineBuffer == null)
            {
              multiLineBuffer = new StringBuilder();
            }

            multiLineBuffer.Append(line);
          }
        }

        return sqlStatements;
      }
    }

    /// <summary>Clean the SQL text.</summary>
    /// <param name="text">The SQL text to clean.</param>
    /// <returns>The cleaned SQL text.</returns>
    private static string CleanSQL(string text)
    {
      if (text == null)
      {
        return "";
      }

      text = text.Trim();

      if (text.Length == 0)
      {
        return text;
      }

      // First remove the new line characters
      var index = text.Length - 1;

      while ((index >= 0) && ((text[index] == '\r') || (text[index] == '\n')))
      {
        index--;
      }

      if (index < 0)
      {
        return "";
      }

      text = text.Substring(0, index + 1);

      // Replace multiple spaces with a single space
      while (text.Contains("  "))
      {
        text = text.Replace("  ", " ");
      }

      // Replace tabs with a single space
      text = text.Replace("\t", " ");

      return text;
    }
  }
}