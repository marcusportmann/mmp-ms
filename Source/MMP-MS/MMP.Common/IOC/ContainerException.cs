﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Runtime.Serialization;

namespace MMP.Common.IOC
{
  /// <summary>
  ///   The <b>ContainerException</b> exception is thrown to indicate an error condition when working
  ///   with the IOC container.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class ContainerException : Exception
  {
    /// <summary>Constructs a new <b>ContainerException</b> with the default message.</summary>
    public ContainerException() : this("Container Error")
    {
    }

    /// <summary>Constructs a new <b>ContainerException</b> using serialization.</summary>
    /// <param name="info">The serialization info.</param>
    /// <param name="context">The streaming context.</param>
    public ContainerException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    /// <summary>
    ///   Constructs a new <b>ContainerException</b> with the default message and specified cause.
    /// </summary>
    /// <param name="cause">The cause saved for later retrieval via the <b>InnerException</b> property.</param>
    public ContainerException(Exception cause) : this("Container Error", cause)
    {
    }

    /// <summary>Constructs a new <b>ContainerException</b> with the specified message.</summary>
    /// <param name="message">The message saved for later retrieval via <b>Message</b> property.</param>
    public ContainerException(string message) : this(message, null)
    {
    }

    /// <summary>Constructs a new <b>ContainerException</b> with the specified message and cause.</summary>
    /// <param name="message">The message saved for later retrieval via the <b>Message</b> property.</param>
    /// <param name="cause">The cause saved for later retrieval via the <b>InnerException</b> property.</param>
    public ContainerException(string message, Exception cause) : base(message, cause)
    {
    }
  }
}