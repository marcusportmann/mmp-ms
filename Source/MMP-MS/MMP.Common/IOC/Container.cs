﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using Microsoft.Practices.Unity;

namespace MMP.Common.IOC
{
  /// <summary>The <b>Container</b> class is a static wrapper around the Unity Container.</summary>
  [CLSCompliant(true)]
  public static class Container
  {
    /// <summary>The shared Unity Container.</summary>
    public static readonly IUnityContainer UNITY_CONTAINER = new UnityContainer();

    /// <summary>The default resolver overrides;</summary>
    private static readonly ResolverOverride[] RESOLVER_OVERRIDES = {};

    /// <summary>Statically initialise the <b>Container</b>.</summary>
    static Container()
    {
    }

    public static T BuildUp<T>(T instance, string name)
    {
      lock (UNITY_CONTAINER)
      {
        return (T) UNITY_CONTAINER.BuildUp(instance.GetType(), instance, name, RESOLVER_OVERRIDES);
      }
    }

    public static T BuildUp<T>(T instance)
    {
      return BuildUp(instance, null);
    }

    public static bool CanResolve<T>(string name)
    {
      try
      {
        UNITY_CONTAINER.Resolve<T>(name);

        return true;
      }
      catch (ResolutionFailedException)
      {
        return false;
      }
    }

    public static bool CanResolve(Type type)
    {
      try
      {
        UNITY_CONTAINER.Resolve(type);

        return true;
      }
      catch (ResolutionFailedException)
      {
        return false;
      }
    }

    /// <summary>Register an instance with the container.</summary>
    /// <remarks>
    ///   <para>
    ///     Instance registration is much like setting a type as a singleton, except that instead of the
    ///     container creating the instance the first time it is requested, the user creates the instance
    ///     ahead of time and adds that instance to the container.
    ///   </para>
    /// </remarks>
    /// <param name="type">
    ///   The type of instance to register (may be an implemented interface instead of the full type).
    /// </param>
    /// <param name="instance">The instance to register.</param>
    /// <param name="name">The name of the instance being registered.</param>
    /// <param name="lifetime">
    ///   <see cref="T:Microsoft.Practices.Unity.LifetimeManager" /> object that controls how this instance
    ///   will be managed by the container.
    /// </param>
    /// <returns>
    ///   The <see cref="T:Microsoft.Practices.Unity.SharedUnityContainer" /> object that this method was
    ///   called on.
    /// </returns>
    public static IUnityContainer RegisterInstance(Type type, string name, object instance, LifetimeManager lifetime)
    {
      return UNITY_CONTAINER.RegisterInstance(type, name, instance, lifetime);
    }

    /// <summary>Register an instance with the container.</summary>
    /// <remarks>
    ///   <para>
    ///     Instance registration is much like setting a type as a singleton, except that instead of the
    ///     container creating the instance the first time it is requested, the user creates the instance
    ///     ahead of time and adds that instance to the container.
    ///   </para>
    /// </remarks>
    /// <param name="type">
    ///   The type of instance to register (may be an implemented interface instead of the full type).
    /// </param>
    /// <param name="instance">The instance to register.</param>
    /// <param name="name">The name of the instance being registered.</param>
    /// <returns>
    ///   The <see cref="T:Microsoft.Practices.Unity.SharedUnityContainer" /> object that this method was
    ///   called on.
    /// </returns>
    public static IUnityContainer RegisterInstance(Type type, string name, object instance)
    {
      return UNITY_CONTAINER.RegisterInstance(type, name, instance, new ContainerControlledLifetimeManager());
    }

    /// <summary>Register an instance with the container using the name of the type.</summary>
    /// <remarks>
    ///   <para>
    ///     Instance registration is much like setting a type as a singleton, except that instead of the
    ///     container creating the instance the first time it is requested, the user creates the instance
    ///     ahead of time and adds that instance to the container.
    ///   </para>
    /// </remarks>
    /// <param name="type">
    ///   The type of instance to register (may be an implemented interface instead of the full type).
    /// </param>
    /// <param name="instance">The instance to register.</param>
    /// <param name="lifetime">
    ///   <see cref="T:Microsoft.Practices.Unity.LifetimeManager" /> object that controls how this instance
    ///   will be managed by the container.
    /// </param>
    /// <returns>
    ///   The <see cref="T:Microsoft.Practices.Unity.SharedUnityContainer" /> object that this method was
    ///   called on.
    /// </returns>
    public static IUnityContainer RegisterInstance(Type type, object instance, LifetimeManager lifetime)
    {
      return UNITY_CONTAINER.RegisterInstance(type, null, instance, lifetime);
    }

    /// <summary>Register an instance with the container using the name of the type.</summary>
    /// <remarks>
    ///   <para>
    ///     Instance registration is much like setting a type as a singleton, except that instead of the
    ///     container creating the instance the first time it is requested, the user creates the instance
    ///     ahead of time and adds that instance to the container.
    ///   </para>
    /// </remarks>
    /// <param name="type">
    ///   The type of instance to register (may be an implemented interface instead of the full type).
    /// </param>
    /// <param name="instance">The instance to register.</param>
    /// <returns>
    ///   The <see cref="T:Microsoft.Practices.Unity.SharedUnityContainer" /> object that this method was
    ///   called on.
    /// </returns>
    public static IUnityContainer RegisterInstance(Type type, object instance)
    {
      return UNITY_CONTAINER.RegisterInstance(type, null, instance, new ContainerControlledLifetimeManager());
    }

    public static T Resolve<T>(string name)
    {
      return UNITY_CONTAINER.Resolve<T>(name);
    }

    public static T Resolve<T>()
    {
      return UNITY_CONTAINER.Resolve<T>();
    }

    /// <summary>
    ///   Resolve the initial dependencies for the instances registered with the IOC container.
    /// </summary>
    public static void ResolveInitialDependencies()
    {
      foreach (var registration in UNITY_CONTAINER.Registrations)
      {
        var containerControlledLifetimeManager = registration.LifetimeManager as ContainerControlledLifetimeManager;

        if (containerControlledLifetimeManager?.GetValue() != null)
        {
          try
          {
            //Debug.WriteLine(string.Format(
            //    "Resolving the initial dependencies for the registered type ({0}) mapped to the instance ({1})",
            //    registration.RegisteredType,
            //    containerControlledLifetimeManager.GetValue().GetType()));

            BuildUp(containerControlledLifetimeManager.GetValue());
          }
          catch (Exception ex)
          {
            throw new ContainerException(
              $"Failed to resolve the initial dependencies for the registered type ({registration.RegisteredType}) mapped to the instance ({containerControlledLifetimeManager.GetValue().GetType()})", ex);
          }
        }
      }
    }
  }
}