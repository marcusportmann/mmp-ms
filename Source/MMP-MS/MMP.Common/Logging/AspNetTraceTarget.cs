﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Web;
using NLog;
using NLog.Targets;

namespace MMP.Common.Logging
{
  /// <summary>
  ///   The <b>AspNetTraceTarget</b> class provides an <b>NLog</b> target that writes to the ASP.NET
  ///   trace log.
  /// </summary>
  [Target("AspNetTrace")]
  [CLSCompliant(true)]
  public sealed class AspNetTraceTarget : TargetWithLayout
  {
    /// <summary>Write the <b>LogEvent</b> to the target.</summary>
    /// <param name="logEvent">The logging event.</param>
    protected override void Write(LogEventInfo logEvent)
    {
      var context = HttpContext.Current;

      if (context == null)
      {
        return;
      }

      if (logEvent.Level >= LogLevel.Warn)
      {
        context.Trace.Warn(logEvent.LoggerName, Layout.Render(logEvent));
      }
      else
      {
        context.Trace.Write(logEvent.LoggerName, Layout.Render(logEvent));
      }
    }
  }
}