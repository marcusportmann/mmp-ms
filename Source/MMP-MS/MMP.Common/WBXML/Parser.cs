﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;
using System.Text;

namespace MMP.Common.WBXML
{
  /// <summary>
  ///   The <b>Parser</b> class generates a WBXML object hierarchy from the binary data representation of
  ///   a WBXML document.
  /// </summary>
  [CLSCompliant(true)]
  public sealed class Parser
  {
    /// <summary>Parse the specified binary data representation of the WBXML document.</summary>
    /// <param name="data">The binary data representation of the WBXML document.</param>
    /// <returns>The WBXML object hierarchy.</returns>
    /// <exception cref="MMP.Common.WBXML.ParserException"></exception>
    public Document Parse(byte[] data)
    {
      // Local variables

      // Initialise the input stream
      using (var stream = new MemoryStream(data))
      {
        // Read the version number
        int version;
        if ((version = ReadByte(stream)) != WBXML.WBXML_VERSION)
        {
          throw new ParserException("Invalid WBXML version: " + version);
        }

        // Read the well known public identifier and check whether it is supported
        int publicId;
        if ((publicId = ReadByte(stream)) != Document.PUBLIC_ID_UNKNOWN)
        {
          throw new ParserException("Unknown document public identifier: " + publicId);
        }

        // Read the character set and check whether it is supported
        int charset;
        if ((charset = ReadMultiByteUint32(stream)) != WBXML.CHARSET_UTF_8)
        {
          throw new ParserException("Unsupported character set: " + charset);
        }

        // Read the length of the string table
        var stringTableLength = ReadMultiByteUint32(stream);

        // If we have a string table then read the entire thing and store it for later use
        byte[] stringTable;

        if (stringTableLength > 0)
        {
          stringTable = new byte[stringTableLength];
          stream.Read(stringTable, 0, stringTableLength);
        }
        else
        {
          stringTable = new byte[0];
        }

        // Create the root element
        var rootElement = new Element();

        int tmpValue;
        while ((tmpValue = stream.ReadByte()) != -1)
        {
          switch (tmpValue)
          {
            case WBXML.TOKEN_SWITCH_PAGE:
            {
              throw new ParserException("Unsupported token (TOKEN_SWITCH_PAGE");
            }

            case WBXML.TOKEN_PI:
            {
              throw new ParserException("Unsupported token (TOKEN_PI");
            }

            case WBXML.TOKEN_END:
            {
              throw new ParserException("Unexpected token: TOKEN_END");
            }

            default:
            {
              ParseElement(stream, stringTable, tmpValue, rootElement);

              break;
            }
          }
        }

        return new Document(rootElement);
      }
    }

    private void ParseAttributes(MemoryStream stream, byte[] stringTable, Element element)
    {
      // Read the first byte detailing the format of the attribute
      var tmpValue = ReadByte(stream);

      while (tmpValue != WBXML.TOKEN_END)
      {
        // We can only handle literal attributes
        if (tmpValue != WBXML.TOKEN_LITERAL)
        {
          throw new ParserException("Unsupported attribute tag identity (" + tmpValue + ")");
        }

        // Read the offset into the string table for the name of this attribute
        var attributeNameOffset = ReadMultiByteUint32(stream);

        // Read the name of the attribute from the string table
        string attributeName;

        if ((attributeName = ReadFromStringTable(stringTable, attributeNameOffset)) == null)
        {
          throw new ParserException("Invalid string table offset for attribute name (" + attributeNameOffset + ")");
        }

        // Read the byte giving the type of value for this attribute
        tmpValue = ReadByte(stream);

        // We can only handle inline string attribute values
        if (tmpValue != WBXML.TOKEN_STR_I)
        {
          throw new ParserException("Unsupported attribute value identity (" + tmpValue + ")");
        }

        // Read the inline string
        var attributeValue = ReadString(stream);

        // Set the attribute
        element.SetAttribute(attributeName, attributeValue);

        // Read the next byte. This will either be the end of the attributes or
        // alternatively the start of a new attribute
        tmpValue = ReadByte(stream);
      }
    }

    private void ParseContent(MemoryStream stream, byte[] stringTable, Element element)
    {
      // int result = 0;

      // Read the first byte detailing the format of the content
      while (true)
      {
        var tmpValue = ReadByte(stream);

        switch (tmpValue)
        {
          case WBXML.TOKEN_END:
          {
            // End of content under this element
            return;
          }

          case WBXML.TOKEN_ENTITY:
          {
            // NOTE: We do not process the entity token
            var entityLength = ReadMultiByteUint32(stream);

            var currentPos = stream.Position;

            if (stream.Seek(entityLength, SeekOrigin.Current) != (currentPos + entityLength))
            {
              throw new IOException("Unexpected EOF while skipping entity with length (" + entityLength + ")");
            }

            break;
          }

          case WBXML.TOKEN_STR_I:
          {
            // Read the inline string and add it as text content to the element
            element.AddContent(ReadString(stream));

            break;
          }

          case WBXML.TOKEN_STR_T:
          {
            // Read the index into the string table
            var stringTableOffset = ReadMultiByteUint32(stream);

            // Read the string from the string table and add as text content to the element
            element.AddContent(ReadFromStringTable(stringTable, stringTableOffset));

            break;
          }

          case WBXML.TOKEN_OPAQUE:
          {
            // Read the length of the opaque data
            var opaqueLength = ReadMultiByteUint32(stream);

            // Read the opaque data
            element.AddContent(ReadOpaque(stream, opaqueLength));

            break;
          }

          case WBXML.TOKEN_EXT_I_0:
            throw new ParserException("Unsupported token (TOKEN_EXT_I_0)");

          case WBXML.TOKEN_EXT_I_1:
            throw new ParserException("Unsupported token (TOKEN_EXT_I_1)");

          case WBXML.TOKEN_EXT_I_2:
            throw new ParserException("Unsupported token (TOKEN_EXT_I_2)");

          case WBXML.TOKEN_EXT_0:
            throw new ParserException("Unsupported token (TOKEN_EXT_0)");

          case WBXML.TOKEN_EXT_1:
            throw new ParserException("Unsupported token (TOKEN_EXT_1)");

          case WBXML.TOKEN_EXT_2:
            throw new ParserException("Unsupported token (TOKEN_EXT_2)");

          case WBXML.TOKEN_EXT_T_0:
            throw new ParserException("Unsupported token (TOKEN_EXT_T_0)");

          case WBXML.TOKEN_EXT_T_1:
            throw new ParserException("Unsupported token (TOKEN_EXT_T_1)");

          case WBXML.TOKEN_EXT_T_2:
            throw new ParserException("Unsupported token (TOKEN_EXT_T_2)");

          case WBXML.TOKEN_PI:
            throw new ParserException("Unsupported token (TOKEN_PI)");

          default:
          {
            var childElement = new Element();

            ParseElement(stream, stringTable, tmpValue, childElement);
            element.AddContent(childElement);

            break;
          }
        }
      }
    }

    private void ParseElement(MemoryStream stream, byte[] stringTable, int token, Element element)
    {
      // Local variables
      // int result = 0;
      var hasAttributes = (token & 0x80) > 0;
      var hasContent = (token & 0x40) > 0;
      var elementIdentity = token & 0x3F;

      // We only support unknown tags with an offset into the string table
      if (elementIdentity != 0x04)
      {
        throw new ParserException("Unsupported element tag identity (" + elementIdentity + ")");
      }

      // Read the offset into the string table for the element name
      var elementNameOffset = ReadMultiByteUint32(stream);

      // Read the element name from the string table
      string elementName;

      if ((elementName = ReadFromStringTable(stringTable, elementNameOffset)) == null)
      {
        throw new ParserException("Invalid string table offset for element name (" + elementNameOffset + ")");
      }

      element.Name = elementName;

      // If we have attributes parse them now
      if (hasAttributes)
      {
        ParseAttributes(stream, stringTable, element);
      }

      // If we have content parse it now
      if (hasContent)
      {
        ParseContent(stream, stringTable, element);
      }
      else
      {
        if (ReadByte(stream) != WBXML.TOKEN_END)
        {
          throw new ParserException("Missing token after element without content (TOKEN_END)");
        }
      }
    }

    private byte ReadByte(MemoryStream stream)
    {
      var value = stream.ReadByte();

      if (value == -1)
      {
        throw new ParserException("EOF while parsing the WBXML");
      }

      return (byte) value;
    }

    private string ReadFromStringTable(byte[] stringTable, int offset)
    {
      var index = offset;

      while (index < stringTable.Length)
      {
        if (stringTable[index] == 0)
        {
          return Encoding.UTF8.GetString(stringTable, offset, index - offset);
        }

        index++;
      }

      throw new ParserException("String exceeds string table at offset (" + offset + ")");
    }

    private int ReadMultiByteUint32(MemoryStream stream)
    {
      var result = 0;
      int i;

      do
      {
        i = ReadByte(stream);
        result = (result << 7) | (i & 0x7f);
      }
      while ((i & 0x80) != 0);

      return result;
    }

    private byte[] ReadOpaque(MemoryStream stream, int length)
    {
      var data = new byte[length];

      if (stream.Read(data, 0, length) != length)
      {
        throw new ParserException("EOF while parsing the WBXML and reading opaque data");
      }

      return data;
    }

    private string ReadString(MemoryStream stream)
    {
      using (var ms = new MemoryStream())
      {
        while (true)
        {
          var b = ReadByte(stream);

          if (b == 0)
          {
            return Encoding.UTF8.GetString(ms.ToArray());
          }

          ms.WriteByte(b);
        }
      }
    }
  }
}