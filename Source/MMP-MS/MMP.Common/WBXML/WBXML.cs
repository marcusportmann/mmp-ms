﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Common.WBXML
{
  /// <summary>The <b>WBXML</b> class contains the WBXML constants.</summary>
  [CLSCompliant(true)]
  public sealed class WBXML
  {
    /// <summary>The ID for the ISO 8895-1 character set.</summary>
    public const int CHARSET_ISO8859_1 = 0x04;

    /// <summary>The ID for the UTF-16 character set.</summary>
    public const int CHARSET_UTF_16 = 0x03F7;

    /// <summary>The ID for the UTF-8 character set.</summary>
    public const int CHARSET_UTF_8 = 106;

    public const string ENCODING_UTF_8 = "utf-8";

    /// <summary>The value signifying an end token.</summary>
    public const int TOKEN_END = 0x01;

    /// <summary>The value signifying an entity token.</summary>
    public const int TOKEN_ENTITY = 0x02;

    public const int TOKEN_EXT_0 = 0xC0;

    public const int TOKEN_EXT_1 = 0xC1;

    public const int TOKEN_EXT_2 = 0xC2;

    public const int TOKEN_EXT_I_0 = 0x40;

    public const int TOKEN_EXT_I_1 = 0x41;

    public const int TOKEN_EXT_I_2 = 0x42;

    public const int TOKEN_EXT_T_0 = 0x80;

    public const int TOKEN_EXT_T_1 = 0x81;

    public const int TOKEN_EXT_T_2 = 0x82;

    public const int TOKEN_LITERAL = 0x04;

    public const int TOKEN_LITERAL_A = 0x84;

    public const int TOKEN_LITERAL_AC = 0xC4;

    public const int TOKEN_LITERAL_C = 0x44;

    /// <summary>The value signifying an opaque content token.</summary>
    public const int TOKEN_OPAQUE = 0xC3;

    public const int TOKEN_PI = 0x43;

    public const int TOKEN_STR_I = 0x03;

    public const int TOKEN_STR_T = 0x83;

    /// <summary>The value signifying a switch page token.</summary>
    public const int TOKEN_SWITCH_PAGE = 0x00;

    /// <summary>The supported WBXML version.</summary>
    public const int WBXML_VERSION = 0x01;
  }
}