﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MMP.Common.WBXML
{
  /// <summary>
  ///   The <b>Element</b> class stores the data for a WBXML element content type in a WBXML document.
  ///   <para>This content type represents a node in the WBXML document.</para>
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class Element : Content
  {
    private readonly IList<Attribute> _attributes = new List<Attribute>();
    private readonly IList<Content> _content = new List<Content>();

    /// <summary>Constructs an <b>Element</b> with the specified name.</summary>
    /// <param name="name">The name of the element.</param>
    public Element(string name)
    {
      Name = name;
    }

    /// <summary>Constructs an <b>Element</b> with the specified name and content.</summary>
    /// <param name="name">The name of the element.</param>
    /// <param name="content">The content for the element.</param>
    public Element(string name, byte[] content)
    {
      Name = name;

      AddContent(content);
    }

    /// <summary>Constructs an <b>Element</b> with the specified name and content.</summary>
    /// <param name="name">The name of the element.</param>
    /// <param name="content">The content for the element.</param>
    public Element(string name, string content)
    {
      Name = name;

      AddContent(content);
    }

    /// <summary>
    ///   Constructs an <b>Element</b>.
    ///   <para>NOTE: This default constructor is hidden.</para>
    /// </summary>
    internal Element()
    {
      Name = "";
    }

    /// <summary>The attributes for this element.</summary>
    public IList<Attribute> Attributes => _attributes;

    /// <summary>The child elements for this element.</summary>
    public IList<Element> Children
    {
      get { return _content.OfType<Element>().Select(content => content).ToList(); }
    }

    /// <summary>The content items for this element.</summary>
    public IList<Content> Content => _content;

    /// <summary>The name of the element.</summary>
    public string Name { get; set; }

    /// <summary>The binary data content for the element.</summary>
    public byte[] Opaque
    {
      get
      {
        if (_content.Count == 1)
        {
          var opaque = _content[0] as Opaque;
          if (opaque != null)
          {
            return opaque.Data;
          }
        }

        var buffer = new BinaryBuffer();

        foreach (var content in _content)
        {
          if (content is Opaque)
          {
            var opaque = content as Opaque;

            buffer.Append(opaque.Data);
          }
        }

        return buffer.Data;
      }
    }

    /// <summary>The text content for the element.</summary>
    public string Text
    {
      get
      {
        if (_content.Count == 1)
        {
          var cdata = _content[0] as CDATA;
          if (cdata != null)
          {
            return cdata.Text;
          }
        }

        var buffer = new StringBuilder();

        foreach (var content in _content)
        {
          if (content is CDATA)
          {
            var cdata = content as CDATA;

            buffer.Append(cdata.Text);
          }
        }

        return buffer.ToString();
      }
    }

    /// <summary>Add the binary content to the element as an <b>Opaque</b> content type instance.</summary>
    /// <param name="data">The binary content to add to the element.</param>
    public void AddContent(byte[] data)
    {
      _content.Add(new Opaque(data));
    }

    /// <summary>Add the specified element as a child element to this element.</summary>
    /// <param name="element">The child element.</param>
    public void AddContent(Element element)
    {
      _content.Add(element);
    }

    /// <summary>Add the text content to the element as a <b>CDATA</b> content type instance.</summary>
    /// <param name="text">The text content.</param>
    public void AddContent(string text)
    {
      _content.Add(new CDATA(text));
    }

    /// <summary>Returns the attribute with the specified name.</summary>
    /// <param name="name">The name of the attribute.</param>
    /// <returns>
    ///   The attribute with the specified name or <b>null</b> if no matching attribute could be found.
    /// </returns>
    public Attribute GetAttribute(string name)
    {
      return _attributes.FirstOrDefault(attribute => attribute.Name == name);
    }

    /// <summary>Returns the value for the attribute with the specified name.</summary>
    /// <param name="name">The name of the attribute.</param>
    /// <returns>
    ///   The value for the attribute with the specified name or <b>null</b> if no matching attribute could
    ///   be found.
    /// </returns>
    public string GetAttributeValue(string name)
    {
      return (from attribute in _attributes where attribute.Name == name select attribute.Value).FirstOrDefault();
    }

    /// <summary>Returns the child element with the specified name.</summary>
    /// <param name="name">The name of the child element.</param>
    /// <returns>
    ///   The child element or <b>null</b> if a child element with the specified name could not be found.
    /// </returns>
    public Element GetChild(string name)
    {
      return _content.OfType<Element>().Select(content => content).FirstOrDefault(element => element.Name == name);
    }

    /// <summary>Returns the binary data content for the child element with the specified name.</summary>
    /// <param name="name">The name of the child element.</param>
    /// <returns>
    ///   The binary data content for the child element or <b>null</b> if a child element with the
    ///   specified name could not be found.
    /// </returns>
    public byte[] GetChildOpaque(string name)
    {
      return (from content in _content.OfType<Element>()
        select content
        into element
        where element.Name == name
        select element.Opaque).FirstOrDefault();
    }

    /// <summary>Returns the list of child elements with the specified name for this element.</summary>
    /// <param name="name">The name of the child elements to return.</param>
    /// <returns>The list of child elements with the specified name.</returns>
    public IList<Element> GetChildren(string name)
    {
      return _content.OfType<Element>().Select(content => content).Where(element => element.Name == name).ToList();
    }

    /// <summary>Returns the text content for the child element with the specified name.</summary>
    /// <param name="name">The name of the child element.</param>
    /// <returns>
    ///   The text content for the child element or <b>null</b> if a child element with the specified name
    ///   could not be found.
    /// </returns>
    public string GetChildText(string name)
    {
      return
        (from content in _content.OfType<Element>()
          select content
          into element where element.Name == name select element.Text).FirstOrDefault();
    }

    /// <summary>
    ///   Returns <b>True</b> if the element has an attribute with the specified name or
    ///   <b>False</b> otherwise.
    /// </summary>
    /// <param name="name">The name of the attribute.</param>
    /// <returns>
    ///   <b>True</b> if the element has an attribute with the specified name or
    ///   <b>False</b> otherwise.
    /// </returns>
    public bool HasAttribute(string name)
    {
      return _attributes.Any(attribute => attribute.Name == name);
    }

    /// <summary>Returns <b>True</b> if the element has attributes or <b>False</b> otherwise.</summary>
    /// <returns>
    ///   <b>True</b> if the element has attributes or <b>False</b> otherwise.
    /// </returns>
    public bool HasAttributes()
    {
      return (_attributes.Count > 0);
    }

    /// <summary>
    ///   Returns <b>True</b> if the element has a child element with the specified name or
    ///   <b>False</b> otherwise.
    /// </summary>
    /// <param name="name">The name of the child element.</param>
    /// <returns>
    ///   <b>True</b> if the element has a child element with the specified name or <b>False</b>
    ///   otherwise.
    /// </returns>
    public bool HasChild(string name)
    {
      return _content.OfType<Element>().Select(content => content).Any(element => element.Name == name);
    }

    /// <summary>Returns <b>True</b> if the element has children or <b>False</b> otherwise.</summary>
    /// <returns>
    ///   <b>True</b> if the element has children or <b>False</b> otherwise.
    /// </returns>
    public bool HasChildren()
    {
      return _content.OfType<Element>().Any();
    }

    /// <summary>Returns <b>True</b> if the element has content or <b>False</b> otherwise.</summary>
    /// <returns>
    ///   <b>True</b> if the element has content or <b>False</b> otherwise.
    /// </returns>
    public bool HasContent()
    {
      return (_content.Count > 0);
    }

    /// <summary>
    ///   Print the content to the specified <b>TextWriter</b> using the specified indent level.
    /// </summary>
    /// <param name="writer">The <b>TextWriter</b> to output the content with.</param>
    /// <param name="indent">The indent level.</param>
    public override void Print(TextWriter writer, int indent)
    {
      var indentBuffer = new StringBuilder();

      for (var i = 0; i < indent; i++)
      {
        indentBuffer.Append(INDENT);
      }

      writer.Write(indentBuffer);
      writer.Write("<" + Name);

      if (HasAttributes())
      {
        foreach (var attribute in _attributes)
        {
          writer.Write(" " + attribute.Name + "=\"" + attribute.Value + "\"");
        }
      }

      if (HasContent())
      {
        writer.WriteLine(">");

        foreach (var content in _content)
        {
          content.Print(writer, indent + 1);
        }

        writer.Write(indentBuffer);
        writer.WriteLine("</" + Name + ">");
      }
      else
      {
        writer.WriteLine("/>");
      }
    }

    /// <summary>Remove the attribute with the specified name.</summary>
    /// <param name="name">The name of the attribute.</param>
    public void RemoveAttribute(string name)
    {
      for (var i = 0; i < _attributes.Count; i++)
      {
        if (_attributes[i].Name == name)
        {
          _attributes.RemoveAt(i);
          return;
        }
      }
    }

    /// <summary>Set the value of the attribute with the specified name for the element.</summary>
    /// <param name="name">The name of the attribute.</param>
    /// <param name="value">The new value for the attribute.</param>
    public void SetAttribute(string name, string value)
    {
      foreach (var t in _attributes)
      {
        if (t.Name == name)
        {
          t.Value = value;
          return;
        }
      }

      _attributes.Add(new Attribute(name, value));
    }

    /// <summary>Set the binary data content for the element.</summary>
    /// <param name="data">The binary data content for the element.</param>
    public void SetOpaque(byte[] data)
    {
      var hasRemoved = true;

      while (hasRemoved)
      {
        hasRemoved = false;

        for (var i = 0; i < _content.Count; i++)
        {
          if (_content[i] is Opaque)
          {
            hasRemoved = true;
            _content.RemoveAt(i);
            break;
          }
        }
      }

      _content.Add(new Opaque(data));
    }

    /// <summary>Set the text content for the element.</summary>
    /// <param name="text">The text content for the element.</param>
    public void SetText(string text)
    {
      var hasRemoved = true;

      while (hasRemoved)
      {
        hasRemoved = false;

        for (var i = 0; i < _content.Count; i++)
        {
          if (_content[i] is CDATA)
          {
            hasRemoved = true;
            _content.RemoveAt(i);
            break;
          }
        }
      }

      _content.Add(new CDATA(text));
    }
  }
}