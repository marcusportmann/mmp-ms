﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;
using System.Xml.Linq;

namespace MMP.Common.WBXML
{
  /// <summary>The <b>Document</b> class represents a WBXML document.</summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class Document
  {
    /// <summary>The Public ID for an Unknown WBXML document type.</summary>
    public const int PUBLIC_ID_UNKNOWN = 0x01;

    private int _publicId = PUBLIC_ID_UNKNOWN;

    /// <summary>Constructs a new <b>Document</b> with the specified root elememt.</summary>
    /// <param name="element">The root element for the document.</param>
    public Document(Element element)
    {
      RootElement = element;
    }

    /// <summary>
    ///   Constructs a new <b>Document</b> by extracting the information from the specified XML root
    ///   elememt.
    /// </summary>
    /// <param name="element">The root element for the document.</param>
    public Document(XElement element)
    {
      RootElement = ToWBXML(element);
    }

    /// <summary>The public ID for the document.</summary>
    public int PublicId
    {
      get { return _publicId; }

      set { _publicId = value; }
    }

    /// <summary>The root element for the document.</summary>
    public Element RootElement { get; set; }

    /// <summary>Print the document.</summary>
    public void Print()
    {
      RootElement.Print(0);
    }

    /// <summary>Print the document to the specified <b>Stream</b>.</summary>
    /// <param name="writer">The <b>TextWriter</b> to output the document to.</param>
    public void Print(TextWriter writer)
    {
      RootElement.Print(writer, 0);
    }

    /// <summary>Returns the XML representation of the WBXML document.</summary>
    /// <returns>The XML representation of the WBXML document.</returns>
    public XElement ToXml()
    {
      return ToXml(RootElement);
    }

    private Element ToWBXML(XElement xmlElement)
    {
      var element = new Element(xmlElement.Name.LocalName);

      foreach (var attribute in xmlElement.Attributes())
      {
        element.SetAttribute(attribute.Name.LocalName, attribute.Value);
      }

      if (xmlElement.Value.Length > 0)
      {
        element.AddContent(xmlElement.Value);
      }

      foreach (var childElement in xmlElement.Elements())
      {
        element.AddContent(ToWBXML(childElement));
      }

      return element;
    }

    private XElement ToXml(Element element)
    {
      var xmlElement = new XElement(XName.Get(element.Name));

      foreach (var attribute in element.Attributes)
      {
        xmlElement.SetAttributeValue(XName.Get(attribute.Name), attribute.Value);
      }

      /*
       * NOTE: If we have both text and opaque content which is supported using WBXML we default to
       * using only the text content when converting to XML.
       */
      if (element.Text.Length > 0)
      {
        xmlElement.Add(element.Text);
      }
      else if (element.Opaque.Length > 0)
      {
        xmlElement.Add(Convert.ToBase64String(element.Opaque));
      }

      foreach (var childElement in element.Children)
      {
        xmlElement.Add(ToXml(childElement));
      }

      return xmlElement;
    }
  }
}