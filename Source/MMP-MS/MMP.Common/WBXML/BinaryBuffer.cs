﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;

namespace MMP.Common.WBXML
{
  /// <summary>
  ///   The <b>BinaryBuffer</b> class provides the capabilities similar to the <b>StringBuilder</b> class
  ///   when working with binary data (bytes).
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class BinaryBuffer
  {
    private MemoryStream _stream;

    /// <summary>Constructs a new empty <b>BinaryBuffer</b>.</summary>
    public BinaryBuffer()
    {
      _stream = new MemoryStream();
    }

    /// <summary>Constructs a new <b>BinaryBuffer</b> containing the specified binary data.</summary>
    /// <param name="data">The binary data to initialise the <b>BinaryBuffer</b> with.</param>
    public BinaryBuffer(byte[] data)
    {
      _stream = new MemoryStream(data);
    }

    /// <summary>
    ///   Constructs a new empty <b>BinaryBuffer</b> and pre-allocates the specified number of bytes of
    ///   storage.
    /// </summary>
    /// <param name="length">The number of bytes of storage to pre-allocate.</param>
    public BinaryBuffer(int length)
    {
      _stream = new MemoryStream(length);
    }

    /// <summary>The binary data stored by the <b>BinaryBuffer</b>.</summary>
    public byte[] Data => _stream.ToArray();

    /// <summary>The length of the binary data stored by the <b>BinaryBuffer</b>.</summary>
    public long Length => _stream.Length;

    /// <summary>
    ///   Initialise the <b>BinaryBuffer</b> and pre-allocate the specified number of bytes of storage.
    /// </summary>
    /// <param name="length">The number of bytes of storage to pre-allocate.</param>
    public void Allocate(int length)
    {
      if (_stream != null)
      {
        _stream.Close();
        _stream.Dispose();
      }

      _stream = new MemoryStream(length);
    }

    /// <summary>
    ///   Append the binary data stored in the specified <b>BinaryBuffer</b> instance to this
    ///   <b>BinaryBuffer</b>.
    /// </summary>
    /// <param name="buffer">The <b>BinaryBuffer</b> instance containing the binary data to append.</param>
    public void Append(BinaryBuffer buffer)
    {
      Append(buffer.Data);
    }

    /// <summary>
    ///   Append the specified binary data to the binary data already stored in the
    ///   <b>BinaryBuffer</b>.
    /// </summary>
    /// <param name="data">The binary data to append.</param>
    public void Append(byte[] data)
    {
      _stream.Write(data, 0, data.Length);
    }

    /// <summary>
    ///   Append the specified byte to the binary data already stored in the <b>BinaryBuffer</b>.
    /// </summary>
    /// <param name="value">The byte to append.</param>
    public void Append(byte value)
    {
      _stream.WriteByte(value);
    }

    /// <summary>Empty the <b>BinaryBuffer</b> removing any binary data stored by the buffer.</summary>
    public void Empty()
    {
      if (_stream != null)
      {
        _stream.Close();
        _stream.Dispose();
      }

      _stream = new MemoryStream();
    }
  }
}