﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;

namespace MMP.Common.WBXML
{
  /// <summary>
  ///   The <b>Opaque</b> class stores the data for an opaque (binary data) content type in a WBXML
  ///   document.
  ///   <para>This content type is used to store binary data.</para>
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public class Opaque : Content
  {
    private readonly BinaryBuffer _buffer;

    /// <summary>Constructs a new empty <b>Opaque</b>.</summary>
    public Opaque()
    {
      _buffer = new BinaryBuffer();
    }

    /// <summary>Constructs a new <b>Opaque</b> containing the specified binary data.</summary>
    /// <param name="data">The binary data to populate the <b>Opaque</b> instance with.</param>
    public Opaque(byte[] data)
    {
      _buffer = new BinaryBuffer(data);
    }

    /// <summary>The binary data for the <b>Opaque</b> instance.</summary>
    public byte[] Data => _buffer.Data;

    /// <summary>The length of the binary data for the <b>Opaque</b> instance.</summary>
    public long Length => _buffer.Length;

    /// <summary>
    ///   Append the specified binary data to the binary data already stored in the <b>Opaque</b>
    ///   instance.
    /// </summary>
    /// <param name="data">The binary data to append.</param>
    public void Append(byte[] data)
    {
      _buffer.Append(data);
    }

    /// <summary>
    ///   Append the binary data stored in the specified <b>Opaque</b> instance to this
    ///   <b>Opaque</b> instance.
    /// </summary>
    /// <param name="opaque">The <b>Opaque</b> instance containing the binary data to append.</param>
    public void Append(Opaque opaque)
    {
      Append(opaque.Data);
    }

    /// <summary>
    ///   Print the content to the specified <b>TextWriter</b> using the specified indent level.
    /// </summary>
    /// <param name="writer">The <b>TextWriter</b> to output the content with.</param>
    /// <param name="indent">The indent level.</param>
    public override void Print(TextWriter writer, int indent)
    {
      for (var i = 0; i < indent; i++)
      {
        writer.Write(INDENT);
      }

      writer.Write(INDENT);
      writer.WriteLine("[BINARY: " + _buffer.Length + " bytes]");
    }
  }
}