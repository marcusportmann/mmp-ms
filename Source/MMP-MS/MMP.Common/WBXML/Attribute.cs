﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Common.WBXML
{
  /// <summary>The <b>Attribute</b> class stores the name and value of a WBXML attribute.</summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class Attribute
  {
    /// <summary>Constructs a new <b>Attribute</b> with the specified name and value.</summary>
    /// <param name="name">The name of the attribute.</param>
    /// <param name="value">The value for the attribute.</param>
    public Attribute(string name, string value)
    {
      Name = name;
      Value = value;
    }

    /// <summary>The name of the attribute.</summary>
    public string Name { get; set; }

    /// <summary>The value for the attribute.</summary>
    public string Value { get; set; }
  }
}