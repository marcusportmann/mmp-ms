﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Runtime.Serialization;

namespace MMP.Common.WBXML
{
  /// <summary>
  ///   The <b>ParserException</b> exception is thrown to indicate an error condition when parsing a
  ///   WBXML document.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class ParserException : Exception
  {
    /// <summary>Constructs a new <b>ParserException</b> with the default message.</summary>
    public ParserException() : this("WBXML Parser Error")
    {
    }

    /// <summary>Constructs a new <b>ParserException</b> using serialization.</summary>
    /// <param name="info">The serialization info.</param>
    /// <param name="context">The streaming context.</param>
    public ParserException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    /// <summary>
    ///   Constructs a new <b>ParserException</b> with the default message and specified cause.
    /// </summary>
    /// <param name="cause">The cause saved for later retrieval via the <b>InnerException</b> property.</param>
    public ParserException(Exception cause) : this("WBXML Parser Error", cause)
    {
    }

    /// <summary>Constructs a new <b>ParserException</b> with the specified message.</summary>
    /// <param name="message">The message saved for later retrieval via <b>Message</b> property.</param>
    public ParserException(string message) : this(message, null)
    {
    }

    /// <summary>Constructs a new <b>ParserException</b> with the specified message and cause.</summary>
    /// <param name="message">The message saved for later retrieval via the <b>Message</b> property.</param>
    /// <param name="cause">The cause saved for later retrieval via the <b>InnerException</b> property.</param>
    public ParserException(string message, Exception cause) : base(message, cause)
    {
    }
  }
}

/*
  public static final int ErrEOF = -1;

  public static final int ErrInvalidWBXML = -2;

  public static final int ErrInvalidVersion = -3;

  public static final int ErrUnknownPublicID = -4;

  public static final int ErrInvalidCharset = -5;

  public static final int ErrNotImplemented = -6;

  public static final int ErrUnknownElementTagIdentity = -7;

  public static final int ErrUnknownAttributeTagIdentity = -8;

  public static final int ErrUnknownAttributeValueIdentity = -9;

  public static final int ErrUnknownContentType = -10;

  public static final int ErrEndOfElementNotFound = -11;

  public static final int ErrInvalidStringTableOffset = -12;
*/