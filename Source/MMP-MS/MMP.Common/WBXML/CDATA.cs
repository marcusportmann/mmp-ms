﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;

namespace MMP.Common.WBXML
{
  /// <summary>The <b>CDATA</b> class stores the data for a CDATA (Character Data) content type in a WBXML document.
  ///   <para>This content type is used to store a text string.</para>
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class CDATA : Content
  {
    /// <summary>Constructs a new <b>CDATA</b> containing the specified text.</summary>
    /// <param name="text">The text content for the <b>CData</b> instance.</param>
    public CDATA(string text)
    {
      Text = text;
    }

    /// <summary>The text content for the <b>CDATA</b> instance.</summary>
    public string Text { get; set; }

    /// <summary>Print the content to the specified <b>TextWriter</b> using the specified indent level.</summary>
    /// <param name="writer">The <b>TextWriter</b> to output the content with.</param>
    /// <param name="indent">The indent level.</param>
    public override void Print(TextWriter writer, int indent)
    {
      for (int i = 0; i < indent; i++)
      {
        writer.Write(INDENT);
      }

      writer.WriteLine(Text);
    }
  }
}