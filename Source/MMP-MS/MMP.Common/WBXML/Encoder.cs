﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMP.Common.WBXML
{
  /// <summary>
  ///   The <b>Encoder</b> class generates the binary data representation of a WBXML document from a
  ///   WBXML object hierarchy.
  /// </summary>
  [CLSCompliant(true)]
  public sealed class Encoder
  {
    private const int INITIAL_BUFFER_SIZE = 1024;
    private readonly BinaryBuffer _buffer;
    private readonly IList<string> _stringTable;

    /// <summary>Constructs a new <b>Encoder</b> to WBXML encode the specified document.</summary>
    /// <param name="document">The document to encode.</param>
    public Encoder(Document document)
    {
      // Initialise member variables
      var document1 = document;
      _buffer = new BinaryBuffer(INITIAL_BUFFER_SIZE);
      _stringTable = new List<string>();

      // The WBXML version
      AppendByte(WBXML.WBXML_VERSION);

      // The public ID of the DTD associated with the specified document
      AppendMultiByteUint32(document1.PublicId);

      // The IANA assigned ID of the charset for the specified document
      AppendMultiByteUint32(WBXML.CHARSET_UTF_8);

      // If we are dealing with a document with an unknown public ID  for its DTD then
      // we must build a string table
      if (document1.PublicId == Document.PUBLIC_ID_UNKNOWN)
      {
        BuildStringTable(document1.RootElement);
      }

      // Write the length of the string table
      var stringTableLength = GetStringTableLength();

      AppendMultiByteUint32(stringTableLength);

      // Write the string table if required
      if (stringTableLength > 0)
      {
        foreach (var tmpStr in _stringTable)
        {
          AppendString(tmpStr);
        }
      }

      GenerateWbxml(document1.RootElement);
    }

    /// <summary>The binary data which represents the WBXML encoded document.</summary>
    public byte[] Data => _buffer.Data;

    /// <summary>The length of the binary data which represents the WBXML encoded document.</summary>
    public long Length => _buffer.Length;

    private void AddToStringTable(string str)
    {
      if (_stringTable.Any(existingString => existingString == str))
      {
        return;
      }

      // Add the string to the string table
      _stringTable.Add(str);
    }

    private void AppendBinary(byte[] data)
    {
      _buffer.Append(data);
    }

    private void AppendByte(byte b)
    {
      _buffer.Append(b);
    }

    private void AppendMultiByteUint32(int value)
    {
      var buf = new byte[5];
      var idx = 0;

      do
      {
        buf[idx++] = (byte) (value & 0x7f);
        value = value >> 7;
      }
      while (value != 0);

      while (idx > 1)
      {
        AppendByte((byte) (buf[--idx] | 0x80));
      }

      AppendByte(buf[0]);
    }

    private void AppendString(string str)
    {
      AppendBinary(Encoding.UTF8.GetBytes(str));
      AppendByte(0);
    }

    private void BuildStringTable(Element element)
    {
      // Get the name for the element and add it to the string table
      AddToStringTable(element.Name);

      // Process the attributes for this element
      if (element.HasAttributes())
      {
        foreach (var attribute in element.Attributes)
        {
          AddToStringTable(attribute.Name);
        }
      }

      // Process any child elements under this element
      if (element.HasChildren())
      {
        foreach (var childElement in element.Children)
        {
          BuildStringTable(childElement);
        }
      }
    }

    private void GenerateWbxml(Element element)
    {
      // Literal TAG
      byte elementTag = WBXML.TOKEN_LITERAL;

      // Build the tag for this element and write it to the buffer
      // We assume that all tag names will be LITERALS store in the string table
      if (element.HasAttributes())
      {
        elementTag += 0x80;
      }

      if (element.HasContent())
      {
        elementTag += 0x40;
      }

      AppendByte(elementTag);

      // Append the offset into the string table for the tag name
      AppendMultiByteUint32(GetStringTableOffset(element.Name));

      // If we have attributes then write them out
      if (element.HasAttributes())
      {
        foreach (var attribute in element.Attributes)
        {
          // Write out that this attribute is a literal
          AppendByte(WBXML.TOKEN_LITERAL);

          // Write the string table offset for the name of the attribute
          AppendMultiByteUint32(GetStringTableOffset(attribute.Name));

          // Inline string value
          AppendByte(WBXML.TOKEN_STR_I);

          // Append the string
          AppendString(attribute.Value);
        }

        // Append the end of attribute space token
        AppendByte(WBXML.TOKEN_END);
      }

      // Process any content under this element
      if (element.HasContent())
      {
        foreach (var content in element.Content)
        {
          // If this is a child element...
          if (content is Element)
          {
            GenerateWbxml(content as Element);
          }
          // If this is a CDATA element...
          else if (content is CDATA)
          {
            var cdata = content as CDATA;

            AppendByte(WBXML.TOKEN_STR_I);
            AppendString(cdata.Text);
          }
          // If this is an Opaque element...
          else if (content is Opaque)
          {
            var opaque = content as Opaque;

            AppendByte(WBXML.TOKEN_OPAQUE);
            AppendMultiByteUint32((int) opaque.Length);
            AppendBinary(opaque.Data);
          }
        }
      }

      // Append the end of element space token
      AppendByte(WBXML.TOKEN_END);
    }

    private int GetStringTableLength()
    {
      var stringTablelength = 0;

      foreach (var str in _stringTable)
      {
        stringTablelength += Encoding.UTF8.GetByteCount(str);
        stringTablelength += 1;
      }

      return stringTablelength;
    }

    private int GetStringTableOffset(string str)
    {
      var offset = 0;

      foreach (var tmpStr in _stringTable)
      {
        if (tmpStr == str)
        {
          return offset;
        }

        offset += Encoding.UTF8.GetByteCount(tmpStr);
        offset += 1;
      }

      // The string could not be found
      return -1;
    }
  }
}