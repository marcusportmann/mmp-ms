﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using MMP.Application.Mvc.Template;
using MMP.Common.IOC;
using MMP.Common.Logging;
using MMP.Common.Persistence;
using NLog;
using NLog.Config;
using NLog.Targets;
using Unity.Mvc5;
using DebugTarget = MMP.Common.Logging.DebugTarget;

namespace MMP.Application.Mvc
{
  /// <summary>
  ///   The <b>WebApplication</b> class provides a base class for all "application specific" web
  ///   application classes.
  ///   <para>
  ///     For instructions on enabling IIS6 or IIS7 classic mode, visit
  ///     http://go.microsoft.com/?LinkId=9394801
  ///   </para>
  /// </summary>
  public abstract class WebApplication<T> : HttpApplication, IWebApplication<T> where T : WebSession, new()
  {
    /// <summary>
    ///   The configuration information for the web application stored in the Windows Registry.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     This is a <b>static</b> variable because it is initialised once by the <b>InitApplication</b>
    ///     method.
    ///   </para>
    /// </remarks>
    // ReSharper disable once StaticMemberInGenericType
    private static WebApplicationConfiguration _configuration;

    // ReSharper disable once StaticMemberInGenericType
    protected static readonly string WEB_SESSION_KEY = typeof (WebSession).FullName;

    /// <summary>The assembly for the application.</summary>
    // ReSharper disable once StaticMemberInGenericType
    private static Assembly _assembly;

    /// <summary>The types for the ASP.NET MVC controllers for the web application.</summary>
    // ReSharper disable once StaticMemberInGenericType
    private static IDictionary<string, Type> _controllerTypes;

    /// <summary>
    ///   The empty list of security attributes for an action method on an ASP.NET MVC controller for the
    ///   web application.
    /// </summary>
    // ReSharper disable once StaticMemberInGenericType
    public static readonly ActionSecurityAttribute[] EMPTY_CONTROLLER_ACTION_SECURITY_ATTRIBUTES =
      new ActionSecurityAttribute[0];

    private IList<Assembly> _assemblies;

    /// <summary>
    ///   The security attributes for the action methods on the ASP.NET MVC controllers for the web
    ///   application.
    ///   <remarks>The keys are in the format <b>ControllerName.ActionMethodName</b>.</remarks>
    /// </summary>
    private IDictionary<string, ActionSecurityAttribute[]> _controllerActionSecurityAttributes;

    /// <summary>The logger.</summary>
    private Logger _logger;

    /// <summary>
    ///   The security attributes for the action methods on the ASP.NET MVC controllers for the web
    ///   application.
    ///   <remarks>The keys are in the format <b>ControllerName.ActionMethodName</b>.</remarks>
    /// </summary>
    public IDictionary<string, ActionSecurityAttribute[]> ControllerActionSecurityAttributes
    {
      get
      {
        if (_controllerActionSecurityAttributes != null)
        {
          return _controllerActionSecurityAttributes;
        }

        _controllerActionSecurityAttributes = new Dictionary<string, ActionSecurityAttribute[]>();

        foreach (var controllerType in ControllerTypes.Values)
        {
          foreach (var actionMethodInfo in controllerType.GetMethods())
          {
            var securityAttributes =
              actionMethodInfo.GetCustomAttributes(typeof (ActionSecurityAttribute), true) as
                ActionSecurityAttribute[];

            if ((securityAttributes != null) && (securityAttributes.Length > 0))
            {
              var controllerName = controllerType.Name;

              if ((controllerName.EndsWith("Controller", StringComparison.CurrentCultureIgnoreCase)) &&
                  (controllerName.Length > 10))
              {
                controllerName = controllerName.Substring(0, controllerName.Length - 10);
              }

              _controllerActionSecurityAttributes.Add($"{controllerName}.{actionMethodInfo.Name}", securityAttributes);
            }
          }
        }

        return _controllerActionSecurityAttributes;
      }
    }

    /// <summary>
    ///   Returns the fully qualified or application-relative path for the application's log file.
    /// </summary>
    public string LogFilePath => "${basedir}/" + ApplicationName + ".log";

    /// <summary>The logger.</summary>
    protected Logger Logger => _logger ?? (_logger = LogManager.GetCurrentClassLogger());

    /// <summary>Returns the name of the application.</summary>
    /// <remarks>
    ///   The application name is used to retrieve the configuration for the application stored in the
    ///   registry under the key <b>HKLM\SOFTWARE\{ProviderName}\{ApplicationName}</b>.
    /// </remarks>
    public abstract string ApplicationName { get; }

    /// <summary>The assemblies for the application.</summary>
    public IList<Assembly> Assemblies
    {
      get
      {
        if (_assemblies != null)
        {
          return _assemblies;
        }

        _assemblies = new List<Assembly> {Assembly};

        var referencedAssemblyNames = Assembly.GetReferencedAssemblies();

        foreach (var assembly in from assembly in AppDomain.CurrentDomain.GetAssemblies()
          where !assembly.GlobalAssemblyCache
          from referencedAssemblyName in referencedAssemblyNames
          where assembly.FullName == referencedAssemblyName.FullName
          select assembly)
        {
          _assemblies.Add(assembly);
        }

        return _assemblies;
      }
    }

    /// <summary>The main assembly for the application.</summary>
    public Assembly Assembly
    {
      get
      {
        if (_assembly != null)
          return _assembly;

        if (HttpContext.Current == null || HttpContext.Current.ApplicationInstance == null)
        {
          return null;
        }

        var type = HttpContext.Current.ApplicationInstance.GetType();

        while (type != null && type.Namespace == "ASP")
        {
          type = type.BaseType;
        }

        _assembly = type?.Assembly;

        return _assembly;
      }
    }

    /// <summary>
    ///   Returns the ADO.NET connection string used to connect to the application's database.
    /// </summary>
    public string ConnectionString => _configuration.ConnectionString;

    /// <summary>The types for the ASP.NET MVC controllers for the web application.</summary>
    public IDictionary<string, Type> ControllerTypes
    {
      get
      {
        if (_controllerTypes != null)
        {
          return _controllerTypes;
        }

        _controllerTypes = new Dictionary<string, Type>();

        foreach (var assembly in Assemblies)
        {
          var controllerTypes = assembly.GetTypes().Where(type => type.IsSubclassOf(typeof (Controller))).ToList();

          foreach (var controllerType in controllerTypes)
          {
            if (controllerType.Namespace == null || controllerType.Namespace.StartsWith("System.Web"))
              continue;

            var controllerName = controllerType.Name;

            if ((controllerName.EndsWith("Controller", StringComparison.CurrentCultureIgnoreCase)) &&
                (controllerName.Length > 10))
            {
              controllerName = controllerName.Substring(0, controllerName.Length - 10);
            }

            if (_controllerTypes.ContainsKey(controllerName))
            {
              throw new WebApplicationException($"Duplicate controller found with name \"{controllerName}\"");
            }

            _controllerTypes.Add(controllerName, controllerType);
          }
        }

        return _controllerTypes;
      }
    }

    /// <summary>Returns the name of the application provider.</summary>
    /// <remarks>
    ///   The application provider name is used to retrieve the configuration for the application stored in
    ///   the registry under the key <b>HKLM\SOFTWARE\{ProviderName}\{ApplicationName}</b>.
    /// </remarks>
    public abstract string ProviderName { get; }

    /// <summary>
    ///   The web session instance, derived from the <b>MMP.Application.Mvc.WebSession</b> type, that
    ///   provides strongly typed access to the session information for the user associated with the
    ///   current HTTP request.
    /// </summary>
    public T WebSession
    {
      get
      {
        var httpContext = HttpContext.Current;

        var webSession = httpContext.Session[WEB_SESSION_KEY] as T;

        if (webSession == null)
        {
          webSession = new T();

          httpContext.Session[WEB_SESSION_KEY] = webSession;
        }

        return webSession;
      }
    }

    /// <summary>
    ///   Returns the security attributes for the action methods on the ASP.NET MVC controllers for the web
    ///   application.
    /// </summary>
    /// <param name="controllerName">The name of the ASP.NET MVC controller.</param>
    /// <param name="actionMethodName">The name of the action method.</param>
    /// <returns>
    ///   The security attributes for the action methods on the ASP.NET MVC controllers for the web
    ///   application.
    /// </returns>
    public ActionSecurityAttribute[] GetControllerActionSecurityAttributes(string controllerName,
      string actionMethodName)
    {
      var key = $"{controllerName}.{actionMethodName}";

      var controllerActionSecurityAttributes = ControllerActionSecurityAttributes;

      if (!controllerActionSecurityAttributes.ContainsKey(key))
      {
        return EMPTY_CONTROLLER_ACTION_SECURITY_ATTRIBUTES;
      }
      return controllerActionSecurityAttributes[key];
    }

    protected void Application_Start()
    {
      InitApplication();

      foreach (var controllerName in ControllerTypes.Keys)
      {
        Logger.Info("Found ASP.NET MVC controller \"" + controllerName + "\" with type \"" +
                    ControllerTypes[controllerName].FullName + "\"");
      }

      Logger.Info(
        $"Found {ControllerActionSecurityAttributes.Keys.Count} action methods on the ASP.NET MVC controllers with security attributes");
    }

    /// <summary>Initialise the application.</summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>Application_Start</b> method, as part of the
    ///     global initialisation of the ASP.NET application.
    ///   </para>
    /// </remarks>
    protected virtual void InitApplication()
    {
      // Load the configuration information for the web application stored in the Windows Registry
      LoadConfiguration();

      // Initialise the logging for the web application
      InitLogging();

      // Register the application-specific types with the dependency injection container
      RegisterAllTypes();

      // Set the dependency resolver for the ASP.NET MVC web application
      DependencyResolver.SetResolver(new UnityDependencyResolver(Container.UNITY_CONTAINER));

      // Register the virtual path provider for the ASP.NET MVC web application
      HostingEnvironment.RegisterVirtualPathProvider(new TemplateVirtualPathProvider());

      AreaRegistration.RegisterAllAreas();

      // Initialise the routing for the ASP.NET MVC web application
      InitRouting();
    }

    /// <summary>Initialise the routing for the ASP.NET MVC web application.</summary>
    protected virtual void InitRouting()
    {
      RouteTable.Routes.IgnoreRoute("{*staticfile}", new {staticfile = @".*\.(css|js|gif|jpg|png)(/.*)?"});

      RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

      RouteTable.Routes.MapRoute("Default", "{controller}/{action}/{id}",
        new {controller = "Home", action = "Index", id = UrlParameter.Optional});

      //RouteTable.Routes.MapRoute("ApplicationBodyLayout", "{controller}/{action}/{id}",
      //  new {controller = "Layout", action = "ApplicationBody", id = UrlParameter.Optional});

      // Register the application-specific routes
      RegisterRoutes();
    }

    /// <summary>Register the ASP.NET MVC web application routes.</summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off initialisation of the
    ///     ASP.NET MVC web application routes.
    ///   </para>
    /// </remarks>
    protected abstract void RegisterRoutes();

    /// <summary>Register the application-specific types with the IOC container.</summary>
    /// <remarks>
    ///   <para>
    ///     You cannot use the logging infrastructure while registering the default and
    ///     application-specific types with the IOC container.
    ///   </para>
    ///   <para>
    ///     Types should be registered using the <b>MMP.Common.IOC.Container</b> static wrapper around the
    ///     IOC container.
    ///   </para>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off registration of the
    ///     application-specific types with the IOC container.
    ///   </para>
    /// </remarks>
    protected abstract void RegisterTypes();

    /// <summary>Initialise the logging infrastructure.</summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off initialisation of the
    ///     logging.
    ///   </para>
    /// </remarks>
    private void InitLogging()
    {
      try
      {
        var config = new LoggingConfiguration();

        // Initialise the DebugTarget
        var debugTarget = new DebugTarget
        {
          Layout =
            "${date:format=yyyy-MM-dd HH\\:MM\\:ss,fff} ${pad:padding=5:inner=${level:uppercase=true}} [${logger:shortName=true}] ${message}"
        };
        config.AddTarget("debug", debugTarget);

        var debugLoggingRule = new LoggingRule("*", LogLevel.Debug, debugTarget);
        config.LoggingRules.Add(debugLoggingRule);

        // Initialise the ASPNetTraceTarget
        var aspNetTraceTarget = new AspNetTraceTarget
        {
          Layout =
            "${date:format=yyyy-MM-dd HH\\:MM\\:ss,fff} ${pad:padding=5:inner=${level:uppercase=true}} [${logger}] ${message}"
        };

        var aspNetLoggingRule = new LoggingRule("*", LogLevel.Debug, aspNetTraceTarget);
        config.LoggingRules.Add(aspNetLoggingRule);

        // Initialise the FileTarget
        var fileTarget = new FileTarget
        {
          Layout =
            "${date:format=yyyy-MM-dd HH\\:MM\\:ss,fff} ${pad:padding=5:inner=${level:uppercase=true}} [${logger}] ${message}",
          FileName = LogFilePath
        };
        var fileLoggingRule = new LoggingRule("*", LogLevel.Info, fileTarget);
        config.LoggingRules.Add(fileLoggingRule);

        LogManager.Configuration = config;
      }
      catch (Exception ex)
      {
        throw new WebApplicationException("Failed to initialise the logging for the web application", ex);
      }
    }

    /// <summary>
    ///   Load the configuration information for the web application stored in the Windows Registry.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off initialisation of the
    ///     application configuration.
    ///   </para>
    /// </remarks>
    private void LoadConfiguration()
    {
      try
      {
        string applicationConnectionString;
        string applicationCertificateThumbprint;

        using (
          var applicationKey =
            Microsoft.Win32.Registry.LocalMachine.OpenSubKey($@"SOFTWARE\{ProviderName}\{ApplicationName}", true))
        {
          if (applicationKey == null)
          {
            throw new WebApplicationException(
              $@"Failed to find the registry key (SOFTWARE\{ProviderName}\{ApplicationName}) for the application");
          }

          applicationConnectionString = applicationKey.GetValue("ApplicationConnectionString") as string;
          applicationCertificateThumbprint = applicationKey.GetValue("ApplicationCertificateThumbprint") as string;
        }

        _configuration = new WebApplicationConfiguration(applicationConnectionString, applicationCertificateThumbprint);
      }
      catch (Exception ex)
      {
        throw new WebApplicationException("Failed to load the configuration for the web application", ex);
      }
    }

    /// <summary>Register the default and application-specific types with the IOC container.</summary>
    /// <remarks>
    ///   <para>
    ///     You cannot use the logging infrastructure while registering the default and
    ///     application-specific types with the IOC container.
    ///   </para>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off initialisation of the
    ///     types with the IOC Container.
    ///   </para>
    /// </remarks>
    private void RegisterAllTypes()
    {
      try
      {
        // Register the default types
        Container.RegisterInstance(typeof (IDGenerator), new IDGenerator(ConnectionString));

        // Register the application-specific types
        RegisterTypes();

        // Resolve the initial dependencies
        Container.ResolveInitialDependencies();
      }
      catch (Exception ex)
      {
        throw new WebApplicationException("Failed to register the types with the Unity container", ex);
      }
    }
  }
}