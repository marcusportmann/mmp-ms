﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Application.Mvc
{
  /// <summary>
  ///   The <b>SecuredAttribute</b> attribute is used to to identify action methods on ASP.NET MVC
  ///   controllers that have been secured.
  ///   <remarks>
  ///     <para>
  ///       The value specified for the annotation is the function code that uniquely identifies the
  ///       function associated with the web page e.g. Application.AddUser, etc. Function codes can refer
  ///       to a specific "function" e.g. Application.AddUser or a "functionality grouping" e.g.
  ///       Application.UserManagement. The decision* on whether to use a "function" or "functionality
  ///       grouping" is dependent on the granularity of the application's access control.
  ///     </para>
  ///   </remarks>
  /// </summary>
  [AttributeUsage(AttributeTargets.Method)]
  public class SecuredAttribute : ActionSecurityAttribute
  {
    /// <summary>Constructs a new <b>Secured</b> attribute instance.</summary>
    /// <param name="functionCode">
    ///   The function code uniquely identifying the function associated with the action method e.g.
    /// </param>
    public SecuredAttribute(string functionCode)
    {
      FunctionCode = functionCode;
    }

    /// <summary>
    ///   The function code uniquely identifying the function associated with the action method e.g.
    ///   Security.CreateUser.
    /// </summary>
    public string FunctionCode { get; set; }
  }
}