﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Web.Mvc;

namespace MMP.Application.Mvc
{
  /// <summary>
  ///   The <b>WebPage</b> class is the base class that all web page classes must be derived from in web
  ///   applications that make use of the library.
  ///   <remarks>
  ///     It provides a mechanism for securing pages through the use of authorised functions. Every
  ///     authorised function is identified through a unique authorised function code. A page or pages
  ///     that provide a unit of functionality are assigned an authorised function code. Authorised
  ///     function codes are also assigned to users and groups using the Security Service for the web
  ///     application. When a user attempts to access a page this class implements an access control
  ///     check which compares the authorised function code for the page to the authorised function codes
  ///     assigned directly and indirectly(via groups) to the user.
  ///   </remarks>
  /// </summary>
  public abstract class WebPage : WebViewPage
  {
    /// <summary>The Application.AnonymousAccess function code applied to unsecured web pages.</summary>
    public static readonly string FUNCTION_CODE_ANONYMOUS_ACCESS = "Application.AnonymousAccess";

    /// <summary>
    ///   The Application.SecureAnonymousAccess function code applied to secure web pages that can be
    ///   accessed any logged in user regardless of their role.
    /// </summary>
    public static readonly string FUNCTION_CODE_SECURE_ANONYMOUS_ACCESS = "Application.SecureAnonymousAccess";

    /// <summary>
    ///  The function code uniquely identifying the function associated with the page e.g. Application.AddUser.
    /// </summary>
    private string _functionCode;

  }
}