﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Application.Mvc
{
  /// <summary>
  ///   The <b>SecuredAnonymousAttribute</b> attribute is used to identify action methods on ASP.NET MVC
  ///   controllers that can be accessed by authenticated users regardless of their roles.
  /// </summary>
  [AttributeUsage(AttributeTargets.Method)]
  public class SecuredAnonymousAttribute : ActionSecurityAttribute
  {
  }
}