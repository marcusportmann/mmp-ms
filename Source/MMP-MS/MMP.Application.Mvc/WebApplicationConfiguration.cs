﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Application.Mvc
{
  /// <summary>
  ///   The <b>WebApplicationConfiguration</b> class stores the configuration information for a web
  ///   application.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class WebApplicationConfiguration
  {
    /// <summary>
    ///   The subject or certificate thumbprint identifying the application's certificate in the
    ///   TrustedPeople X.509 certificate store.
    /// </summary>
    private readonly string _certificate;

    /// <summary>The ADO.NET connection string used to connect to the application's database.</summary>
    private readonly string _connectionString;

    /// <summary>Constructs a new <b>WebApplicationConfiguration</b>.</summary>
    /// <param name="connectionString">
    ///   The ADO.NET connection string used to connect to the application's
    ///   database.
    /// </param>
    /// <param name="certificate">
    ///   The subject or certificate thumbprint identifying the application's certificate in the
    ///   TrustedPeople X.509 certificate store.
    /// </param>
    public WebApplicationConfiguration(string connectionString, string certificate)
    {
      _connectionString = connectionString;
      _certificate = certificate;
    }

    /// <summary>
    ///   Returns the subject or certificate thumbprint identifying the application's certificate in the
    ///   TrustedPeople X.509 certificate store.
    /// </summary>
    public string Certificate => _certificate;

    /// <summary>
    ///   Returns the ADO.NET connection string used to connect to the application's database.
    /// </summary>
    public string ConnectionString => _connectionString;
  }
}