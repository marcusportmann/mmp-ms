﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Reflection;

namespace MMP.Application.Mvc
{
  /// <summary>
  ///   The <b>IWebApplication</b> interface defines the functionality that must be implemented by all
  ///   "application specific" web application classes.
  /// </summary>
  public interface IWebApplication<out T> where T : WebSession, new()
  {
    /// <summary>Returns the name of the application.</summary>
    /// <remarks>
    ///   The application name is used to retrieve the configuration for the application stored in the
    ///   registry under the key <b>HKLM\SOFTWARE\{ProviderName}\{ApplicationName}</b>.
    /// </remarks>
    string ApplicationName { get; }

    /// <summary>The assemblies for the application.</summary>
    IList<Assembly> Assemblies { get; }

    /// <summary>The main assembly for the application.</summary>
    Assembly Assembly { get; }

    /// <summary>
    ///   Returns the ADO.NET connection string used to connect to the application's database.
    /// </summary>
    string ConnectionString { get; }

    /// <summary>The types for the ASP.NET MVC controllers for the web application.</summary>
    IDictionary<string, Type> ControllerTypes { get; }

    /// <summary>Returns the name of the application provider.</summary>
    /// <remarks>
    ///   The application provider name is used to retrieve the configuration for the application stored in
    ///   the registry under the key <b>HKLM\SOFTWARE\{ProviderName}\{ApplicationName}</b>.
    /// </remarks>
    string ProviderName { get; }

    /// <summary>The <b>WebSession</b> for the current user.</summary>
    /// <remarks>
    ///   <para>
    ///     This will create a new <b>WebSession</b> instance for the current user if one does not already
    ///     exist.
    ///   </para>
    ///   <para>
    ///     The <b>WebSession</b> provides strongly typed access to the session information for the user
    ///     associated with the current HTTP request.
    ///   </para>
    /// </remarks>
    T WebSession { get; }
  }
}