﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using MMP.Application.Mvc.Template.Navigation;

namespace MMP.Application.Mvc.Template
{
  /// <summary>
  ///   The <b>ITemplateWebApplication</b> interface defines the functionality that must be implemented
  ///   by all "application specific" web application classes for applications that make use of the web
  ///   application template.
  /// </summary>
  public interface ITemplateWebApplication<out T> : IWebApplication<T> where T : TemplateWebSession, new()
  {
    /// <summary>The navigation hierarchy for the application.</summary>
    NavigationGroup Navigation { get; }

    /// <summary>The <b>TemplateWebSession</b> for the current user.</summary>
    /// <remarks>
    ///   <para>
    ///     This will create a new <b>TemplateWebSession</b> instance for the current user if one does not
    ///     already exist.
    ///   </para>
    ///   <para>
    ///     The <b>TemplateWebSession</b> provides strongly typed access to the session information for the
    ///     user associated with the current HTTP request.
    ///   </para>
    /// </remarks>
    T TemplateWebSession { get; }
  }
}