﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;

namespace MMP.Application.Mvc.Template.Navigation
{
  /// <summary>
  ///   The <b>NavigationItem</b> class implements the base functionality common to all navigation items
  ///   that form part of a web application's navigation hierarchy.
  /// </summary>
  public abstract class NavigationItem
  {
    /// <summary>Constructs a new <b>NavigationItem</b>.</summary>
    /// <param name="name">The name of the navigation item.</param>
    /// <param name="iconClass">The CSS class for the icon for the navigation item.</param>
    protected NavigationItem(string name, string iconClass)
    {
      Name = name;
      IconClass = iconClass;

      lock (AllNavigationItems)
      {
        Id = AllNavigationItems.Count + 1;
        AllNavigationItems[Id] = this;
      }
    }

    /// <summary>Constructs a new <b>NavigationItem</b>.</summary>
    /// <param name="name">The name of the navigation item.</param>
    protected NavigationItem(string name)
    {
      Name = name;

      lock (AllNavigationItems)
      {
        Id = AllNavigationItems.Count + 1;
        AllNavigationItems[Id] = this;
      }
    }

    public static IDictionary<int, NavigationItem> AllNavigationItems { get; } = new Dictionary<int, NavigationItem>()
      ;

    /// <summary>The CSS class for the icon for the navigation item.</summary>
    public string IconClass { get; set; }

    /// <summary>The numeric ID uniquely identifying the navigation item.</summary>
    public int Id { get; set; }

    /// <summary>The name of the navigation item.</summary>
    public string Name { get; set; }
  }
}