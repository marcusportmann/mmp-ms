﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;

namespace MMP.Application.Mvc.Template.Navigation
{
  /// <summary>
  ///   The <b>NavigationLink</b> class stores the information for a navigation link that forms part of a
  ///   web application's navigation hierarchy.
  /// </summary>
  public class NavigationLink : NavigationItem
  {
    // TODO: DELETE THIS -- MARCUS
    ///// <summary>Constructs a new <b>NavigationLink</b>.</summary>
    ///// <param name="name">The name of the navigation link.</param>
    ///// <param name="functionCode">
    /////   The function code uniquely identifying the function associated with the the link e.g.
    /////   Security.CreateUser.
    ///// </param>
    ///// <param name="parameters">The parameters associated with the link.</param>
    //public NavigationLink(string name, string functionCode, IDictionary<string, string> parameters) : base(name)
    //{
    //  // TODO: DELETE THIS -- MARCUS
    //  //FunctionCode = functionCode;
    //  Parameters = parameters;
    //}

    /// <summary>Constructs a new <b>NavigationLink</b>.</summary>
    /// <param name="name">The name of the navigation link.</param>
    /// <param name="actionName">The name of the action method associated with the navigation link.</param>
    /// <param name="controllerName">The name of the controller associated with the navigation link.</param>
    /// <param name="parameters">The parameters associated with the link.</param>
    public NavigationLink(string name, string actionName, string controllerName,
      IDictionary<string, string> parameters) : base(name)
    {
      ActionName = actionName;
      ControllerName = controllerName;
      Parameters = parameters;
    }

    /// <summary>Constructs a new <b>NavigationLink</b>.</summary>
    /// <param name="name">The name of the navigation link.</param>
    /// <param name="actionName">The name of the action method associated with the navigation link.</param>
    /// <param name="controllerName">The name of the controller associated with the navigation link.</param>
    public NavigationLink(string name, string actionName, string controllerName) : base(name)
    {
      ActionName = actionName;
      ControllerName = controllerName;
      Parameters = new Dictionary<string, string>();
    }

    /// <summary>Constructs a new <b>NavigationLink</b>.</summary>
    /// <param name="name">The name of the navigation link.</param>
    public NavigationLink(string name) : base(name)
    {
      Parameters = new Dictionary<string, string>();
    }

    /// <summary>The name of the action method associated with the navigation link.</summary>
    public string ActionName { get; set; }

    /// <summary>The name of the controller associated with the navigation link.</summary>
    public string ControllerName { get; set; }

    // TODO: DELETE THIS -- MARCUS
    ///// <summary>
    /////   The function code uniquely identifying the function associated with the the link e.g.
    /////   Security.CreateUser.
    ///// </summary>
    //public string FunctionCode { get; set; }

    /// <summary>The parameters associated with the link.</summary>
    public IDictionary<string, string> Parameters { get; set; }
  }
}