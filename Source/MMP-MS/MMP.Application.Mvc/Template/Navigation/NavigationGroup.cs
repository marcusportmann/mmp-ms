﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;

namespace MMP.Application.Mvc.Template.Navigation
{
  /// <summary>
  ///   The <b>NavigationGroup</b> class stores the information for a navigation group which groups a
  ///   number of navigation items under an application's navigation hierarchy.
  /// </summary>
  public class NavigationGroup : NavigationItem
  {
    /// <summary>Constructs a new <b>NavigationGroup</b>.</summary>
    /// <param name="name">The name of the navigation group.</param>
    public NavigationGroup(string name) : base(name)
    {
      Items = new List<NavigationItem>();
    }

    /// <summary>The navigation items for the navigation group.</summary>
    public IList<NavigationItem> Items { get; set; }

    /// <summary>
    ///   Add the navigation item to the navigation group. The item added may be a sub-group.
    /// </summary>
    /// <param name="navigationItem">The navigation item to add to the navigation group.</param>
    public void AddItem(NavigationItem navigationItem)
    {
      Items.Add(navigationItem);
    }


    // * Returns <code>true</code> if the page is in the navigation item's hierarchy or
    // * <code>false</code> otherwise.
    // *
    // * @param page the page
    // *
    // * @return <code>true</code> if the page is in the navigation item's hierarchy or
    // *         <code>false</code> otherwise
    // */
    //public boolean isPageInNavigationHierarchy(Page page)
    //{
    //  for (NavigationItem navigationItem : items)
    //  {
    //    if (navigationItem.isPageInNavigationHierarchy(page))
    //    {
    //      return true;
    //    }
    //  }

    //  return false;
    //}

  }
}