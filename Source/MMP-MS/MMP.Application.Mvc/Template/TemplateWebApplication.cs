﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MMP.Application.Mvc.Template.Navigation;

namespace MMP.Application.Mvc.Template
{
  /// <summary>
  ///   The <b>TemplateWebApplication</b> class provides a base class for all application-specific
  ///   classes for applications that make use of the web application template.
  /// </summary>
  public abstract class TemplateWebApplication<T> : WebApplication<T>, ITemplateWebApplication<T>
    where T : TemplateWebSession, new()
  {
    /// <summary>The navigation hierarchy for the application.</summary>
    /// <remarks>
    ///   <para>
    ///     This is a <b>static</b> variable because it is initialised once by the <b>InitApplication</b>
    ///     method.
    ///   </para>
    /// </remarks>
    // ReSharper disable once StaticMemberInGenericType
    private static NavigationGroup _navigation;

    /// <summary>The user-friendly name that should be displayed for the application.</summary>
    public abstract string ApplicationDisplayName { get; }

    /// <summary>The navigation hierarchy for the application.</summary>
    public NavigationGroup Navigation => _navigation;

    /// <summary>The <b>TemplateWebSession</b> for the current user.</summary>
    /// <remarks>
    ///   <para>
    ///     This will create a new <b>TemplateWebSession</b> instance for the current user if one does not
    ///     already exist.
    ///   </para>
    ///   <para>
    ///     The <b>TemplateWebSession</b> provides strongly typed access to the session information for the
    ///     user associated with the current HTTP request.
    ///   </para>
    /// </remarks>
    public T TemplateWebSession
    {
      get
      {
        var httpContext = HttpContext.Current;

        var templateWebSession = httpContext.Session[WEB_SESSION_KEY] as T;

        if (templateWebSession == null)
        {
          templateWebSession = new T();

          httpContext.Session[WEB_SESSION_KEY] = templateWebSession;
        }

        return templateWebSession;
      }
    }

    /// <summary>The <b>WebSession</b> for the current user.</summary>
    /// <remarks>
    ///   <para>
    ///     This will create a new <b>WebSession</b> instance for the current user if one does not already
    ///     exist.
    ///   </para>
    ///   <para>
    ///     The <b>WebSession</b> provides strongly typed access to the session information for the user
    ///     associated with the current HTTP request.
    ///   </para>
    /// </remarks>
    public new T WebSession => TemplateWebSession;

    /// <summary>Initialise the application.</summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>Application_Start</b> method, as part of the
    ///     global initialisation of the ASP.NET application.
    ///   </para>
    /// </remarks>
    protected override void InitApplication()
    {
      base.InitApplication();

      // Initialise the web application template bundles
      InitTemplateBundles();

      // Initialise the navigation hierarchy
      _navigation = new NavigationGroup("");

      InitNavigation(_navigation);
    }

    /// <summary>
    ///   This method must be implemented by all application-specific <b>TemplateWebApplication</b>
    ///   subclasses to setup the navigation hierarchy for the application.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off initialisation of the
    ///     navigation hierarchy.
    ///   </para>
    /// </remarks>
    /// <param name="navigation">The root of the navigation hierarchy.</param>
    protected abstract void InitNavigation(NavigationGroup navigation);

    /// <summary>Initialise the routing for the web application template.</summary>
    protected override void InitRouting()
    {
      base.InitRouting();

      RouteTable.Routes.MapRoute("Navigation", "{controller}/{action}/{id}",
        new { controller = "Navigation", action = "GoTo" });



    }

    /// <summary>Initialise the web application template bundles.</summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off registration of the
    ///     web application template bundles.
    ///   </para>
    /// </remarks>
    private void InitTemplateBundles()
    {
      // Setup the resource bundles
      BundleTable.VirtualPathProvider = new TemplateVirtualPathProvider();
      BundleTable.EnableOptimizations = true;
      BundleTable.Bundles.FileExtensionReplacementList.Clear();

      // Template fonts bundle
      var templateFontsBundle = new Bundle("~/bundles/template/css/template-fonts", new CssMinify());

      templateFontsBundle.Include("~/template/resources/css/linecons.css");
      templateFontsBundle.Include("~/template/resources/css/font-awesome.css");

      BundleTable.Bundles.Add(templateFontsBundle);

      // Template CSS bundle
      var templateCssBundle = new Bundle("~/bundles/template/css/template", new CssMinify());

      templateCssBundle.Include("~/template/resources/css/template-bootstrap.css");
      templateCssBundle.Include("~/template/resources/css/template-core.css");
      templateCssBundle.Include("~/template/resources/css/template-forms.css");
      templateCssBundle.Include("~/template/resources/css/template-components.css");
      templateCssBundle.Include("~/template/resources/css/template.css");

      BundleTable.Bundles.Add(templateCssBundle);

      // Template thirdparty CSS bundle
      var templateThirdpartyCssBundle = new Bundle("~/bundles/template/css/template-thirdparty", new CssMinify());

      templateThirdpartyCssBundle.Include("~/template/resources/thirdparty/daterangepicker/daterangepicker-bs3.css");
      templateThirdpartyCssBundle.Include("~/template/resources/thirdparty/select2/select2.css");
      templateThirdpartyCssBundle.Include("~/template/resources/thirdparty/select2/select2-bootstrap.css");

      BundleTable.Bundles.Add(templateThirdpartyCssBundle);

      // Template thirdparty JavaScript bundle
      var templateThirdpartyJavaScriptBundle = new Bundle("~/bundles/template/js/template-thirdparty", new JsMinify());

      templateThirdpartyJavaScriptBundle.Include("~/template/resources/thirdparty/jquery/js/jquery-1.11.3.js");
      templateThirdpartyJavaScriptBundle.Include("~/template/resources/thirdparty/jquery-ui/jquery-ui.js");
      templateThirdpartyJavaScriptBundle.Include(
        "~/template/resources/thirdparty/bootstrap-datepicker/bootstrap-datepicker.js");
      templateThirdpartyJavaScriptBundle.Include(
        "~/template/resources/thirdparty/bootstrap-timepicker/bootstrap-timepicker.js");
      templateThirdpartyJavaScriptBundle.Include("~/template/resources/thirdparty/select2/select2.js");
      templateThirdpartyJavaScriptBundle.Include("~/template/resources/thirdparty/greensock-js/TweenMax.js");
      templateThirdpartyJavaScriptBundle.Include("~/template/resources/thirdparty/moment/moment.js");
      templateThirdpartyJavaScriptBundle.Include(
        "~/template/resources/thirdparty/jquery-selectboxit/jquery.selectBoxIt.min.js");
      templateThirdpartyJavaScriptBundle.Include("~/template/resources/thirdparty/daterangepicker/daterangepicker.js");

      BundleTable.Bundles.Add(templateThirdpartyJavaScriptBundle);

      // Template JavaScript bundle
      var templateJavaScriptBundle = new Bundle("~/bundles/template/js/template", new JsMinify());

      templateJavaScriptBundle.Include("~/template/resources/js/template-bootstrap.js");
      templateJavaScriptBundle.Include("~/template/resources/js/template-resizeable.js");
      templateJavaScriptBundle.Include("~/template/resources/js/template-joinable.js");
      templateJavaScriptBundle.Include("~/template/resources/js/template-api.js");
      templateJavaScriptBundle.Include("~/template/resources/js/template-toggles.js");
      templateJavaScriptBundle.Include("~/template/resources/js/template-custom.js");

      BundleTable.Bundles.Add(templateJavaScriptBundle);
    }
  }
}