﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MMP.Application.Mvc.Template.Navigation;

namespace MMP.Application.Mvc.Template
{
  /// <summary>
  ///   The <b>TemplateHelpers</b> class provides the ASP.NET MVC HTML helpers for the web application
  ///   template.
  /// </summary>
  public static class TemplateHelpers
  {
    /// <summary>Generate the main menu HTML for the web application template.</summary>
    /// <param name="htmlHelper">The HTML helper.</param>
    /// <returns>The main menu HTML for the web application template.</returns>
    public static MvcHtmlString GenerateMainMenuHtml(this HtmlHelper htmlHelper)
    {
      var templateWebApplication =
        HttpContext.Current.ApplicationInstance as ITemplateWebApplication<TemplateWebSession>;

      if (templateWebApplication == null)
      {
        throw new WebApplicationException(
          "Failed to generate the main menu HTML: The HttpApplication instance is not derived from TemplateWebApplication<TemplateWebSession>");
      }

      var templateWebSession = templateWebApplication.TemplateWebSession;

      if (string.IsNullOrEmpty(templateWebSession.CachedMainMenuHtml))
      {
        var buffer = new StringBuilder();

        foreach (var navigationItem in templateWebApplication.Navigation.Items)
        {
          buffer.Append(GenerateMainMenuHtml(templateWebApplication, navigationItem));
        }

        templateWebSession.CachedMainMenuHtml = buffer.ToString();
      }

      return MvcHtmlString.Create(templateWebSession.CachedMainMenuHtml);
    }

    private static string GenerateMainMenuHtml(ITemplateWebApplication<TemplateWebSession> templateWebApplication,
      NavigationItem navigationItem)
    {
      if (navigationItem is NavigationLink)
      {
        var navigationLink = navigationItem as NavigationLink;

        // Check that the controller associated with the navigation link exists 
        if (!templateWebApplication.ControllerTypes.ContainsKey(navigationLink.ControllerName))
        {
          throw new WebApplicationException(
            $"Failed to find the controller ({navigationLink.ControllerName}) for the navigation link ({navigationLink.Name})");
        }

        try
        {
          var controllerType = templateWebApplication.ControllerTypes[navigationLink.ControllerName];

          // Check that the action method associated with the navigation link exists
          var actionMethodInfo = controllerType.GetMethod(navigationLink.ActionName);

          if (actionMethodInfo == null)
          {
            throw new WebApplicationException(
              $"The action method action method ({navigationLink.ActionName}) does not exist on the controller ({navigationLink.ControllerName})");
          }

          var securityAttributes =
            actionMethodInfo.GetCustomAttributes(typeof (ActionSecurityAttribute), true) as ActionSecurityAttribute[];

          if ((securityAttributes != null) && (securityAttributes.Length > 0))
          {
            foreach (var securityAttribute in securityAttributes)
            {
              if (securityAttribute is SecuredAttribute)
              {
                if (
                  templateWebApplication.TemplateWebSession.HasAcccessToFunction(
                    (securityAttribute as SecuredAttribute).FunctionCode))
                {
                  return "";
                }
              }
              else if (securityAttribute is SecuredAnonymousAttribute)
              {
                if (!templateWebApplication.TemplateWebSession.IsUserLoggedIn)
                {
                  return "";
                }
              }
              else if (securityAttribute is AnonymousOnlyAttribute)
              {
                if (templateWebApplication.TemplateWebSession.IsUserLoggedIn)
                {
                  return "";
                }
              }
            }
          }
        }
        catch (Exception ex)
        {
          throw new WebApplicationException(
            $"Failed to find the action method ({navigationLink.ActionName}) on the controller ({navigationLink.ControllerName}) for the link ({navigationLink.Name})",
            ex);
        }

        var buffer = new StringBuilder("<li>");

        var aTag = new TagBuilder("a");

        var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

        var actionUrl = urlHelper.Action("GoTo", "Navigation", new RouteValueDictionary {["Id"] = navigationItem.Id});

        aTag.MergeAttribute("href", actionUrl);

        if (!string.IsNullOrEmpty(navigationItem.IconClass))
        {
          var iTag = new TagBuilder("i");
          iTag.MergeAttribute("class", navigationItem.IconClass);

          aTag.InnerHtml += iTag.ToString();
        }

        var spanTag = new TagBuilder("span");
        spanTag.MergeAttribute("class", "title");
        spanTag.InnerHtml += navigationItem.Name;

        aTag.InnerHtml += spanTag.ToString();

        buffer.Append(aTag);

        buffer.Append("</li>");

        return buffer.ToString();
      }

      var navigationGroup = navigationItem as NavigationGroup;

      if (navigationGroup != null)
      {
        var buffer = new StringBuilder("<li>");

        var aTag = new TagBuilder("a");

        aTag.MergeAttribute("href", "#");

        if (!string.IsNullOrEmpty(navigationGroup.IconClass))
        {
          var iTag = new TagBuilder("i");
          iTag.MergeAttribute("class", navigationGroup.IconClass);

          aTag.InnerHtml += iTag.ToString();
        }

        var spanTag = new TagBuilder("span");
        spanTag.MergeAttribute("class", "title");
        spanTag.InnerHtml += navigationGroup.Name;

        aTag.InnerHtml += spanTag.ToString();

        buffer.Append(aTag);

        buffer.Append("<ul>");

        foreach (var childNavigationIten in navigationGroup.Items)
        {
          buffer.Append(GenerateMainMenuHtml(templateWebApplication, childNavigationIten));
        }

        buffer.Append("</ul>");

        buffer.Append("</li>");

        return buffer.ToString();
      }

      throw new WebApplicationException("Unknown navigation item (" + navigationItem.Name + ") with type (" +
                                        navigationItem.GetType().FullName + ")");
    }
  }
}