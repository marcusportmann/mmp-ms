﻿namespace MMP.Application.Mvc.Template
{
  /// <summary>
  ///   The <b>TemplateResource</b> class stores the date for a web application template resource.
  /// </summary>
  public class TemplateResource
  {
    /// <summary>The content type for the template resource.</summary>
    public string ContentType { get; set; }

    /// <summary>The data for the template resource.</summary>
    public byte[] Data { get; set; }
  }
}