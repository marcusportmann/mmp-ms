﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Web;

namespace MMP.Application.Mvc.Template
{
  /// <summary>
  ///   The <b>TemplateWebSession</b> provides strongly-typed access to the session information for a
  ///   user accessing an ASP.NET MVC web application that makes use of the web application template.
  /// </summary>
  public class TemplateWebSession : WebSession
  {
    /// <summary>
    ///  The cached main menu HTML.
    /// </summary>
    public string CachedMainMenuHtml { get; set; }

    /// <summary>Returns the <b>TemplateWebSession</b> instance for the current user.</summary>
    /// <returns>The <b>TemplateWebSession</b> instance for the current user.</returns>
    public static TemplateWebSession TemplateWebSessionForCurrentUser
    {
      get
      {
        var applicationInstance = HttpContext.Current.ApplicationInstance;

        var templateWebSessionForCurrentUserProperty =
          applicationInstance.GetType().GetProperty("TemplateWebSessionForCurrentUser");

        return templateWebSessionForCurrentUserProperty?.GetValue(applicationInstance) as TemplateWebSession;
      }
    }
  }
}