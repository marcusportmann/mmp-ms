﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Web.Mvc;

namespace MMP.Application.Mvc.Template.Controllers
{
  /// <summary>
  ///   The <b>LayoutController</b> class provides the ASP.NET MVC controller for the AngularJS layouts
  ///   for the web application template.
  /// </summary>
  public class LayoutController : Controller
  {
    public ActionResult ApplicationBody()
    {
      return View("~/Template/Views/Layout/ApplicationBody.cshtml");
    }
  }
}