﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

namespace MMP.Application.Mvc.Template
{
  /// <summary>
  ///   The <b>TemplateVirtualPathProvider</b> implements an ASP.NET <b>VirtualPathProvider</b> that
  ///   provides access to web application template resources.
  /// </summary>
  public class TemplateVirtualPathProvider : VirtualPathProvider
  {
    public override bool FileExists(string virtualPath)
    {
      return (TemplateVirtualFile.IsTemplateResourcePath(virtualPath) || base.FileExists(virtualPath));
    }

    public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies,
      DateTime utcStart)
    {
      if (TemplateVirtualFile.IsTemplateResourcePath(virtualPath))
      {
        return null;
      }

      return base.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
    }

    public override VirtualFile GetFile(string virtualPath)
    {
      if (TemplateVirtualFile.IsTemplateResourcePath(virtualPath))
      {
        return new TemplateVirtualFile(virtualPath);
      }

      return base.GetFile(virtualPath);
    }

  }
}