﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace MMP.Application.Mvc.Template
{
  /// <summary>
  ///   The <b>TemplateVirtualFile</b> implements an ASP.NET <b>VirtualFile</b> that provides access to a
  ///   web application template resource.
  /// </summary>
  internal class TemplateVirtualFile : VirtualFile
  {
    private static readonly Dictionary<string, TemplateResource> TEMPLATE_RESOURCES =
      new Dictionary<string, TemplateResource>();

    public static readonly string TEMPLATE_VIEWS_REQUEST_PATH_PREFIX = "~/template/Views";
    public static readonly string TEMPLATE_RESOURCES_REQUEST_PATH_PREFIX = "~/template/resources/";
    private static readonly string TEMPLATE_RESOURCE_PREFIX = typeof (TemplateVirtualFile).Namespace;
    private readonly string _path;

    public TemplateVirtualFile(string virtualPath) : base(virtualPath)
    {
      _path = VirtualPathUtility.ToAppRelative(virtualPath);
    }

    [SuppressMessage("ReSharper", "InconsistentlySynchronizedField")]
    public static TemplateResource GetTemplateResource(string resourcePath, bool cacheResource)
    {
      var path = VirtualPathUtility.ToAppRelative(resourcePath);

      if (cacheResource)
      {
        if (TEMPLATE_RESOURCES.ContainsKey(path))
        {
          return TEMPLATE_RESOURCES[path];
        }
      }

      lock (TEMPLATE_RESOURCES)
      {
        if (cacheResource)
        {
          if (TEMPLATE_RESOURCES.ContainsKey(path))
          {
            return TEMPLATE_RESOURCES[path];
          }
        }

        var requestPath = VirtualPathUtility.ToAppRelative(path);

        if ((requestPath.StartsWith(TEMPLATE_VIEWS_REQUEST_PATH_PREFIX, StringComparison.InvariantCultureIgnoreCase)) ||
            (requestPath.StartsWith(TEMPLATE_RESOURCES_REQUEST_PATH_PREFIX,
              StringComparison.InvariantCultureIgnoreCase)))
        {
          var requestPathComponents = requestPath.Split('/');

          if (requestPathComponents.Length > 2)
          {
            // Resource paths are case sensitive so make sure we use the correct case for "Resources"
            if (requestPath.StartsWith(TEMPLATE_RESOURCES_REQUEST_PATH_PREFIX,
              StringComparison.InvariantCultureIgnoreCase))
            {
              requestPathComponents[2] = "Resources";
            }

            /*
             * Replace any dashes in the namespace components with underscores to obtain the
             * correct namespace name.
             */
            for (var i = 0; i < (requestPathComponents.Length - 1); i++)
            {
              requestPathComponents[i] = requestPathComponents[i].Replace("-", "_");
              requestPathComponents[i] = requestPathComponents[i].Replace(".", "._");
            }

            var resourceName = requestPathComponents[requestPathComponents.Length - 1];

            var namespaceName = new StringBuilder(TEMPLATE_RESOURCE_PREFIX);

            for (var i = 2; i < (requestPathComponents.Length - 1); i++)
            {
              namespaceName.Append(".");
              namespaceName.Append(requestPathComponents[i]);
            }

            var stream =
              typeof (TemplateStaticFileHandler).Assembly.GetManifestResourceStream(namespaceName + "." + resourceName);

            if (stream == null)
            {
              var possibleMatchingResources =
                typeof (TemplateStaticFileHandler).Assembly.GetManifestResourceNames()
                  .Where(name => name.EndsWith(resourceName))
                  .ToArray();


              if (possibleMatchingResources.Length == 1)
              {
                if (possibleMatchingResources[0].StartsWith(TEMPLATE_RESOURCE_PREFIX))
                {
                  stream =
                    typeof (TemplateStaticFileHandler).Assembly.GetManifestResourceStream(possibleMatchingResources[0]);
                }
              }
            }

            if (stream != null)
            {
              var templateResource = new TemplateResource();

              if (resourceName.EndsWith(".css"))
              {
                templateResource.ContentType = "text/css";
              }
              else if (resourceName.EndsWith(".js"))
              {
                templateResource.ContentType = "application/javascript";
              }
              else if (resourceName.EndsWith(".gif"))
              {
                templateResource.ContentType = "image/gif";
              }
              else if (resourceName.EndsWith(".jpg"))
              {
                templateResource.ContentType = "image/jpeg";
              }
              else if (resourceName.EndsWith(".png"))
              {
                templateResource.ContentType = "image/png";
              }
              else if (resourceName.EndsWith(".eot"))
              {
                templateResource.ContentType = "application/vnd.ms-fontobject";
              }
              else if (resourceName.EndsWith(".otf"))
              {
                templateResource.ContentType = "application/font-sfnt";
              }
              else if (resourceName.EndsWith(".svg"))
              {
                templateResource.ContentType = "image/svg+xml";
              }
              else if (resourceName.EndsWith(".ttf"))
              {
                templateResource.ContentType = "application/octet-stream";
              }
              else if (resourceName.EndsWith(".woff"))
              {
                templateResource.ContentType = "application/font-woff";
              }

              var buffer = new byte[16*1024];
              using (var ms = new MemoryStream())
              {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                  ms.Write(buffer, 0, read);
                }

                templateResource.Data = ms.ToArray();
              }

              TEMPLATE_RESOURCES.Add(path, templateResource);

              return templateResource;
            }
          }
        }
      }

      if (!path.EndsWith(".map", StringComparison.OrdinalIgnoreCase))
      {
        Debug.WriteLine($"The template resource with path \"{path}\" could not be found");
      }

      if (cacheResource)
      {
        TEMPLATE_RESOURCES.Add(path, null);
      }

      return null;
    }

    public static bool IsTemplateResourcePath(string resourcePath)
    {
      var path = VirtualPathUtility.ToAppRelative(resourcePath);

      return (path.StartsWith(TEMPLATE_VIEWS_REQUEST_PATH_PREFIX, StringComparison.InvariantCultureIgnoreCase) ||
              path.StartsWith(TEMPLATE_RESOURCES_REQUEST_PATH_PREFIX, StringComparison.InvariantCultureIgnoreCase));
    }

    public override Stream Open()
    {
      var templateResource = GetTemplateResource(_path, false);

      if (templateResource != null)
      {
        return new MemoryStream(templateResource.Data);
      }
      throw new WebApplicationException($"Failed to retrieve the template resource with path \"{_path}\"");
    }
  }
}