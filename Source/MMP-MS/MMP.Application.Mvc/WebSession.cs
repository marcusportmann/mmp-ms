﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;

namespace MMP.Application.Mvc
{
  /// <summary>
  ///   The <b>WebSession</b> provides strongly-typed access to the session information for a user
  ///   accessing an ASP.NET MVC web application.
  /// </summary>
  public class WebSession
  {
    /// <summary>The empty list of group names for anonymous users.</summary>
    public static readonly ISet<string> NO_GROUP_NAMES = new HashSet<string>();

    /// <summary>The empty list of function codes for anonymous users.</summary>
    public static readonly ISet<string> NO_FUNCTION_CODES = new HashSet<string>();

    /// <summary>
    ///   The function codes identifying the functionality assigned to the logged in user.
    /// </summary>
    public ISet<string> FunctionCodes { get; set; } = NO_FUNCTION_CODES;

    /// <summary>The names of the groups the logged in user is a member of.</summary>
    public ISet<string> GroupNames { get; set; } = NO_GROUP_NAMES;

    /// <summary>Is the user logged in?</summary>
    public bool IsUserLoggedIn => (!string.IsNullOrEmpty(Username));

    /// <summary>
    ///   The organisation code identifying the organisational context for the user associated with the web
    ///   session.
    /// </summary>
    public string Organisation { get; set; }

    /// <summary>
    ///   The unique user ID for the logged in user associated with the web session -1 if this is an
    ///   'anonymous' web session.
    /// </summary>
    public long UserId { get; set; } = -1;

    /// <summary>The username for the logged in user associated with the web session.</summary>
    public string Username { get; set; }

    /// <summary>The user properties for the logged in user associated with the web session.</summary>
    public IDictionary<string, object> UserProperties { get; } = new Dictionary<string, object>();

    /// <summary>
    ///   Returns the <b>string</b> value for the user property with the specified name for the logged in
    ///   user associated with the web session or <b>null</b> if the user property does not exist.
    /// </summary>
    /// <param name="name">The name of the user property.</param>
    /// <returns>
    ///   The <b>string</b> value for the user property with the specified name for the logged in user
    ///   associated with the web session or <b>null</b> if the user property does not exist.
    /// </returns>
    public string GetUserPropertyAsString(string name)
    {
      if (UserProperties.ContainsKey(name))
      {
        return UserProperties[name].ToString();
      }
      return null;
    }

    /// <summary>
    ///   Check whether the logged in user associated with the web session as access to the function with
    ///   the specified code.
    ///   <remarks>This method always returns false for anonymous users.</remarks>
    /// </summary>
    /// <param name="functionCode">The function code uniquely identifying the function.</param>
    /// <returns>
    ///   <b>True</b> if the logged in user has access to the function or <b>false</b> otherwise.
    /// </returns>
    public bool HasAcccessToFunction(string functionCode)
    {
      return functionCode == WebPage.FUNCTION_CODE_SECURE_ANONYMOUS_ACCESS
        ? IsUserLoggedIn
        : FunctionCodes.Contains(functionCode);
    }

    /// <summary>
    ///   Checks whether the user associated with the web session is a member of the group given by the
    ///   specified group name.
    /// </summary>
    /// <param name="groupName">The name of the group uniquely identifying the group.</param>
    /// <returns>
    ///   <b>True</b> if the user associated with the web session is a member of the group given by the
    ///   specified group name or <b>false</b> otherwise.
    /// </returns>
    public bool IsUserInGroup(string groupName)
    {
      return !string.IsNullOrEmpty(Username) && GroupNames.Contains(groupName);
    }

    /// <summary>Logout the user.</summary>
    public void LogoutUser()
    {
      Username = null;
      GroupNames = NO_GROUP_NAMES;
      FunctionCodes = NO_FUNCTION_CODES;
    }
  }
}