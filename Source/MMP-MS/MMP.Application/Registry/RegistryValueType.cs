﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMP.Application.Registry
{
  /// <summary>The enumeration giving the possible registry value types.</summary>
  [CLSCompliant(true)]
  public enum RegistryValueType
  {
    /// <summary>None</summary>
    None = -1,

    /// <summary>String</summary>
    String = 1,

    /// <summary>Integer</summary>
    Integer = 2,

    /// <summary>Decimal</summary>
    Decimal = 3,

    /// <summary>Binary</summary>
    Binary = 4
  }
}
