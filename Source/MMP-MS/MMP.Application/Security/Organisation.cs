﻿/*
 * Copyright 2014 Marcus Portmann
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMP.Application.Security
{
  /// <summary>
  ///   The <b>Organisation</b> class stores the information for an organisation.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public class Organisation
  {
    /// <summary>
    ///   The code uniquely identifying the organisation.
    /// </summary>
    public string Code { get; set; }

    /// <summary>
    ///   The description for the organisation.
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    ///   The unique numeric ID for the organisation.
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    ///   The name of the organisation.
    /// </summary>
    public string Name { get; set; }
  }
}
