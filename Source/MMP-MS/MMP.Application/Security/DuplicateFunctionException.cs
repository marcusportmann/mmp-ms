﻿/*
 * Copyright 2014 Marcus Portmann
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Runtime.Serialization;

namespace MMP.Application.Security
{
  /// <summary>
  ///   The <b>DuplicateFunctionException</b> exception is thrown to indicate that a security operation
  ///   failed as a result of a duplicate authorised function error.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class DuplicateFunctionException : Exception
  {
    /// <summary>Constructs a new <b>DuplicateFunctionException</b> with the default message.</summary>
    public DuplicateFunctionException() : this("Duplicate Function Error")
    {
    }

    /// <summary>Constructs a new <b>DuplicateFunctionException</b> using serialization.</summary>
    /// <param name="info">The serialization info.</param>
    /// <param name="context">The streaming context.</param>
    public DuplicateFunctionException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    /// <summary>
    ///   Constructs a new <b>DuplicateFunctionException</b> with the default message and specified cause.
    /// </summary>
    /// <param name="cause">The cause saved for later retrieval via the <b>InnerException</b> property.</param>
    public DuplicateFunctionException(Exception cause) : this("Duplicate Function Error", cause)
    {
    }

    /// <summary>Constructs a new <b>DuplicateFunctionException</b> with the specified message.</summary>
    /// <param name="message">The message saved for later retrieval via <b>Message</b> property.</param>
    public DuplicateFunctionException(string message) : this(message, null)
    {
    }

    /// <summary>
    ///   Constructs a new <b>DuplicateFunctionException</b> with the specified message and cause.
    /// </summary>
    /// <param name="message">The message saved for later retrieval via the <b>Message</b> property.</param>
    /// <param name="cause">The cause saved for later retrieval via the <b>InnerException</b> property.</param>
    public DuplicateFunctionException(string message, Exception cause) : base(message, cause)
    {
    }
  }
}