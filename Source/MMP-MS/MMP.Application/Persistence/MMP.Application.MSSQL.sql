-- -------------------------------------------------------------------------------------------------
-- DROP INDEXES
-- -------------------------------------------------------------------------------------------------
--IF OBJECT_ID(N'MMP.SMS') IS NOT NULL DROP TABLE [MMP].[SMS];



-- -------------------------------------------------------------------------------------------------
-- DROP TABLES
-- -------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'MMP.SMS') IS NOT NULL DROP TABLE [MMP].[SMS];
IF OBJECT_ID(N'MMP.ErrorReport') IS NOT NULL DROP TABLE [MMP].[ErrorReport];
IF OBJECT_ID(N'MMP.MessageAudit') IS NOT NULL DROP TABLE [MMP].[MessageAudit];
IF OBJECT_ID(N'MMP.ArchivedMessage') IS NOT NULL DROP TABLE [MMP].[ArchivedMessage];
IF OBJECT_ID(N'MMP.MessagePart') IS NOT NULL DROP TABLE [MMP].[MessagePart];
IF OBJECT_ID(N'MMP.Message') IS NOT NULL DROP TABLE [MMP].[Message];
IF OBJECT_ID(N'MMP.MessageType') IS NOT NULL DROP TABLE [MMP].[MessageType];
IF OBJECT_ID(N'MMP.MessageStatus') IS NOT NULL DROP TABLE [MMP].[MessageStatus];
IF OBJECT_ID(N'MMP.CachedCode') IS NOT NULL DROP TABLE [MMP].[CachedCode];
IF OBJECT_ID(N'MMP.CachedCodeCategory') IS NOT NULL DROP TABLE [MMP].[CachedCodeCategory];
IF OBJECT_ID(N'MMP.Code') IS NOT NULL DROP TABLE [MMP].[Code];
IF OBJECT_ID(N'MMP.CodeCategory') IS NOT NULL DROP TABLE [MMP].[CodeCategory];
IF OBJECT_ID(N'MMP.ScheduledTaskParameter') IS NOT NULL DROP TABLE [MMP].[ScheduledTaskParameter];
IF OBJECT_ID(N'MMP.ScheduledTask') IS NOT NULL DROP TABLE [MMP].[ScheduledTask];
IF OBJECT_ID(N'MMP.FunctionTemplateMap') IS NOT NULL DROP TABLE [MMP].[FunctionTemplateMap];
IF OBJECT_ID(N'MMP.ExternalFunctionGroupMap') IS NOT NULL DROP TABLE [MMP].[ExternalFunctionGroupMap];
IF OBJECT_ID(N'MMP.FunctionGroupMap') IS NOT NULL DROP TABLE [MMP].[FunctionGroupMap];
IF OBJECT_ID(N'MMP.ExternalFunctionUserMap') IS NOT NULL DROP TABLE [MMP].[ExternalFunctionUserMap];
IF OBJECT_ID(N'MMP.FunctionUserMap') IS NOT NULL DROP TABLE [MMP].[FunctionUserMap];
IF OBJECT_ID(N'MMP.FunctionTemplate') IS NOT NULL DROP TABLE [MMP].[FunctionTemplate];
IF OBJECT_ID(N'MMP.Function') IS NOT NULL DROP TABLE [MMP].[Function];
IF OBJECT_ID(N'MMP.UserOrganisationMap') IS NOT NULL DROP TABLE [MMP].[UserOrganisationMap];
IF OBJECT_ID(N'MMP.UserGroupMap') IS NOT NULL DROP TABLE [MMP].[UserGroupMap];
IF OBJECT_ID(N'MMP.ExternalGroup') IS NOT NULL DROP TABLE [MMP].[ExternalGroup];
IF OBJECT_ID(N'MMP.Group') IS NOT NULL DROP TABLE [MMP].[Group];
IF OBJECT_ID(N'MMP.PasswordHistory') IS NOT NULL DROP TABLE [MMP].[PasswordHistory];
IF OBJECT_ID(N'MMP.ExternalUser') IS NOT NULL DROP TABLE [MMP].[ExternalUser];
IF OBJECT_ID(N'MMP.User') IS NOT NULL DROP TABLE [MMP].[User];
IF OBJECT_ID(N'MMP.Organisation') IS NOT NULL DROP TABLE [MMP].[Organisation];
IF OBJECT_ID(N'MMP.ServiceRegistry') IS NOT NULL DROP TABLE [MMP].[ServiceRegistry];
IF OBJECT_ID(N'MMP.Registry') IS NOT NULL DROP TABLE [MMP].[Registry];
IF OBJECT_ID(N'MMP.LockKeepAlive') IS NOT NULL DROP TABLE [MMP].[LockKeepAlive];
IF OBJECT_ID(N'MMP.IdGenerator') IS NOT NULL DROP TABLE [MMP].[IDGenerator];

GO



-- -------------------------------------------------------------------------------------------------
-- CREATE SCHEMAS
-- -------------------------------------------------------------------------------------------------
IF SCHEMA_ID(N'MMP') IS NULL EXECUTE(N'CREATE SCHEMA [MMP]');

GO



-- -------------------------------------------------------------------------------------------------
-- CREATE TABLES
-- -------------------------------------------------------------------------------------------------
CREATE TABLE [MMP].[IdGenerator] (
  [Name]     NVARCHAR(100) NOT NULL,
  [Current]  BIGINT NOT NULL,

  CONSTRAINT [PK_MMP_IdGenerator_Name] PRIMARY KEY CLUSTERED ([Name])
);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name giving the type of entity associated with the generated ID',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'IdGenerator', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The current ID for the type',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'IdGenerator', @level2type=N'COLUMN', @level2name=N'Current';

GO



CREATE TABLE [MMP].[LockKeepAlive] (
  [LockName]   NVARCHAR(100) NOT NULL,
  [KeepAlive]  DATETIME NOT NULL,

  PRIMARY KEY CLUSTERED ([LockName])
);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the entity that will lock other entities for processing',
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'LockKeepAlive', @level2type=N'COLUMN',@level2name=N'LockName';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time that the entity last confirmed that it was still alive and capable of processing locked entities',
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'LockKeepAlive', @level2type=N'COLUMN',@level2name=N'KeepAlive';

GO



CREATE TABLE [MMP].[Registry] (
  [Id]         UNIQUEIDENTIFIER NOT NULL,
  [ParentId]   UNIQUEIDENTIFIER,
  [EntryType]  INTEGER NOT NULL,
  [Name]       NVARCHAR(250) NOT NULL,
  [SValue]     NVARCHAR(1024),
  [IValue]     INTEGER,
  [DValue]     DECIMAL(16,12),
  [BValue]     VARBINARY(MAX),

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE INDEX [IX_MMP_Registry_ParentId]
  ON [MMP].[Registry]
  ([ParentId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the registry entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Registry', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The ID of the parent entry for the registry entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Registry', @level2type=N'COLUMN', @level2name=N'ParentId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The type of registry entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Registry', @level2type=N'COLUMN', @level2name=N'EntryType';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the registry entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Registry', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The string value for the registry entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Registry', @level2type=N'COLUMN', @level2name=N'SValue';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The integer value for the registry entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Registry', @level2type=N'COLUMN', @level2name=N'IValue';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The decimal value for the registry entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Registry', @level2type=N'COLUMN', @level2name=N'DValue';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The binary value for the registry entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Registry', @level2type=N'COLUMN', @level2name=N'BValue';

GO



--CREATE TABLE MMP.SERVICE_REGISTRY (
--  NAME                  VARCHAR(255) NOT NULL,
--  SECURITY_TYPE         INTEGER NOT NULL,
--  REQUIRES_USER_TOKEN   CHAR(1) NOT NULL,
--  SUPPORTS_COMPRESSION  CHAR(1) NOT NULL,
--  ENDPOINT              VARCHAR(512) NOT NULL,
--    SERVICE_CLASS         VARCHAR(512) NOT NULL,
--    WSDL_LOCATION         VARCHAR(512) NOT NULL,
--    USERNAME              VARCHAR(100),
--    PASSWORD              VARCHAR(100),
--  
--  	PRIMARY KEY (NAME)
--  );
--  
--  COMMENT ON COLUMN MMP.SERVICE_REGISTRY.NAME
--  IS 'The name used to uniquely identify the web service';
--  
--  COMMENT ON COLUMN MMP.SERVICE_REGISTRY.SECURITY_TYPE
--    IS 'The type of security model implemented by the web service i.e. 0 = None, 1 = WS-Security X509 Certificates, 2 = WS-Security Username Token, 3 = Client SSL, 4 = HTTP Authentication';
--  
--  COMMENT ON COLUMN MMP.SERVICE_REGISTRY.REQUIRES_USER_TOKEN
--    IS 'Does the web service require a user security token';
--  
--  COMMENT ON COLUMN MMP.SERVICE_REGISTRY.SUPPORTS_COMPRESSION
--    IS 'Does the web service support compression';
--  
--  COMMENT ON COLUMN MMP.SERVICE_REGISTRY.ENDPOINT
--    IS 'The endpoint for the web service';
--  
--  COMMENT ON COLUMN MMP.SERVICE_REGISTRY.SERVICE_CLASS
--    IS 'The fully qualified name of the Java service class';
--  
--  COMMENT ON COLUMN MMP.SERVICE_REGISTRY.WSDL_LOCATION
--    IS 'The location of the WSDL defining the web service on the classpath';



CREATE TABLE [MMP].[Organisation] (
  [Id]           INTEGER NOT NULL,
  [Code]         NVARCHAR(40) NOT NULL,
  [Name]         NVARCHAR(256) NOT NULL,
  [Description]  NVARCHAR(512),

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE UNIQUE INDEX [IX_MMP_Organisation_Code]
  ON [MMP].[Organisation]
  ([Code]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the organisation used to associate the organisation with other database entities',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Organisation', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The code uniquely identifying the organisation',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Organisation', @level2type=N'COLUMN', @level2name=N'Code';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the organisation',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Organisation', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'A description for the organisation',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Organisation', @level2type=N'COLUMN', @level2name=N'Description';

GO



CREATE TABLE [MMP].[User] (
  [Id]                INTEGER NOT NULL,
  [Username]          NVARCHAR(100) NOT NULL,
  [Password]          NVARCHAR(100),
  [Title]             NVARCHAR(100),
  [FirstNames]        NVARCHAR(100),
  [LastName]          NVARCHAR(100),
  [Phone]             NVARCHAR(30),
  [Fax]               NVARCHAR(30),
  [Mobile]            NVARCHAR(30),
  [EMail]             NVARCHAR(200),
  [PasswordAttempts]  INTEGER,
  [PasswordExpiry]    DATETIME,
  [Description]       NVARCHAR(250),

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE UNIQUE INDEX [IX_MMP_User_Username]
  ON [MMP].[User]
  ([Username]);

CREATE INDEX [IX_MMP_User_EMail]
  ON [MMP].[User]
  ([EMail]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the user used to associate the user with other database entities',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The username for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'Username';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The password for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'Password';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The title for the user e.g. Mr, Mrs, etc',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'Title';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The first name(s) / forname(s) for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'FirstNames';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The last name / surname for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'LastName';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The telephone number for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'Phone';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The fax number for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'Fax';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The mobile number for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'Mobile';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The e-mail address for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'EMail';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The number of failed attempts to authenticate the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'PasswordAttempts';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time that the user''s password expires',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'PasswordExpiry';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'A description for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'User', @level2type=N'COLUMN', @level2name=N'Description';

GO



CREATE TABLE [MMP].[ExternalUser] (
  [Id]        INTEGER NOT NULL,
  [Username]  NVARCHAR(100) NOT NULL,

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE UNIQUE INDEX [IX_MMP_ExternalUser_Username]
  ON [MMP].[ExternalUser]
  ([Username]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the external user used to associate the user with other database entities',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalUser', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The username for the external user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalUser', @level2type=N'COLUMN', @level2name=N'Username';

GO



CREATE TABLE [MMP].[PasswordHistory] (
  [Id]        BIGINT NOT NULL,
  [UserId]    INTEGER NOT NULL,
  [Changed]   DATETIME NOT NULL,
  [Password]  NVARCHAR(100),

  PRIMARY KEY CLUSTERED ([Id]),
  CONSTRAINT [FK_MMP_PasswordHistory_UserId] FOREIGN KEY ([UserId]) REFERENCES [MMP].[User]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_PasswordHistory_UserId]
  ON [MMP].[PasswordHistory]
  ([UserId]);

CREATE INDEX [IX_MMP_PasswordHistory_Changed]
  ON [MMP].[PasswordHistory]
  ([Changed]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the password history entry',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'PasswordHistory', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'PasswordHistory', @level2type=N'COLUMN', @level2name=N'UserId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'When the password change took place',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'PasswordHistory', @level2type=N'COLUMN', @level2name=N'Changed';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The password for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'PasswordHistory', @level2type=N'COLUMN', @level2name=N'Password';

GO



CREATE TABLE [MMP].[Group] (
  [Id]           INTEGER NOT NULL,
  [Name]         NVARCHAR(100) NOT NULL,
  [Description]  NVARCHAR(250),

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE UNIQUE INDEX [IX_MMP_Group_Name]
  ON [MMP].[Group]
  ([Name]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the group used to associate the group with other database entities',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Group', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique group name used to identify the group',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Group', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'A description for the group',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Group', @level2type=N'COLUMN', @level2name=N'Description';

GO



CREATE TABLE [MMP].[ExternalGroup] (
  [Id]    INTEGER NOT NULL,
  [Name]  NVARCHAR(255) NOT NULL,

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE UNIQUE INDEX [IX_MMP_ExternalGroup_Name]
  ON [MMP].[ExternalGroup]
  ([Name]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the external group used to associate the group with other database entities',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalGroup', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique group name used to identify the external group',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalGroup', @level2type=N'COLUMN', @level2name=N'Name';

GO



CREATE TABLE [MMP].[UserGroupMap] (
  [GroupId]         INTEGER NOT NULL,
  [UserId]          INTEGER NOT NULL,
  [OrganisationId]  INTEGER NOT NULL,

  PRIMARY KEY ([GroupId], [UserId], [OrganisationId]),
  CONSTRAINT [FK_MMP_UserGroupMap_GroupId] FOREIGN KEY ([GroupId]) REFERENCES [MMP].[Group]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_UserGroupMap_UserId] FOREIGN KEY ([UserId]) REFERENCES [MMP].[User]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_UserGroupMap_OrganisationId] FOREIGN KEY ([OrganisationId]) REFERENCES [MMP].[Organisation]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_UserGroupMap_GroupId]
  ON [MMP].[UserGroupMap]
  ([GroupId]);

CREATE INDEX [IX_MMP_UserGroupMap_UserId]
  ON [MMP].[UserGroupMap]
  ([UserId]);

CREATE INDEX [IX_MMP_UserGroupMap_OrganisationId]
  ON [MMP].[UserGroupMap]
  ([OrganisationId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the group',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'UserGroupMap', @level2type=N'COLUMN', @level2name=N'GroupId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'UserGroupMap', @level2type=N'COLUMN', @level2name=N'UserId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the organisation',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'UserGroupMap', @level2type=N'COLUMN', @level2name=N'OrganisationId';

GO



CREATE TABLE [MMP].[UserOrganisationMap] (
  [UserId]          INTEGER NOT NULL,
  [OrganisationId]  INTEGER NOT NULL,

  PRIMARY KEY ([UserId], [OrganisationId]),
  CONSTRAINT [FK_MMP_UserOrganisationMap_UserId] FOREIGN KEY ([UserId]) REFERENCES [MMP].[User]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_UserOrganisationMap_OrganisationId] FOREIGN KEY ([OrganisationId]) REFERENCES [MMP].[Organisation]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_UserOrganisationMap_UserId]
  ON [MMP].[UserOrganisationMap]
  ([UserId]);

CREATE INDEX [IX_MMP_UserOrganisationMap_OrganisationId]
  ON [MMP].[UserOrganisationMap]
  ([OrganisationId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'UserOrganisationMap', @level2type=N'COLUMN', @level2name=N'UserId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the organisation',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'UserOrganisationMap', @level2type=N'COLUMN', @level2name=N'OrganisationId';

GO



CREATE TABLE [MMP].[Function] (
  [Id]           INTEGER NOT NULL,
  [Code]         NVARCHAR(100) NOT NULL,
  [Name]         NVARCHAR(100) NOT NULL,
  [Description]  NVARCHAR(250),

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE UNIQUE INDEX [IX_MMP_Function_Code]
  ON [MMP].[Function]
  ([Code]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the function used to associate the function with other database entities',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Function', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique code used to identify the function',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Function', @level2type=N'COLUMN', @level2name=N'Code';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the function',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Function', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'A description for the function',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Function', @level2type=N'COLUMN', @level2name=N'Description';

GO



CREATE TABLE [MMP].[FunctionTemplate] (
  [Id]           INTEGER NOT NULL,
  [Code]         NVARCHAR(100) NOT NULL,
  [Name]         NVARCHAR(100) NOT NULL,
  [Description]  NVARCHAR(250),

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE UNIQUE INDEX [IX_MMP_FunctionTemplate_Code]
  ON [MMP].[FunctionTemplate]
  ([Code]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the function template used to associate the function with other database entities',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionTemplate', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique code used to identify the function template',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionTemplate', @level2type=N'COLUMN', @level2name=N'Code';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the function template',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionTemplate', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'A description for the function template',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionTemplate', @level2type=N'COLUMN', @level2name=N'Description';

GO



CREATE TABLE [MMP].[FunctionUserMap] (
  [UserId]          INTEGER NOT NULL,
  [FunctionId]      INTEGER NOT NULL,
  [OrganisationId]  INTEGER NOT NULL,

  PRIMARY KEY ([UserId], [FunctionId], [OrganisationId]),
  CONSTRAINT [FK_MMP_FunctionUserMap_UserId] FOREIGN KEY ([UserId]) REFERENCES [MMP].[User]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_FunctionUserMap_FunctionId] FOREIGN KEY ([FunctionId]) REFERENCES [MMP].[Function]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_FunctionUserMap_OrganisationId] FOREIGN KEY ([OrganisationId]) REFERENCES [MMP].[Organisation]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_FunctionUserMap_UserId]
  ON [MMP].[FunctionUserMap]
  ([UserId]);

CREATE INDEX [IX_MMP_FunctionUserMap_FunctionId]
  ON [MMP].[FunctionUserMap]
  ([FunctionId]);

CREATE INDEX [IX_MMP_FunctionUserMap_OrganisationId]
  ON [MMP].[FunctionUserMap]
  ([OrganisationId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionUserMap', @level2type=N'COLUMN', @level2name=N'UserId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the function',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionUserMap', @level2type=N'COLUMN', @level2name=N'FunctionId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the organisation',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionUserMap', @level2type=N'COLUMN', @level2name=N'OrganisationId';

GO



CREATE TABLE [MMP].[ExternalFunctionUserMap] (
  [UserId]          INTEGER NOT NULL,
  [FunctionId]      INTEGER NOT NULL,
  [OrganisationId]  INTEGER NOT NULL,

  PRIMARY KEY ([UserId], [FunctionId], [OrganisationId]),
  CONSTRAINT [FK_MMP_ExternalFunctionUserMap_UserId] FOREIGN KEY ([UserId]) REFERENCES [MMP].[ExternalUser]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_ExternalFunctionUserMap_FunctionId] FOREIGN KEY ([FunctionId]) REFERENCES [MMP].[Function]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_ExternalFunctionUserMap_OrganisationId] FOREIGN KEY ([OrganisationId]) REFERENCES [MMP].[Organisation]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_ExternalFunctionUserMap_UserId]
  ON [MMP].[ExternalFunctionUserMap]
  ([UserId]);

CREATE INDEX [IX_MMP_ExternalFunctionUserMap_FunctionId]
  ON [MMP].[ExternalFunctionUserMap]
  ([FunctionId]);

CREATE INDEX [IX_MMP_ExternalFunctionUserMap_OrganisationId]
  ON [MMP].[ExternalFunctionUserMap]
  ([OrganisationId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the external user',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalFunctionUserMap', @level2type=N'COLUMN', @level2name=N'UserId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the function',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalFunctionUserMap', @level2type=N'COLUMN', @level2name=N'FunctionId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the organisation',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalFunctionUserMap', @level2type=N'COLUMN', @level2name=N'OrganisationId';



CREATE TABLE [MMP].[FunctionGroupMap] (
  [GroupId]     INTEGER NOT NULL,
  [FunctionId]  INTEGER NOT NULL,

  PRIMARY KEY ([GroupId], [FunctionId]),
  CONSTRAINT [FK_MMP_FunctionGroupMap_GroupId] FOREIGN KEY ([GroupId]) REFERENCES [MMP].[Group]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_FunctionGroupMap_FunctionId] FOREIGN KEY ([FunctionId]) REFERENCES [MMP].[Function]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_FunctionGroupMap_GroupId]
  ON [MMP].[FunctionGroupMap]
  ([GroupId]);

CREATE INDEX [IX_MMP_FunctionGroupMap_FunctionId]
  ON [MMP].[FunctionGroupMap]
  ([FunctionId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the group',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionGroupMap', @level2type=N'COLUMN', @level2name=N'GroupId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the function',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionGroupMap', @level2type=N'COLUMN', @level2name=N'FunctionId';

GO



CREATE TABLE [MMP].[ExternalFunctionGroupMap] (
  [GroupId]     INTEGER NOT NULL,
  [FunctionId]  INTEGER NOT NULL,

  PRIMARY KEY ([GroupId], [FunctionId]),
  CONSTRAINT [FK_MMP_ExternalFunctionGroupMap_GroupId] FOREIGN KEY ([GroupId]) REFERENCES [MMP].[ExternalGroup]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_ExternalFunctionGroupMap_FunctionId] FOREIGN KEY ([FunctionId]) REFERENCES [MMP].[Function]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_ExternalFunctionGroupMap_GroupId]
  ON [MMP].[ExternalFunctionGroupMap]
  ([GroupId]);

CREATE INDEX [IX_MMP_ExternalFunctionGroupMap_FunctionId]
  ON [MMP].[ExternalFunctionGroupMap]
  ([FunctionId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the external group',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalFunctionGroupMap', @level2type=N'COLUMN', @level2name=N'GroupId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the function',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ExternalFunctionGroupMap', @level2type=N'COLUMN', @level2name=N'FunctionId';

GO



CREATE TABLE [MMP].[FunctionTemplateMap] (
  [FunctionId]  INTEGER NOT NULL,
  [TemplateId]  INTEGER NOT NULL,

  PRIMARY KEY ([FunctionId], [TemplateId]),
  CONSTRAINT [FK_MMP_FunctionTemplateMap_FunctionId] FOREIGN KEY ([FunctionId]) REFERENCES [MMP].[Function]([Id]) ON DELETE CASCADE,
  CONSTRAINT [FK_MMP_FunctionTemplateMap_TemplateId] FOREIGN KEY ([TemplateId]) REFERENCES [MMP].[FunctionTemplate]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_FunctionTemplateMap_FunctionId]
  ON [MMP].[FunctionTemplateMap]
  ([FunctionId]);

CREATE INDEX [IX_MMP_FunctionTemplateMap_TemplateId]
  ON [MMP].[FunctionTemplateMap]
  ([TemplateId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the function',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionTemplateMap', @level2type=N'COLUMN', @level2name=N'FunctionId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID for the template',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'FunctionTemplateMap', @level2type=N'COLUMN', @level2name=N'TemplateId';

GO



CREATE TABLE [MMP].[ScheduledTask] (
  [Id]                 UNIQUEIDENTIFIER NOT NULL,
  [Name]               NVARCHAR(200) NOT NULL,
  [SchedulingPattern]  NVARCHAR(200) NOT NULL,
  [TaskClass]          NVARCHAR(512) NOT NULL,
  [Status]             INTEGER NOT NULL DEFAULT 1,
  [ExecutionAttempts]  INTEGER NOT NULL DEFAULT 0,
  [LockName]           NVARCHAR(100),
  [LastExecuted]       DATETIME,
  [NextExecution]      DATETIME,
  [Updated]            DATETIME,

  PRIMARY KEY CLUSTERED ([Id])
);

CREATE INDEX [IX_MMP_ScheduledTask_Name]
  ON [MMP].[ScheduledTask]
  ([Name]);

CREATE INDEX [IX_MMP_ScheduledTask_Status]
  ON [MMP].[ScheduledTask]
  ([Status]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the scheduled task',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the scheduled task',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The cron-style scheduling pattern for the scheduled task',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'SchedulingPattern';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The fully qualified name of the Java class that implements the scheduled task',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'TaskClass';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The status of the scheduled task',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask',  @level2type=N'COLUMN', @level2name=N'Status';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The number of times the current execution of the scheduled task has been attempted',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'ExecutionAttempts';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the entity that has locked the scheduled task for execution',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'LockName';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the scheduled task was last executed',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'LastExecuted';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time when the scheduled task will next be executed',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'NextExecution';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the scheduled task was updated',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTask', @level2type=N'COLUMN', @level2name=N'Updated';

GO



CREATE TABLE [MMP].[ScheduledTaskParameter] (
  [Id]               UNIQUEIDENTIFIER NOT NULL,
  [ScheduledTaskId]  UNIQUEIDENTIFIER NOT NULL,
  [Name]             NVARCHAR(250) NOT NULL,
  [Value]            NVARCHAR(1024) NOT NULL,

  PRIMARY KEY CLUSTERED ([Id]),
  CONSTRAINT [FK_MMP_ScheduledTaskParameter_ScheduledTaskId] FOREIGN KEY ([ScheduledTaskId]) REFERENCES [MMP].[ScheduledTask]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_ScheduledTaskParameter_ScheduledTaskId]
  ON [MMP].[ScheduledTaskParameter]
  ([ScheduledTaskId]);

CREATE INDEX [IX_MMP_ScheduledTaskParameter_Name]
  ON [MMP].[ScheduledTaskParameter]
  ([Name]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the scheduled task parameter',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTaskParameter', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the scheduled task the scheduled task parameter is associated with',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTaskParameter', @level2type=N'COLUMN', @level2name=N'ScheduledTaskId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the scheduled task parameter',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTaskParameter', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The value of the scheduled task parameter',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'ScheduledTaskParameter', @level2type=N'COLUMN', @level2name=N'Value';

GO



CREATE TABLE [MMP].[CodeCategory] (
  [Id]                UNIQUEIDENTIFIER NOT NULL,
  [OrganisationId]    INTEGER NOT NULL,
  [Type]              INTEGER NOT NULL,
  [Name]              NVARCHAR(256) NOT NULL,
  [Description]       NVARCHAR(512) NOT NULL DEFAULT N'',
  [Data]              VARBINARY(MAX),
  [EndPoint]          NVARCHAR(512),
  [IsEndPointSecure]  BIT NOT NULL DEFAULT 0,
  [IsCacheable]       BIT NOT NULL DEFAULT 0,
  [CacheExpiry]       INTEGER,
  [Updated]           DATETIME NOT NULL,

  PRIMARY KEY CLUSTERED ([Id]),
  CONSTRAINT [FK_MMP_CodeCategory_OrganisationId] FOREIGN KEY ([OrganisationId]) REFERENCES [MMP].[Organisation]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_CodeCategory_OrganisationId]
  ON [MMP].[CodeCategory]
  ([OrganisationId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the code category',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The unique ID identifying the organisation the code category is associated with',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'OrganisationId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The type of code category e.g. Local, RemoteHTTPService, RemoteWebService, etc',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'Type';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the code category',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The description for the code category',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'Description';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The custom code data for the code category',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'Data';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The endpoint if this is a remote code category',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'EndPoint';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'Is the endpoint for the remote code category secure',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'IsEndPointSecure';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'Is the code data retrieved for the remote code category cacheable',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'IsCacheable';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The time in seconds after which the cached code data for the remote code category will expire',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'CacheExpiry';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the code category was updated',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CodeCategory', @level2type=N'COLUMN', @level2name=N'Updated';

GO



CREATE TABLE [MMP].[Code] (
  [Id]           NVARCHAR(80) NOT NULL,
  [CategoryId]   UNIQUEIDENTIFIER NOT NULL,
  [Name]         NVARCHAR(256) NOT NULL,
  [Description]  NVARCHAR(512),
  [Value]        NVARCHAR(512) NOT NULL,

  PRIMARY KEY CLUSTERED ([CategoryId], [Id]),
  CONSTRAINT [FK_MMP_Code_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES [MMP].[CodeCategory]([Id]) ON DELETE CASCADE
);

CREATE INDEX [IX_MMP_Code_CategoryId]
  ON [MMP].[Code]
  ([CategoryId]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The ID used to uniquely identify the code',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Code', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the category the code is associated with',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Code', @level2type=N'COLUMN', @level2name=N'CategoryId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the code',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Code', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The description for the code',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Code', @level2type=N'COLUMN', @level2name=N'Description';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The value for the code',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'Code', @level2type=N'COLUMN', @level2name=N'Value';

GO



CREATE TABLE [MMP].[CachedCodeCategory] (
  [Id]      UNIQUEIDENTIFIER NOT NULL,
  [Data]    VARBINARY(MAX),
  [Cached]  DATETIME NOT NULL,

  PRIMARY KEY CLUSTERED (ID),
  CONSTRAINT [FK_MMP_CachedCodeCategory_Id] FOREIGN KEY ([Id]) REFERENCES [MMP].[CodeCategory]([Id]) ON DELETE CASCADE
);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the cached code category',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CachedCodeCategory', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The custom code data for the cached code category',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CachedCodeCategory', @level2type=N'COLUMN', @level2name=N'Data';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the code category was cached',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CachedCodeCategory', @level2type=N'COLUMN', @level2name=N'Cached';

GO



CREATE TABLE [MMP].[CachedCode] (
  [Id]           UNIQUEIDENTIFIER NOT NULL,
  [CategoryId]   UNIQUEIDENTIFIER NOT NULL,
  [Name]         NVARCHAR(256) NOT NULL,
  [Description]  NVARCHAR(512),
  [Value]        NVARCHAR(512) NOT NULL,

  PRIMARY KEY CLUSTERED ([CategoryId], [Id]),
  CONSTRAINT [FK_MMP_CachedCode_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES [MMP].[CachedCodeCategory]([Id]) ON DELETE CASCADE
);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The ID used to uniquely identify the code',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CachedCode', @level2type=N'COLUMN', @level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the category the code is associated with',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CachedCode', @level2type=N'COLUMN', @level2name=N'CategoryId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the code',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CachedCode', @level2type=N'COLUMN', @level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The description for the code',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CachedCode', @level2type=N'COLUMN', @level2name=N'Description';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The value for the code',
@level0type=N'SCHEMA', @level0name=N'MMP', @level1type=N'TABLE', @level1name=N'CachedCode', @level2type=N'COLUMN', @level2name=N'Value';

GO



CREATE TABLE [MMP].[MessageStatus] (
  [Code]  INTEGER NOT NULL,
  [Name]  NVARCHAR(100) NOT NULL,
  
  PRIMARY KEY CLUSTERED ([Code])
);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The code identifying the message status' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessageStatus', @level2type=N'COLUMN',@level2name=N'Code';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the message status' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessageStatus', @level2type=N'COLUMN',@level2name=N'Name';

GO



CREATE TABLE [MMP].[MessageType] (
  [Id]           UNIQUEIDENTIFIER NOT NULL,
  [Name]         NVARCHAR(100) NOT NULL,
  [Description]  NVARCHAR(500),

  PRIMARY KEY CLUSTERED ([Id])
);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the message type' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessageType', @level2type=N'COLUMN',@level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the message type' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessageType', @level2type=N'COLUMN',@level2name=N'Name';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'A description of the message type' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessageType', @level2type=N'COLUMN',@level2name=N'Description';

GO



CREATE TABLE [MMP].[Message] (
  [Id]                UNIQUEIDENTIFIER NOT NULL,
  [Username]          NVARCHAR(100) NOT NULL,
  [Organisation]      NVARCHAR(40) NOT NULL, 
  [Device]            NVARCHAR(40) NOT NULL,
  [TypeId]            UNIQUEIDENTIFIER NOT NULL,
  [TypeVersion]       INTEGER NOT NULL,
  [CorrelationId]     UNIQUEIDENTIFIER NOT NULL,
  [Priority]          INTEGER NOT NULL,
  [Status]            INTEGER NOT NULL,
  [Created]           DATETIME NOT NULL,
  [Received]          DATETIME NOT NULL,
  [Updated]           DATETIME,
  [SendAttempts]      INTEGER NOT NULL,
  [ProcessAttempts]   INTEGER NOT NULL,
  [DownloadAttempts]  INTEGER NOT NULL,
  [LockName]          NVARCHAR(100),
  [LastProcessed]     DATETIME,
  [Data]              VARBINARY(MAX),

  PRIMARY KEY CLUSTERED ([Id]),
  CONSTRAINT [FK_MMP_Message_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [MMP].[MessageType]([Id]),
  CONSTRAINT [FK_MMP_Message_Status] FOREIGN KEY ([Status]) REFERENCES [MMP].[MessageStatus]([Code])
);

CREATE INDEX [IX_MMP_Message_Username]
  ON [MMP].[Message]
  ([Username]);

CREATE INDEX [IX_MMP_Message_Device]
  ON [MMP].[Message]
  ([Device]);

CREATE INDEX [IX_MMP_Message_TypeId]
  ON [MMP].[Message]
  ([TypeId]);

CREATE INDEX [IX_MMP_Message_Priority]
  ON [MMP].[Message]
  ([Priority]);

CREATE INDEX [IX_MMP_Message_Status]
  ON [MMP].[Message]
  ([Status]);

CREATE INDEX [IX_MMP_Message_LockName]
  ON [MMP].[Message]
  ([LockName]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The username identifying the user associated with the message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Username';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The organisation code identifying the organisation associated with the message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Organisation';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The device ID identifying the device the message originated from' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Device';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the message type' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'TypeId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The version of the message type' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'TypeVersion';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to correlate the message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'CorrelationId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The message priority' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Priority';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The message status e.g. Initialized, QueuedForSending, etc' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Status';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the message was created' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Created';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the message was received i.e. uploaded or downloaded' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Received';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the message was last updated' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Updated';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The number of times that the sending of the message was attempted' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'SendAttempts';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The number of times that the processing of the message was attempted' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'ProcessAttempts';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The number of times that an attempt was made to download the message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'DownloadAttempts';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the entity that has locked the message for processing' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'LockName';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the last attempt was made to process the message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'LastProcessed';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The data for the message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'Message', @level2type=N'COLUMN',@level2name=N'Data';

GO



CREATE TABLE [MMP].[MessagePart] (
  [Id]                       UNIQUEIDENTIFIER NOT NULL,
  [PartNo]                   INTEGER NOT NULL,
  [TotalParts]               INTEGER NOT NULL,
  [SendAttempts]             INTEGER NOT NULL,
  [DownloadAttempts]         INTEGER NOT NULL,
  [Status]                   INTEGER NOT NULL,
  [Received]                 DATETIME NOT NULL,
  [Updated]                  DATETIME,
  [MessageId]                UNIQUEIDENTIFIER NOT NULL,
  [MessageUser]              NVARCHAR(100) NOT NULL,
  [MessageOrganisation]      NVARCHAR(40) NOT NULL, 
  [MessageDevice]            NVARCHAR(40) NOT NULL,
  [MessageTypeId]            UNIQUEIDENTIFIER NOT NULL,
  [MessageTypeVersion]       INTEGER NOT NULL,
  [MessageCorrelationId]     UNIQUEIDENTIFIER NOT NULL,
  [MessagePriority]          INTEGER NOT NULL,
  [MessageCreated]           DATETIME NOT NULL,
  [MessageDataHash]          NVARCHAR(40),
  [MessageEncryptionScheme]  INTEGER NOT NULL,
  [MessageEncryptionIV]      NVARCHAR(512) NOT NULL,
  [MessageChecksum]          NVARCHAR(40) NOT NULL,
  [LockName]                 NVARCHAR(100),
  [Data]                     VARBINARY(MAX),
	
  PRIMARY KEY ([Id]),
  CONSTRAINT [FK_MMP_MessagePart_MessageTypeId] FOREIGN KEY ([MessageTypeId]) REFERENCES [MMP].[MessageType]([Id])	
);

CREATE INDEX [IX_MMP_MessagePart_Status]
  ON [MMP].[MessagePart]
  ([Status]);

CREATE INDEX [IX_MMP_MessagePart_MessageId]
  ON [MMP].[MessagePart]
  ([MessageId]);

CREATE INDEX [IX_MMP_MessagePart_MessageDevice]
  ON [MMP].[MessagePart]
  ([MessageDevice]);

CREATE INDEX [IX_MMP_MessagePart_LockName]
  ON [MMP].[MessagePart]
  ([LockName]);

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the message part' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'Id';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The number of the message part in the set of message parts for the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'PartNo';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The total number of parts in the set of message parts for the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'TotalParts';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The number of times that the sending of the message part was attempted' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'SendAttempts';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The number of times that an attempt was made to download the message part' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'DownloadAttempts';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The message part status e.g. Initialized, QueuedForSending, etc' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'Status';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the message part was received i.e. uploaded or downloaded' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'Received';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The username identifying the user associated with the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageUser';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The organisation code identifying the organisation associated with the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageOrganisation';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The device ID identifying the device the original message originated from' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageDevice';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to uniquely identify the message type of the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageTypeId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The version of the original message type' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageTypeVersion';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The Universally Unique Identifier (UUID) used to correlate the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageCorrelationId';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The original message priority' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessagePriority';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The date and time the original message was created' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageCreated';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The hash of the unencrypted data for the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageDataHash';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The encryption scheme used to secure the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageEncryptionScheme';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The base-64 encoded initialisation vector for the encryption scheme for the original message' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageEncryptionIV';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The checksum used to verify the reconstruction of the original message data' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'MessageChecksum';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The name of the entity that has locked the message part for processing' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'LockName';

EXEC sys.sp_addextendedproperty
@name=N'MS_Description', @value=N'The data for the message part' ,
@level0type=N'SCHEMA',@level0name=N'MMP', @level1type=N'TABLE',@level1name=N'MessagePart', @level2type=N'COLUMN',@level2name=N'Data';

GO




/*

CREATE TABLE MMP.ARCHIVED_MESSAGES (
  ID              VARCHAR(40) NOT NULL,
  USERNAME        VARCHAR(100) NOT NULL,
  ORGANISATION    VARCHAR(40) NOT NULL,
  DEVICE          VARCHAR(20) NOT NULL,
  MSG_TYPE        VARCHAR(40) NOT NULL,
  MSG_TYPE_VER    INTEGER NOT NULL,
  CORRELATION_ID  VARCHAR(40) NOT NULL,
  CREATED         DATETIME NOT NULL,
  ARCHIVED        DATETIME NOT NULL,
  DATA            BYTEA,

  PRIMARY KEY (ID)
);

CREATE INDEX ARCHIVED_MESSAGES_USERNAME_IX
  ON MMP.ARCHIVED_MESSAGES
  (USERNAME);

CREATE INDEX ARCHIVED_MESSAGES_ORGANISATION_IX
  ON MMP.ARCHIVED_MESSAGES
  (ORGANISATION);

CREATE INDEX ARCHIVED_MESSAGES_DEVICE_IX
  ON MMP.ARCHIVED_MESSAGES
  (DEVICE);

CREATE INDEX ARCHIVED_MESSAGES_MSG_TYPE_IX
  ON MMP.ARCHIVED_MESSAGES
  (MSG_TYPE);

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.ID
  IS 'The Universally Unique Identifier (UUID) used to uniquely identify the message';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.USERNAME
  IS 'The username identifying the user associated with the message';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.ORGANISATION
  IS 'The organisation code identifying the organisation associated with the message';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.DEVICE
  IS 'The device ID identifying the device the message originated from';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.MSG_TYPE
  IS 'The UUID identifying the type of message';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGEs.MSG_TYPE_VER
  IS 'The version of the message type';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.CORRELATION_ID
  IS 'The UUID used to correlate the message';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.CREATED
  IS 'The date and time the message was created';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.ARCHIVED
  IS 'The date and time the message was archived';

COMMENT ON COLUMN MMP.ARCHIVED_MESSAGES.DATA
  IS 'The data for the message';









CREATE TABLE MMP.MESSAGE_AUDIT_LOG (
  ID            BIGINT NOT NULL,
  MSG_TYPE      VARCHAR(40) NOT NULL,
  USERNAME      VARCHAR(100) NOT NULL,
  ORGANISATION  VARCHAR(40) NOT NULL,
  DEVICE        VARCHAR(20) NOT NULL,
  IP            VARCHAR(20) NOT NULL,
  LOGGED        DATETIME NOT NULL,
  SUCCESSFUL    CHAR(1) NOT NULL,

  PRIMARY KEY (ID)
);

COMMENT ON COLUMN MMP.MESSAGE_AUDIT_LOG.ID
  IS 'The ID used to uniquely identify the message audit entry';

COMMENT ON COLUMN MMP.MESSAGE_AUDIT_LOG.MSG_TYPE
  IS 'The type of message associated with the message audit entry';

COMMENT ON COLUMN MMP.MESSAGE_AUDIT_LOG.USERNAME
  IS 'The user responsible for the message audit entry';

COMMENT ON COLUMN MMP.MESSAGE_AUDIT_LOG.ORGANISATION
  IS 'The organisation code identifying the organisation associated with the message audit entry';

COMMENT ON COLUMN MMP.MESSAGE_AUDIT_LOG.DEVICE
  IS 'The ID for the device associated with the message audit entry';

COMMENT ON COLUMN MMP.MESSAGE_AUDIT_LOG.IP
  IS 'The IP address of the remote device associated with the message audit entry';

COMMENT ON COLUMN MMP.MESSAGE_AUDIT_LOG.LOGGED
  IS 'The date and time the message audit entry was logged';

COMMENT ON COLUMN MMP.MESSAGE_AUDIT_LOG.SUCCESSFUL
  IS 'Was the message associated with the message audit entry successfully processed';



CREATE TABLE MMP.ERROR_REPORTS (
  ID                   VARCHAR(40) NOT NULL,
  APPLICATION_ID       VARCHAR(40) NOT NULL,
  APPLICATION_VERSION  INTEGER NOT NULL,
  DESCRIPTION          VARCHAR(2048) NOT NULL,
  DETAIL               VARCHAR(16384) NOT NULL,
  FEEDBACK             VARCHAR(4000) NOT NULL,
  CREATED              DATETIME NOT NULL,
  WHO                  VARCHAR(100) NOT NULL,
  DEVICE               VARCHAR(40) NOT NULL,
  DATA                 BYTEA,

  PRIMARY KEY (ID)
);

CREATE INDEX ERROR_REPORTS_APPLICATION_ID_IX
  ON MMP.ERROR_REPORTS
  (APPLICATION_ID);

CREATE INDEX ERROR_REPORTS_CREATED_IX
  ON MMP.ERROR_REPORTS
  (CREATED);

CREATE INDEX ERROR_REPORTS_WHO_IX
  ON MMP.ERROR_REPORTS
  (WHO);

COMMENT ON COLUMN MMP.ERROR_REPORTS.ID
  IS 'The Universally Unique Identifier (UUID) used to uniquely identify the error report';

COMMENT ON COLUMN MMP.ERROR_REPORTS.APPLICATION_ID
  IS 'The Universally Unique Identifier (UUID) used to uniquely identify the application that generated the error report';

COMMENT ON COLUMN MMP.ERROR_REPORTS.APPLICATION_VERSION
  IS 'The version of the application that generated the error report';

COMMENT ON COLUMN MMP.ERROR_REPORTS.DESCRIPTION
  IS 'The description of the error';

COMMENT ON COLUMN MMP.ERROR_REPORTS.DETAIL
  IS 'The error detail e.g. a stack trace';

COMMENT ON COLUMN MMP.ERROR_REPORTS.FEEDBACK
  IS 'The feedback provided by the user for the error';

COMMENT ON COLUMN MMP.ERROR_REPORTS.CREATED
  IS 'The date and time the error report was created';

COMMENT ON COLUMN MMP.ERROR_REPORTS.WHO
  IS 'The username identifying the user associated with the error report';

COMMENT ON COLUMN MMP.ERROR_REPORTS.DEVICE
  IS 'The device ID identifying the device the error report originated from';

COMMENT ON COLUMN MMP.ERROR_REPORTS.DATA
  IS 'The data associated with the error report';
  


CREATE TABLE MMP.SMS (
  ID              BIGINT NOT NULL,
  MOBILE_NUMBER   VARCHAR(40) NOT NULL,
  MESSAGE         VARCHAR(1024) NOT NULL,
  STATUS          INTEGER NOT NULL,
  SEND_ATTEMPTS   INTEGER NOT NULL,
  LOCK_NAME       VARCHAR(100),
  LAST_PROCESSED  DATETIME,

  PRIMARY KEY (ID)
);

COMMENT ON COLUMN MMP.SMS.ID
  IS 'The ID used to uniquely identify the SMS';

COMMENT ON COLUMN MMP.SMS.MOBILE_NUMBER
  IS 'The mobile number to send the SMS to';

COMMENT ON COLUMN MMP.SMS.MESSAGE
  IS 'The message to send';

COMMENT ON COLUMN MMP.SMS.STATUS
  IS 'The status of the SMS';

COMMENT ON COLUMN MMP.SMS.SEND_ATTEMPTS
  IS 'The number of times that the sending of the SMS was attempted';

COMMENT ON COLUMN MMP.SMS.LOCK_NAME
  IS 'The name of the entity that has locked the SMS for sending';

COMMENT ON COLUMN MMP.SMS.LAST_PROCESSED
  IS 'The date and time the last attempt was made to send the SMS';
*/


-- -------------------------------------------------------------------------------------------------
-- POPULATE TABLES
-- -------------------------------------------------------------------------------------------------
INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES
  (N'Application.OrganisationId', 100000);
INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES
  (N'Application.UserId', 100000);
INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES
  (N'Application.UserPasswordHistoryId', 100000);
INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES
  (N'Application.GroupId', 100000);
INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES
  (N'Application.FunctionId', 100000);
INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES
  (N'Application.FunctionTemplateId', 100000);
INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES
  (N'Application.ScheduledTaskParameterId', 100000);
INSERT INTO [MMP].[IdGenerator] ([Name], [Current]) VALUES
  (N'Application.CodeId', 10000000);

INSERT INTO [MMP].[Organisation] ([Id], [Code], [Name], [Description]) VALUES
  (1, N'MMP', N'MMP', N'MMP');

INSERT INTO [MMP].[User] ([Id], [Username], [Password], [Title], [FirstNames], [LastName], [Phone], [Fax],
  [Mobile], [Email], [PasswordAttempts], [PasswordExpiry], [Description]) VALUES
  (1, N'Administrator', N'cMzZAHM41tgd07YnFiG5z5qX6gA=', N'', N'', N'', N'', N'', N'', N'', null, null,
  N'Administrator');
INSERT INTO [MMP].[User] ([Id], [Username], [Password], [Title], [FirstNames], [LastName], [Phone], [Fax],
  [Mobile], [Email], [PasswordAttempts], [PasswordExpiry], [Description]) VALUES
  (2, N'test', N'cMzZAHM41tgd07YnFiG5z5qX6gA=', N'', N'', N'', N'', N'', N'', N'', null, null,
  N'Test User');

INSERT INTO [MMP].[Group] ([Id], [Name], [Description]) VALUES
  (1, N'Administrators', N'Administrators');
INSERT INTO [MMP].[Group] ([Id], [Name], [Description]) VALUES
  (2, N'Organisation Administrators', N'Organisation Administrators');

INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (0, 'Application.SecureHome', 'Secure Home', 'Secure Home');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (1, 'Application.Dashboard', 'Dashboard', 'Dashboard');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (2, 'Application.OrganisationAdministration', 'Organisation Administration', 'Organisation Administration');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (3, 'Application.AddOrganisation', 'Add Organisation', 'Add Organisation');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (4, 'Application.UpdateOrganisation', 'Update Organisation', 'Update Organisation');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (5, 'Application.RemoveOrganisation', 'Remove Organisation', 'Remove Organisation');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (6, 'Application.UserAdministration', 'User Administration', 'User Administration');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (7, 'Application.AddUser', 'Add User', 'Add User');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (8, 'Application.UpdateUser', 'Update User', 'Update User');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (9, 'Application.RemoveUser', 'Remove User', 'Remove User');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (10, 'Application.GroupAdministration', 'Group Administration', 'Group Administration');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (11, 'Application.AddGroup', 'Add Group', 'Add Group');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (12, 'Application.UpdateGroup', 'Update Group', 'Update Group');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (13, 'Application.RemoveGroup', 'Remove Group', 'Remove Group');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (14, 'Application.UserGroups', 'User Groups', 'User Groups');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (15, 'Application.CodeCategoryAdministration', 'Code Category Administration', 'Code Category Administration');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (16, 'Application.AddCodeCategory', 'Add Code Category', 'Add Code Category');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (17, 'Application.RemoveCodeCategory', 'Remove Code Category', 'Remove Code Category');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (18, 'Application.UpdateCodeCategory', 'Update Code Category', 'Update Code Category');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (19, 'Application.CodeAdministration', 'Code Administration', 'Code Administration');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (20, 'Application.AddCode', 'Add Code', 'Add Code');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (21, 'Application.RemoveCode', 'Remove Code', 'Remove Code');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (22, 'Application.UpdateCode', 'Update Code', 'Update Code');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (23, 'Application.ResetUserPassword', 'Reset User Password', 'Reset User Password');

INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (1000, 'ApplicationMessaging.ErrorReports', 'Error Reports', 'Error Reports');

INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (3000, 'ApplicationReporting.ReportDefinitionAdministration', 'Report Definition Administration', 'Report Definition Administration');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (3001, 'ApplicationReporting.AddReportDefinition', 'Add Report Definition', 'Add Report Definition');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (3002, 'ApplicationReporting.RemoveReportDefinition', 'Remove Report Definition', 'Remove Report Definition');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (3003, 'ApplicationReporting.UpdateReportDefinition', 'Update Report Definition', 'Update Report Definition');
INSERT INTO [MMP].[Function] ([Id], [Code], [Name], [Description]) VALUES
  (3004, 'ApplicationReporting.ViewReport', 'View Report', 'View Report');


-- INSERT INTO [MMP].[FunctionTemplate] ([Id], [Code], [Name], [Description]) VALUES
--   (1, N'Application.Administration', N'Administration', N'Administration');

-- INSERT INTO [MMP].[FunctionTemplateMap] ([FunctionId], [TemplateId]) VALUES (1, 1);
-- INSERT INTO [MMP].[FunctionTemplateMap] ([FunctionId], [TemplateId]) VALUES (2, 1);

INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 0);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 1);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 2);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 4);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 5);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 6);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 7);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 8);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 9);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 10);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 11);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 12);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 13);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 14);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 15);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 16);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 17);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 18);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 19);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 20);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 21);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 22);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 23);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 1000);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3000);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3001);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3002);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3003);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3004);

INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 0);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 6);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 7);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 8);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 9);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 14);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 22);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 23);
INSERT INTO [MMP].[FunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 3004);

INSERT INTO [MMP].[UserOrganisationMap] ([UserId], [OrganisationId]) VALUES (1, 1);
INSERT INTO [MMP].[UserOrganisationMap] ([UserId], [OrganisationId]) VALUES (2, 1);

INSERT INTO [MMP].[UserGroupMap] ([GroupId], [UserId], [OrganisationId]) VALUES (1, 1, 1);
INSERT INTO [MMP].[UserGroupMap] ([GroupId], [UserId], [OrganisationId]) VALUES (1, 2, 1);

INSERT INTO [MMP].[ExternalGroup] ([Id], [Name]) VALUES (1, N'Administrators');
INSERT INTO [MMP].[ExternalGroup] ([Id], [Name]) VALUES (2, N'Organisation Administrators');

INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 0);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 1);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 2);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 4);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 5);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 6);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 7);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 8);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 9);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 10);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 11);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 12);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 13);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 14);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 15);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 16);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 17);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 18);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 19);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 20);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 21);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 22);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 23);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 1000);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3000);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3001);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3002);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3003);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (1, 3004);

INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 0);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 6);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 7);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 8);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 9);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 14);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 22);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 23);
INSERT INTO [MMP].[ExternalFunctionGroupMap] ([GroupId], [FunctionId]) VALUES (2, 3004);

INSERT INTO [MMP].[MessageType] ([Id], [Name], [Description]) VALUES (N'3dbf238d-b56f-468a-8850-4ddf9f15c329', N'RegisterRequest', '');
INSERT INTO [MMP].[MessageType] ([Id], [Name], [Description]) VALUES (N'aa08aac9-4d15-452f-b3f9-756641b71735', N'RegisterResponse', '');
INSERT INTO [MMP].[MessageType] ([Id], [Name], [Description]) VALUES (N'a589dc87-2328-4a9b-bdb6-970e55ca2323', N'TestRequest', '');
INSERT INTO [MMP].[MessageType] ([Id], [Name], [Description]) VALUES (N'a3bad7ba-f9d4-4403-b54a-cb1f335ebbad', N'TestResponse', '');
INSERT INTO [MMP].[MessageType] ([Id], [Name], [Description]) VALUES (N'e9918051-8ebc-48f1-bad7-13c59b550e1a', N'AnotherTestRequest', '');
INSERT INTO [MMP].[MessageType] ([Id], [Name], [Description]) VALUES (N'a714a9c6-2914-4498-ab59-64be9991bf37', N'AnotherTestResponse', '');
INSERT INTO [MMP].[MessageType] ([Id], [Name], [Description]) VALUES (N'ff638c33-b4f1-4e79-804c-9560da2543d6', N'SubmitErrorReportRequest', '');
INSERT INTO [MMP].[MessageType] ([Id], [Name], [Description]) VALUES (N'8be50cfa-2fb1-4634-9bfa-d01e77eaf766', N'SubmitErrorReportResponse', '');

INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (0, N'Initialised');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (1, N'QueuedForSending');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (2, N'QueuedForProcessing');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (3, N'Aborted');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (4, N'Failed');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (5, N'Processing');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (6, N'Sending');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (7, N'QueuedForDownload');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (8, N'Downloading');
INSERT INTO [MMP].[MessageStatus] ([Code], [Name]) VALUES (10, N'Processed');


-- SELECT CAST('B14BC077-F1DF-457C-9F7E-7CB9E0BC1CF3' AS UNIQUEIDENTIFIER)


