﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Application.Codes
{
  /// <summary>The <b>Code</b> class holds the information for a code.</summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class Code
  {
    /// <summary>Constructs a new <b>Code</b>.</summary>
    /// <param name="id">The ID used to uniquely identify the code.</param>
    /// <param name="categoryId">
    ///   The Universally Unique Identifier (UUID) used to uniquely identify the category the code is
    ///   associated with.
    /// </param>
    /// <param name="name">The name of the code.</param>
    /// <param name="description">The description for the code.</param>
    /// <param name="value">The value for the code.</param>
    public Code(string id, Guid categoryId, string name, string description, string value)
    {
      Id = id;
      CategoryId = categoryId;
      Name = name;
      Description = description;
      Value = value;
    }

    /// <summary>
    ///   The Universally Unique Identifier (UUID) used to uniquely identify the category the code is
    ///   associated with.
    /// </summary>
    public Guid CategoryId { get; set; }

    /// <summary>The description for the code.</summary>
    public string Description { get; set; }

    /// <summary>The ID used to uniquely identify the code.</summary>
    public string Id { get; set; }

    /// <summary>The name of the code.</summary>
    public string Name { get; set; }

    /// <summary>The value for the code.</summary>
    public string Value { get; set; }
  }
}