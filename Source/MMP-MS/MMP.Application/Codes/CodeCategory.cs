﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;

namespace MMP.Application.Codes
{
  /// <summary>The <b>CodeCategory</b> class holds the information for a code category.</summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class CodeCategory
  {
    /// <summary>
    ///   The default time in seconds after which the cached code data for the remote code category will
    ///   expire, which is 1 day.
    /// </summary>
    public const int DEFAULT_CACHE_EXPIRY = 60*60*24;

    /// <summary>Constructs a new <b>CodeCategory</b>.</summary>
    /// <param name="id">
    ///   The Universally Unique Identifier (UUID) used to uniquely identify the code category.
    /// </param>
    /// <param name="organisationId">
    ///   The unique ID identifying the organisation the code category is associated with.
    /// </param>
    /// <param name="type">
    ///   The type of code category e.g. Local, RemoteHTTPService, RemoteWebService, etc.
    /// </param>
    /// <param name="name">The name of the code category.</param>
    /// <param name="description">The description for the code category.</param>
    /// <param name="endPoint">The endpoint if this is a remote code category.</param>
    /// <param name="isEndPointSecure">Is the endpoint for the remote code category secure?</param>
    /// <param name="isCacheable">Is the code data retrieved for the remote code category cacheable?</param>
    /// <param name="cacheExpiry">
    ///   The time in seconds after which the cached code data for the remote code category will expire.
    /// </param>
    public CodeCategory(Guid id, int organisationId, CodeCategoryType type, string name, string description,
      string endPoint, bool isEndPointSecure, bool isCacheable, int? cacheExpiry)
    {
      Id = id;
      OrganisationId = organisationId;
      Type = type;
      Name = name;
      Description = description;
      Data = null;
      EndPoint = endPoint;
      IsEndPointSecure = isEndPointSecure;
      IsCacheable = isCacheable;
      CacheExpiry = cacheExpiry;
    }

    /// <summary>Constructs a new <b>CodeCategory</b>.</summary>
    /// <param name="id">
    ///   The Universally Unique Identifier (UUID) used to uniquely identify the code category.
    /// </param>
    /// <param name="organisationId">
    ///   The unique ID identifying the organisation the code category is associated with.
    /// </param>
    /// <param name="type">
    ///   The type of code category e.g. Local, RemoteHTTPService, RemoteWebService, etc.
    /// </param>
    /// <param name="name">The name of the code category.</param>
    /// <param name="description">The description for the code category.</param>
    /// <param name="data">The custom code data for the code category.</param>
    /// <param name="endPoint">The endpoint if this is a remote code category.</param>
    /// <param name="isEndPointSecure">Is the endpoint for the remote code category secure?</param>
    /// <param name="isCacheable">Is the code data retrieved for the remote code category cacheable?</param>
    /// <param name="cacheExpiry">
    ///   The time in seconds after which the cached code data for the remote code category will expire.
    /// </param>
    public CodeCategory(Guid id, int organisationId, CodeCategoryType type, string name, string description,
      byte[] data, string endPoint, bool isEndPointSecure, bool isCacheable, int? cacheExpiry)
    {
      Id = id;
      OrganisationId = organisationId;
      Type = type;
      Name = name;
      Description = description;
      Data = data;
      EndPoint = endPoint;
      IsEndPointSecure = isEndPointSecure;
      IsCacheable = isCacheable;
      CacheExpiry = cacheExpiry;
    }

    /// <summary>Constructs a new <b>CodeCategory</b>.</summary>
    public CodeCategory()
    {
    }

    /// <summary>
    ///   The time in seconds after which the cached code data for the remote code category will expire.
    /// </summary>
    public int? CacheExpiry { get; set; }

    /// <summary>The codes for the code category.</summary>
    public IList<Code> Codes { get; set; }

    /// <summary>The custom code data for the code category.</summary>
    public byte[] Data { get; set; }

    /// <summary>The description for the code category.</summary>
    public string Description { get; set; }

    /// <summary>The endpoint if this is a remote code category.</summary>
    public string EndPoint { get; set; }

    /// <summary>
    ///   The Universally Unique Identifier (UUID) used to uniquely identify the code category.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>Is the code data retrieved for the remote code category cacheable?</summary>
    public bool IsCacheable { get; set; }

    /// <summary>Is the endpoint for the remote code category secure?</summary>
    public bool IsEndPointSecure { get; set; }

    /// <summary>The name of the code category.</summary>
    public string Name { get; set; }

    /// <summary>
    ///   The unique ID identifying the organisation the code category is associated with.
    /// </summary>
    public int OrganisationId { get; set; }

    /// <summary>The type of code category e.g. Local, RemoteHTTPService, RemoteWebService, etc.</summary>
    public CodeCategoryType Type { get; set; }

    /// <summary>The date and time the code category was updated.</summary>
    public DateTime Updated { get; set; }
  }
}