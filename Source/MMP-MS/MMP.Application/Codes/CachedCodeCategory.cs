﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;

namespace MMP.Application.Codes
{
  /// <summary>
  ///   The <b>CachedCodeCategory</b> class holds the information for a cached code category.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class CachedCodeCategory
  {
    /// <summary>Constructs a new <b>CachedCodeCategory</b>.</summary>
    /// <param name="id">
    ///   The Universally Unique Identifier (UUID) used to uniquely identify the cached code category.
    /// </param>
    /// <param name="data">The custom code data for the cached code category.</param>
    /// <param name="cached">The date and time the code category was cached.</param>
    public CachedCodeCategory(Guid id, byte[] data, DateTime cached)
    {
      Id = id;
      Data = data;
      Cached = cached;
    }

    /// <summary>The date and time the code category was cached.</summary>
    public DateTime Cached { get; set; }

    /// <summary>The codes for the cached code category.</summary>
    public IList<Code> Codes { get; set; }

    /// <summary>The custom code data for the cached code category.</summary>
    public byte[] Data { get; set; }

    /// <summary>
    ///   The Universally Unique Identifier (UUID) used to uniquely identify the cached code category.
    /// </summary>
    public Guid Id { get; set; }
  }
}