﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;

namespace MMP.Application.Codes
{
  /// <summary>
  ///   The <b>ICodesDAO</b> interface defines the functionality that must be provided by a Codes DAO
  ///   implementation.
  /// </summary>
  [CLSCompliant(true)]
  public interface ICodesDAO
  {
    /// <summary>Check whether the cached code category with the specified ID exists.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the cached code category.</param>
    /// <returns>
    ///   <b>True</b> if the cached code category exists or <b>False</b> otherwise.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //bool CachedCodeCategoryExists(Guid id);

    /// <summary>Check whether the code category with the specified ID exists.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the code category.</param>
    /// <returns>
    ///   <b>True</b> if the code category exists or <b>False</b> otherwise.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //bool CodeCategoryExists(Guid id);

    /// <summary>Create the new cached code.</summary>
    /// <param name="code">The <b>Code</b> instance containing the information for the new cached code.</param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //void CreateCachedCode(Code code);

    /// <summary>Create the new cached code category.</summary>
    /// <param name="cachedCodeCategory">
    ///   The <b>CachedCodeCategory</b> instance containing the information for the new cached code
    ///   category
    /// </param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //void CreateCachedCodeCategory(CachedCodeCategory cachedCodeCategory);

    /// <summary>Create the new code.</summary>
    /// <param name="code">The <b>Code</b> instance containing the information for the new code.</param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //void CreateCode(Code code);

    /// <summary>Create the new code category.</summary>
    /// <param name="codeCategory">
    ///   The <b>CodeCategory</b> instance containing the information for the new code category.
    /// </param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    void CreateCodeCategory(CodeCategory codeCategory);

    /// <summary>Delete the cached code category.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the cached code category.</param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //void DeleteCachedCodeCategory(Guid id);

    /// <summary>Delete the code.</summary>
    /// <param name="id">The ID uniquely identifying the code.</param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //void DeleteCode(string id);

    /// <summary>Delete the code category.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the code category.</param>
    /// <returns><b>True</b> if the code category was deleted successfully or <b>False</b> otherwise.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    bool DeleteCodeCategory(Guid id);

    /// <summary>Retrieve the cached code category with the specified ID.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the cached code category.</param>
    /// <param name="retrieveCodes">
    ///   Should the codes and/or code data for the cached code category be
    ///   retrieved?
    /// </param>
    /// <returns>
    ///   The cached code category with the specified ID or <b>null</b> if the cached code category could
    ///   not be found.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //CachedCodeCategory GetCachedCodeCategory(Guid id, bool retrieveCodes);

    /// <summary>Returns all the cached codes for the cached code category with the specified ID.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the cached code category.</param>
    /// <returns>All the cached codes for the cached code category with the specified ID.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //IList<Code> GetCachedCodesForCachedCodeCategory(Guid id);

    /// <summary>Retrieve the code with the specified ID.</summary>
    /// <param name="id">The ID uniquely identifying the code.</param>
    /// <returns>The code with the specified ID or <b>null</b> if the code could not be found.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //Code GetCode(string id);

    /// <summary>
    ///   Returns all the code categories associated with the organisation identified by the specified
    ///   organisation code.
    /// </summary>
    /// <param name="organisation">The organisation code identifying the organisation.</param>
    /// <param name="retrieveCodes">Should the codes and/or code data for the code categories be retrieved?</param>
    /// <returns>
    ///   All the code categories associated with the organisation identified by the specified organisation
    ///   code.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //IList<CodeCategory> GetCodeCategoriesForOrganisation(string organisation, bool retrieveCodes);

    /// <summary>Retrieve the code category with the specified ID.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the code category.</param>
    /// <param name="retrieveCodes">Should the codes and/or code data for the code category be retrieved?</param>
    /// <returns>
    ///   The code category with the specified ID or <b>null</b> if the code category could not be found.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //CodeCategory GetCodeCategory(Guid id, bool retrieveCodes);

    /// <summary>Returns all the codes for the code category with the specified ID.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the code category.</param>
    /// <returns>All the codes for the code category with the specified ID.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //IList<Code> GetCodesForCodeCategory(Guid id);

    /// <summary>
    ///   Returns the number of code categories associated with the organisation identified by the
    ///   specified organisation code.
    /// </summary>
    /// <param name="organisation">The organisation code identifying the organisation.</param>
    /// <returns>
    ///   The number of code categories associated with the organisation identified by the specified
    ///   organisation code.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //int GetNumberOfCodeCategoriesForOrganisation(string organisation);

    /// <summary>Returns the number of codes for the code category with the specified ID.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the code category.</param>
    /// <returns>The number of codes for the code category with the specified ID.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //int GetNumberOfCodesForCodeCategory(Guid id);

    /// <summary>Is the cached code category current?</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the cached code category.</param>
    /// <returns>
    ///   <b>True</b> if the cached code category is current or <b>False</b> otherwise.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //bool IsCachedCodeCategoryCurrent(Guid id);

    /// <summary>Update the existing cached code category.</summary>
    /// <param name="cachedCodeCategory">
    ///   The <b>CachedCodeCategory</b> instance containing the updated information for the cached code
    ///   category.
    /// </param>
    /// <param name="updatedBy">The username identifying the user that updated the cached code category.</param>
    /// <returns>The updated cached code category.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //CachedCodeCategory UpdateCachedCodeCategory(CachedCodeCategory cachedCodeCategory, string updatedBy);

    /// <summary>Update the existing code.</summary>
    /// <param name="code">The <b>Code</b> instance containing the updated information for the code.</param>
    /// <param name="updatedBy">The username identifying the user that updated the code.</param>
    /// <returns>The updated code.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //Code UpdateCode(Code code, string updatedBy);

    /// <summary>Update the existing code category.</summary>
    /// <param name="codeCategory">
    ///   The <b>CodeCategory</b> instance containing the updated information for the code category.
    /// </param>
    /// <param name="updatedBy">The username identifying the user that updated the code category.</param>
    /// <returns>The updated code category.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    //CodeCategory UpdateCodeCategory(CodeCategory codeCategory, string updatedBy);
  }
}


