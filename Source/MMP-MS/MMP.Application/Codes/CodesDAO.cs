﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Data;
using System.Data.SqlClient;
using MMP.Common.Persistence;

namespace MMP.Application.Codes
{
  /// <summary>The <b>CodesDao</b> class provides the Codes DAO implementation.</summary>
  [CLSCompliant(true)]
  public sealed class CodesDAO : DAO, ICodesDAO
  {
    /// <summary>Constructs a new <b>CodesDAO</b>.</summary>
    /// <param name="connectionString">The ADO.NET connection string.</param>
    public CodesDAO(string connectionString) : base(connectionString)
    {
    }

    /// <summary>Create the new code category.</summary>
    /// <param name="codeCategory">
    ///   The <b>CodeCategory</b> instance containing the information for the new code category.
    /// </param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    public void CreateCodeCategory(CodeCategory codeCategory)
    {
      try
      {
        using (var connection = GetConnection())
        {
          connection.Open();

          var createCodeCategorySQL = @"INSERT INTO [MMP].[CodeCategory]
            ([Id], [OrganisationId], [Type], [Name], [Description], [Data],
            [EndPoint], [IsEndPointSecure], [IsCacheable], [CacheExpiry], [Updated])
            VALUES 
            (@id, @organisationId, @type, @name, @description, @data, 
            @endPoint, @isEndPointSecure, @isCacheable, @cacheExpiry, @updated)";

          var command = new SqlCommand(createCodeCategorySQL, connection);

          command.Parameters.AddWithValue("@id", codeCategory.Id);
          command.Parameters.AddWithValue("@organisationId", codeCategory.OrganisationId);
          command.Parameters.AddWithValue("@type", (int) codeCategory.Type);
          command.Parameters.AddWithValue("@name", codeCategory.Name);

          if (codeCategory.Description == null)
            command.Parameters.AddWithValue("@description", DBNull.Value);
          else
            command.Parameters.AddWithValue("@description", codeCategory.Description);

          if (codeCategory.Data == null)
          {
            command.Parameters.Add("@data", SqlDbType.VarBinary, -1);
            command.Parameters["@data"].Value = DBNull.Value;
          }
          else
            command.Parameters.AddWithValue("@data", codeCategory.Data);

          if (codeCategory.EndPoint == null)
            command.Parameters.AddWithValue("@endPoint", DBNull.Value);
          else
            command.Parameters.AddWithValue("@endPoint", codeCategory.EndPoint);

          command.Parameters.AddWithValue("@isEndPointSecure", codeCategory.IsEndPointSecure);
          command.Parameters.AddWithValue("@isCacheable", codeCategory.IsCacheable);

          if (!codeCategory.CacheExpiry.HasValue)
            command.Parameters.AddWithValue("@cacheExpiry", DBNull.Value);
          else
            command.Parameters.AddWithValue("@cacheExpiry", codeCategory.CacheExpiry.Value);

          command.Parameters.AddWithValue("@updated", DateTime.Now);

          if (command.ExecuteNonQuery() != 1)
          {
            throw new DAOException("Failed to add the code category (" + codeCategory.Id + ") to the" +
                                   " database: No rows were affected as a result of executing the SQL statement (" +
                                   createCodeCategorySQL + ")");
          }
        }
      }
      catch (Exception ex)
      {
        throw new DAOException("Failed to add the code category (" + codeCategory.Id + ") to the database", ex);
      }
    }

    /// <summary>Delete the code category.</summary>
    /// <param name="id">The Universally Unique Identifier (UUID) used to uniquely identify the code category.</param>
    /// <returns><b>True</b> if the code category was deleted successfully or <b>False</b> otherwise.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    public bool DeleteCodeCategory(Guid id)
    {
      try
      {
        using (var connection = GetConnection())
        {
          connection.Open();

          var createCodeCategorySQL = @"DELETE FROM [MMP].[CodeCategory]
            WHERE [Id] = @id";

          var command = new SqlCommand(createCodeCategorySQL, connection);

          command.Parameters.AddWithValue("@id", id);

          return (command.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception ex)
      {
        throw new DAOException("Failed to delete the code category (" + id + ") from the database", ex);
      }
    }

  }
}

#if _XXX_

/**
 * The <code>CodesDAO</code> class implements the codes-related persistence operations.
 *
 * @author Marcus Portmann
 */
@ApplicationScoped
@Default
public class CodesDAO
  implements ICodesDAO
{
  /* Logger */
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(CodesDAO.class);
  private String cachedCodeCategoryExistsSQL;
  private String codeCategoryExistsSQL;
  private String createCachedCodeCategorySQL;
  private String createCachedCodeSQL;  private String createCodeSQL;

  /** The data source used to provide connections to the database. */
  private DataSource dataSource;
  private String deleteCachedCodeCategorySQL;

  private String deleteCodeSQL;
  private String getCachedCodeCategoryCachedSQL;
  private String getCachedCodeCategorySQL;
  private String getCachedCodesForCachedCodeCategorySQL;
  private String getCodeCategoriesForOrganisationSQL;
  private String getCodeCategoriesNoDataForOrganisationSQL;
  private String getCodeCategoryCacheExpirySQL;
  private String getCodeCategorySQL;
  private String getCodeSQL;
  private String getCodesForCodeCategorySQL;
  private String getNumberOfCodeCategoriesForOrganisationSQL;
  private String getNumberOfCodesForCodeCategorySQL;

  /** The ID generator used to generate unique numeric IDs for the DAO. */
  private IDGenerator idGenerator;

  /**
   * The name of the database schema that contains the database objects associated with the DAO.
   * Also called the <b>database schema</b>.
   */
  private String schema;
  private String updateCachedCodeCategorySQL;
  private String updateCodeCategorySQL;
  private String updateCodeSQL;

  /** Has a schema name been specified that should be used when building the SQL statements? */
  private boolean useSchema;

  /**
   * Constructs a new <code>CodesDAO</code>.
   */
  public CodesDAO() {}

  /**
   * Check whether the cached code category with the specified ID exists.
   *
   * @param id the ID uniquely identifying the cached code category
   *
   * @return <code>true</code> if the cached code category exists or <code>false</code> otherwise
   *
   * @throws DAOException
   */
  public boolean cachedCodeCategoryExists(String id)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(cachedCodeCategoryExistsSQL);
      statement.setString(1, id);

      rs = statement.executeQuery();

      if (rs.next())
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to check whether the cached code category (" + id
          + ") exists in the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Check whether the code category with the specified ID exists.
   *
   * @param id the ID uniquely identifying the code category
   *
   * @return <code>true</code> if the code category exists or <code>false</code> otherwise
   *
   * @throws DAOException
   */
  public boolean codeCategoryExists(String id)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(codeCategoryExistsSQL);
      statement.setString(1, id);

      rs = statement.executeQuery();

      if (rs.next())
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to check whether the code category (" + id
          + ") exists in the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Create the new cached code.
   *
   * @param code the <code>Code</code> instance containing the information for the new cached code
   *
   * @throws DAOException
   */
  public void createCachedCode(Code code)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(createCachedCodeSQL);

      if (StringUtil.isNullOrEmpty(code.getId()))
      {
        statement.setString(1, String.valueOf(idGenerator.next("Application.CodeId")));
      }
      else
      {
        statement.setString(1, code.getId());
      }

      statement.setString(2, code.getCategoryId());
      statement.setString(3, code.getName());
      statement.setString(4, code.getDescription());
      statement.setString(5, code.getValue());

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to add the cached code (" + code.getName()
            + ") for the cached code category (" + code.getCategoryId() + ") to the"
            + " database: No rows were affected as a result of executing the SQL statement ("
            + createCachedCodeSQL + ")");
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to add the cached code (" + code.getName()
          + ") for the cached code category (" + code.getCategoryId() + ") to the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Create the new cached code category.
   *
   * @param cachedCodeCategory the <code>CachedCodeCategory</code> instance containing the
   *                           information for the new code category
   *
   * @throws DAOException
   */
  public void createCachedCodeCategory(CachedCodeCategory cachedCodeCategory)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(createCachedCodeCategorySQL);

      statement.setString(1, cachedCodeCategory.getId());
      statement.setBytes(2, cachedCodeCategory.getCodeData());
      statement.setTimestamp(3, new Timestamp(cachedCodeCategory.getLastUpdated().getTime()));
      statement.setTimestamp(4, new Timestamp(cachedCodeCategory.getCached().getTime()));

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to add the cached code category ("
            + cachedCodeCategory.getId() + ") to the"
            + " database: No rows were affected as a result of executing the SQL statement ("
            + createCachedCodeCategorySQL + ")");
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to add the cached code category ("
          + cachedCodeCategory.getId() + ") to the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Create the new code.
   *
   * @param code the <code>Code</code> instance containing the information for the new code
   *
   * @throws DAOException
   */
  public void createCode(Code code)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(createCodeSQL);

      if (StringUtil.isNullOrEmpty(code.getId()))
      {
        statement.setString(1, String.valueOf(idGenerator.next("Application.CodeId")));
      }
      else
      {
        statement.setString(1, code.getId());
      }

      statement.setString(2, code.getCategoryId());
      statement.setString(3, code.getName());
      statement.setString(4, code.getDescription());
      statement.setString(5, code.getValue());

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to add the code (" + code.getName()
            + ") for the code category (" + code.getCategoryId() + ") to the"
            + " database: No rows were affected as a result of executing the SQL statement ("
            + createCodeSQL + ")");
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to add the code (" + code.getName()
          + ") for the code category (" + code.getCategoryId() + ") to the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }



  /**
   * Delete the cached code category.
   *
   * @param id the ID uniquely identifying the cached code category
   *
   * @throws DAOException
   */
  public void deleteCachedCodeCategory(String id)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(deleteCachedCodeCategorySQL);

      statement.setString(1, id);

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to delete the cached code category (" + id
            + ") in the database:"
            + " No rows were affected as a result of executing the SQL statement ("
            + deleteCachedCodeCategorySQL + ")");
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to delete the cached code category (" + id
          + ") in the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Delete the code.
   *
   * @param id the ID uniquely identifying the code
   *
   * @throws DAOException
   */
  public void deleteCode(String id)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(deleteCodeSQL);

      statement.setString(1, id);

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to delete the code (" + id + ") in the database:"
            + " No rows were affected as a result of executing the SQL statement (" + deleteCodeSQL
            + ")");
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to delete the code (" + id + ") in the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Delete the code category.
   *
   * @param id the ID uniquely identifying the code category
   *
   * @throws DAOException
   */
  public void deleteCodeCategory(String id)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(deleteCodeCategorySQL);

      statement.setString(1, id);

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to delete the code category (" + id + ") in the database:"
            + " No rows were affected as a result of executing the SQL statement ("
            + deleteCodeCategorySQL + ")");
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to delete the code category (" + id + ") in the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Retrieve the cached code category with the specified ID.
   *
   * @param id the ID uniquely identifying the cached code category
   *
   * @return the cached code category with the specified ID or <code>null</code> if the cached code
   *         category could not be found
   *
   * @throws DAOException
   */
  public CachedCodeCategory getCachedCodeCategory(String id)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(getCachedCodeCategorySQL);
      statement.setString(1, id);

      rs = statement.executeQuery();

      if (rs.next())
      {
        return getCachedCodeCategory(rs);
      }
      else
      {
        return null;
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the cached code category (" + id
          + ") from the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Returns all the cached codes for the cached code category with the specified ID.
   *
   * @param cachedCodeCategoryId the ID uniquely identifying the cached code category
   *
   * @return all the cached codes for the cached code category with the specified ID
   *
   * @throws DAOException
   */
  public List<Code> getCachedCodesForCachedCodeCategory(String cachedCodeCategoryId)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(getCachedCodesForCachedCodeCategorySQL);
      statement.setString(1, cachedCodeCategoryId);

      rs = statement.executeQuery();

      List<Code> codes = new ArrayList<Code>();

      while (rs.next())
      {
        codes.add(getCode(rs));
      }

      return codes;
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the cached codes for the cached code category ("
          + cachedCodeCategoryId + ") from the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Retrieve the code with the specified ID.
   *
   * @param id the ID uniquely identifying the code
   *
   * @return the code with the specified ID or <code>null</code> if the code could not be found
   *
   * @throws DAOException
   */
  public Code getCode(String id)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(getCodeSQL);
      statement.setString(1, id);

      rs = statement.executeQuery();

      if (rs.next())
      {
        return getCode(rs);
      }
      else
      {
        return null;
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the code (" + id + ") from the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Returns all the code categories associated with the organisation identified by the
   * specified organisation code.
   *
   * @param organisation  the organisation code identifying the organisation
   * @param retrieveCodes retrieve the codes and/or code data for the code categories
   *
   * @return all the code categories associated with the organisation identified by the
   *         specified organisation code
   *
   * @throws DAOException
   */
  public List<CodeCategory> getCodeCategoriesForOrganisation(String organisation,
      boolean retrieveCodes)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      if (retrieveCodes)
      {
        statement = connection.prepareStatement(getCodeCategoriesForOrganisationSQL);
      }
      else
      {
        statement = connection.prepareStatement(getCodeCategoriesNoDataForOrganisationSQL);
      }

      statement.setString(1, organisation);

      rs = statement.executeQuery();

      List<CodeCategory> codeCategories = new ArrayList<CodeCategory>();

      while (rs.next())
      {
        if (retrieveCodes)
        {
          codeCategories.add(getCodeCategory(rs));
        }
        else
        {
          codeCategories.add(getCodeCategoryNoData(rs));
        }
      }

      return codeCategories;
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the code categories for the organisation ("
          + organisation + ") from the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Retrieve the code category with the specified ID.
   *
   * @param id the ID uniquely identifying the code category
   *
   * @return the code category with the specified ID or <code>null</code> if the code category
   *         could not be found
   *
   * @throws DAOException
   */
  public CodeCategory getCodeCategory(String id)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(getCodeCategorySQL);
      statement.setString(1, id);

      rs = statement.executeQuery();

      if (rs.next())
      {
        return getCodeCategory(rs);
      }
      else
      {
        return null;
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the code category (" + id + ") from the database",
          e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Returns all the codes for the code category with the specified ID.
   *
   * @param codeCategoryId the ID uniquely identifying the code category
   *
   * @return all the codes for the code category with the specified ID
   *
   * @throws DAOException
   */
  public List<Code> getCodesForCodeCategory(String codeCategoryId)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(getCodesForCodeCategorySQL);
      statement.setString(1, codeCategoryId);

      rs = statement.executeQuery();

      List<Code> codes = new ArrayList<Code>();

      while (rs.next())
      {
        codes.add(getCode(rs));
      }

      return codes;
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the codes for the code category ("
          + codeCategoryId + ") from the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Returns the number of code categories associated with the organisation identified by the
   * specified organisation code.
   *
   * @param organisation the organisation code identifying the organisation
   *
   * @return the number of code categories associated with the organisation identified by the
   *         specified organisation code
   *
   * @throws DAOException
   */
  public int getNumberOfCodeCategoriesForOrganisation(String organisation)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(getNumberOfCodeCategoriesForOrganisationSQL);
      statement.setString(1, organisation);

      rs = statement.executeQuery();

      if (rs.next())
      {
        return rs.getInt(1);
      }
      else
      {
        throw new DAOException("Failed to retrieve the number of code categories for the"
            + " organisation (" + organisation + ") in the database:"
            + " No results were returned as a result of executing the SQL statement ("
            + getNumberOfCodeCategoriesForOrganisationSQL + ")");
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the number of code categories for the"
          + " organisation (" + organisation + ") in the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Returns the number of codes for the code category with the specified ID.
   *
   * @param codeCategoryId the ID uniquely identifying the code category
   *
   * @return the number of codes for the code category with the specified ID
   *
   * @throws DAOException
   */
  public int getNumberOfCodesForCodeCategory(String codeCategoryId)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(getNumberOfCodesForCodeCategorySQL);
      statement.setString(1, codeCategoryId);

      rs = statement.executeQuery();

      if (rs.next())
      {
        return rs.getInt(1);
      }
      else
      {
        throw new DAOException("Failed to retrieve the number of codes for the code category ("
            + codeCategoryId + ") in the database:"
            + " No results were returned as a result of executing the SQL statement ("
            + getNumberOfCodesForCodeCategorySQL + ")");
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the number of codes for the code category ("
          + codeCategoryId + ") in the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Initialise the <code>DataAccessObject</code>.
   */
  @PostConstruct
  public void init()
  {
    try
    {
      dataSource = InitialContext.doLookup("java:app/jdbc/ApplicationDataSource");
    }
    catch (Throwable e) {}

    if (dataSource == null)
    {
      throw new DAOException("Failed to initialise the codes DAO:"
          + " Failed to retrieve the data source using the JNDI lookup"
          + " (java:app/jdbc/ApplicationDataSource)");
    }

    try
    {
      schema = InitialContext.doLookup("java:app/env/ApplicationDatabaseSchema");
    }
    catch (Throwable e) {}

    try
    {
      // Retrieve the database meta data
      Connection connection = null;
      String schemaSeparator = ".";
      String idQuote = "\"";
      
      try
      {
        connection = dataSource.getConnection();

        DatabaseMetaData metaData = connection.getMetaData();
        
        // Retrieve the schema separator for the database
        schemaSeparator = metaData.getCatalogSeparator();

        if ((schemaSeparator == null) || (schemaSeparator.length() == 0))
        {
          schemaSeparator = ".";
        }

        // Retrieve the identifier enquoting string for the database
        idQuote = metaData.getIdentifierQuoteString();

        if ((idQuote == null) || (idQuote.length() == 0))
        {
          idQuote = "\"";
        }        
      }
      finally
      {
        DAOUtil.close(connection);
      }

      // Determine the schema prefix
      String schemaPrefix = "";

      if ((schema != null) && (schema.length() > 0))
      {
        schemaPrefix = "\"" + schema + "\"" + schemaSeparator;
      }

      // Build the SQL statements for the DAO
      buildStatements(schema, schemaSeparator, schemaPrefix, idQuote);
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to initialise the " + getClass().getName()
          + " data access object: " + e.getMessage(), e);
    }

    if (useSchema)
    {
      idGenerator = new IDGenerator(dataSource, schema);
    }
    else
    {
      idGenerator = new IDGenerator(dataSource);
    }
  }

  /**
   * Is the cached code category current?
   *
   * @param id the ID uniquely identifying the cached code category
   *
   * @return <code>true</code> if the cached code category is current or <code>false</code>
   *         otherwise
   *
   * @throws DAOException
   */
  public boolean isCachedCodeCategoryCurrent(String id)
    throws DAOException
  {
    Connection connection = null;

    try
    {
      connection = dataSource.getConnection();

      Integer cacheExpiry = getCodeCategoryCacheExpiry(connection, id);

      if (cacheExpiry == null)
      {
        return false;
      }

      Date cached = getCachedCodeCategoryCached(connection, id);

      if (cached == null)
      {
        return false;
      }

      if (System.currentTimeMillis() > (cached.getTime() + (cacheExpiry.longValue() * 1000L)))
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to check whether the cached code category (" + id
          + ") in the database is current", e);
    }
    finally
    {
      DAOUtil.close(connection);
    }
  }

  /**
   * Update the existing cached code category.
   *
   * @param cachedCodeCategory the <code>CachedCodeCategory</code> instance containing the updated
   *                           information for the cached code category
   * @param updatedBy          the username identifying the user that updated the cached code
   *                           category
   *
   * @return the updated cached code category
   *
   * @throws DAOException
   */
  public CachedCodeCategory updateCachedCodeCategory(CachedCodeCategory cachedCodeCategory,
      String updatedBy)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(updateCachedCodeCategorySQL);

      Date cached = new Date();

      statement.setBytes(1, cachedCodeCategory.getCodeData());
      statement.setTimestamp(2, new Timestamp(cached.getTime()));
      statement.setTimestamp(3, new Timestamp(cachedCodeCategory.getLastUpdated().getTime()));
      statement.setString(4, cachedCodeCategory.getId());

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to update the cached code category ("
            + cachedCodeCategory.getId() + ") in the"
            + " database: No rows were affected as a result of executing the SQL statement ("
            + updateCachedCodeCategorySQL + ")");
      }

      cachedCodeCategory.setCached(cached);

      return cachedCodeCategory;
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to update the cached code category ("
          + cachedCodeCategory.getId() + ") in the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Update the existing code.
   *
   * @param code      the <code>Code</code> instance containing the updated information for the code
   * @param updatedBy the username identifying the user that updated the code
   *
   * @return the updated code
   *
   * @throws DAOException
   */
  public Code updateCode(Code code, String updatedBy)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(updateCodeSQL);

      statement.setString(1, code.getCategoryId());
      statement.setString(2, code.getName());
      statement.setString(3, code.getDescription());
      statement.setString(4, code.getValue());
      statement.setString(5, code.getId());

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to update the code (" + code.getId() + ") in the database:"
            + " No rows were affected as a result of executing the SQL statement (" + updateCodeSQL
            + ")");
      }

      return code;
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to update the code (" + code.getId() + ") in the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * Update the existing code category.
   *
   * @param codeCategory the <code>CodeCategory</code> instance containing the updated information
   *                     for the code category
   * @param updatedBy    the username identifying the user that updated the code category
   *
   * @return the updated code category
   *
   * @throws DAOException
   */
  public CodeCategory updateCodeCategory(CodeCategory codeCategory, String updatedBy)
    throws DAOException
  {
    Connection connection = null;
    PreparedStatement statement = null;

    try
    {
      connection = dataSource.getConnection();

      statement = connection.prepareStatement(updateCodeCategorySQL);

      Date updated = new Date();

      statement.setString(1, codeCategory.getOrganisation());
      statement.setInt(2, codeCategory.getCategoryType().getCode());
      statement.setString(3, codeCategory.getName());
      statement.setString(4, codeCategory.getDescription());
      statement.setBytes(5, codeCategory.getCodeData());
      statement.setString(6, codeCategory.getEndPoint());
      statement.setBoolean(7, codeCategory.getIsEndPointSecure());
      statement.setBoolean(8, codeCategory.getIsCacheable());

      if (codeCategory.getCacheExpiry() == null)
      {
        statement.setNull(9, java.sql.Types.INTEGER);
      }
      else
      {
        statement.setInt(9, codeCategory.getCacheExpiry());
      }

      statement.setTimestamp(10, new Timestamp(updated.getTime()));
      statement.setString(11, updatedBy);
      statement.setString(12, codeCategory.getId());

      if (statement.executeUpdate() != 1)
      {
        throw new DAOException("Failed to update the code category (" + codeCategory.getId()
            + ") in the"
            + " database: No rows were affected as a result of executing the SQL statement ("
            + updateCodeCategorySQL + ")");
      }

      codeCategory.setUpdated(updated);
      codeCategory.setUpdatedBy(updatedBy);

      return codeCategory;
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to update the code category (" + codeCategory.getId()
          + ") in the database", e);
    }
    finally
    {
      DAOUtil.close(statement);
      DAOUtil.close(connection);
    }
  }

  /**
   * This method must be implemented by all classes derived from <code>DataAccessObject</code> and
   * should contain the code to generate the SQL statements for the DAO.
   *
   * @param schema          the name of the database schema that contains the database objects
   *                        referenced by the DAO
   * @param schemaSeparator the separator that the database uses to separate the name of a
   *                        database schema and a database object located in the schema
   * @param schemaPrefix    the schema prefix to append to database objects reference by the DAO
   * @param idQuote         the string used quote SQL identifiers
   *
   * @param metaData the database meta data
   *
   * @throws SQLException if a database error occurs
   */
  protected void buildStatements(String schema, String schemaSeparator, String schemaPrefix,
      String idQuote)
    throws SQLException
  {
    // cachedCodeCategoryExistsSQL
    cachedCodeCategoryExistsSQL = "SELECT ID" + " FROM " + schemaPrefix + "CACHED_CODE_CATEGORIES"
        + " WHERE ID=?";

    // codeCategoryExistsSQL
    codeCategoryExistsSQL = "SELECT ID" + " FROM " + schemaPrefix + "CODE_CATEGORIES"
        + " WHERE ID=?";

    // createCachedCodeCategorySQL
    createCachedCodeCategorySQL = "INSERT INTO " + schemaPrefix + "CACHED_CODE_CATEGORIES"
        + " (ID, CODE_DATA, LAST_UPDATED, CACHED)" + " VALUES (?, ?, ?, ?)";

    // createCachedCodeSQL
    createCachedCodeSQL = "INSERT INTO " + schemaPrefix + "CACHED_CODES"
        + " (ID, CATEGORY_ID, NAME, DESCRIPTION, VALUE)" + " VALUES (?, ?, ?, ?, ?)";



    // createCodeSQL
    createCodeSQL = "INSERT INTO " + schemaPrefix + "CODES"
        + " (ID, CATEGORY_ID, NAME, DESCRIPTION, VALUE)" + " VALUES (?, ?, ?, ?, ?)";

    // deleteCachedCodeCategorySQL
    deleteCachedCodeCategorySQL = "DELETE FROM " + schemaPrefix
        + "CACHED_CODE_CATEGORIES WHERE ID=?";

    // deleteCodeCategorySQL
    deleteCodeCategorySQL = "DELETE FROM " + schemaPrefix + "CODE_CATEGORIES WHERE ID=?";

    // deleteCodeSQL
    deleteCodeSQL = "DELETE FROM " + schemaPrefix + "CODES WHERE ID=?";

    // getCachedCodeCategoryCachedSQL
    getCachedCodeCategoryCachedSQL = "SELECT CACHED" + " FROM " + schemaPrefix
        + "CACHED_CODE_CATEGORIES" + " WHERE ID=?";

    // getCachedCodeCategorySQL
    getCachedCodeCategorySQL = "SELECT ID, CODE_DATA, LAST_UPDATED, CACHED" + " FROM "
        + schemaPrefix + "CACHED_CODE_CATEGORIES" + " WHERE ID=?";

    // getCodeCategoriesForOrganisationSQL
    getCodeCategoriesForOrganisationSQL = "SELECT ID, ORGANISATION, CATEGORY_TYPE, NAME,"
        + " DESCRIPTION, CODE_DATA, ENDPOINT, IS_ENDPOINT_SECURE, IS_CACHEABLE, CACHE_EXPIRY,"
        + " CREATED, CREATED_BY, UPDATED, UPDATED_BY" + " FROM " + schemaPrefix + "CODE_CATEGORIES"
        + " WHERE ORGANISATION=?" + " ORDER BY NAME";

    // getCodeCategoriesNoDataForOrganisationSQL
    getCodeCategoriesNoDataForOrganisationSQL = "SELECT ID, ORGANISATION, CATEGORY_TYPE, NAME,"
        + " DESCRIPTION, ENDPOINT, IS_ENDPOINT_SECURE, IS_CACHEABLE, CACHE_EXPIRY,"
        + " CREATED, CREATED_BY, UPDATED, UPDATED_BY" + " FROM " + schemaPrefix + "CODE_CATEGORIES"
        + " WHERE ORGANISATION=?" + " ORDER BY NAME";

    // getCodeCategoryCacheExpirySQL
    getCodeCategoryCacheExpirySQL = "SELECT CACHE_EXPIRY" + " FROM " + schemaPrefix
        + "CODE_CATEGORIES" + " WHERE ID=?";

    // getCodeCategorySQL
    getCodeCategorySQL = "SELECT ID, ORGANISATION, CATEGORY_TYPE, NAME, DESCRIPTION, CODE_DATA,"
        + " ENDPOINT, IS_ENDPOINT_SECURE, IS_CACHEABLE, CACHE_EXPIRY, CREATED, CREATED_BY,"
        + " UPDATED, UPDATED_BY FROM " + schemaPrefix + "CODE_CATEGORIES" + " WHERE ID=?";

    // getCodesForCodeCategorySQL
    getCodesForCodeCategorySQL = "SELECT ID, CATEGORY_ID, NAME, DESCRIPTION, VALUE" + " FROM "
        + schemaPrefix + "CODES" + " WHERE CATEGORY_ID=? ORDER BY NAME";

    // getCachedCodesForCachedCodeCategorySQL
    getCachedCodesForCachedCodeCategorySQL = "SELECT ID, CATEGORY_ID, NAME, DESCRIPTION, VALUE"
        + " FROM " + schemaPrefix + "CACHED_CODES" + " WHERE CATEGORY_ID=? ORDER BY NAME";

    // getCodeSQL
    getCodeSQL = "SELECT ID, CATEGORY_ID, NAME, DESCRIPTION, VALUE" + " FROM " + schemaPrefix
        + "CODES" + " WHERE ID=?";

    // getNumberOfCodesForCodeCategorySQL
    getNumberOfCodesForCodeCategorySQL = "SELECT COUNT(ID) FROM " + schemaPrefix + "CODES"
        + " WHERE CATEGORY_ID=?";

    // getNumberOfCodeCategoriesForOrganisationSQL
    getNumberOfCodeCategoriesForOrganisationSQL = "SELECT COUNT(ID) FROM " + schemaPrefix
        + "CODE_CATEGORIES" + " WHERE ORGANISATION=?";

    // updateCachedCodeCategorySQL
    updateCachedCodeCategorySQL = "UPDATE " + schemaPrefix + "CACHED_CODE_CATEGORIES"
        + " SET CODE_DATA=?, LAST_UPDATED, CACHED=? WHERE ID=?";

    // updateCodeCategorySQL
    updateCodeCategorySQL =
      "UPDATE " + schemaPrefix + "CODE_CATEGORIES"
      + " SET ORGANISATION=?, CATEGORY_TYPE=?, NAME=?, DESCRIPTION=?, CODE_DATA=?, ENDPOINT=?,"
      + " IS_ENDPOINT_SECURE=?, IS_CACHEABLE=?, CACHE_EXPIRY=?, UPDATED=?, UPDATED_BY=? WHERE ID=?";

    // updateCodeSQL
    updateCodeSQL = "UPDATE " + schemaPrefix + "CODES"
        + " SET CATEGORY_ID=?, NAME=?, DESCRIPTION=?, VALUE=? WHERE ID=?";
  }

  private CachedCodeCategory getCachedCodeCategory(ResultSet rs)
    throws SQLException
  {
    return new CachedCodeCategory(rs.getString(1), rs.getBytes(2), rs.getTimestamp(3),
        rs.getTimestamp(4));
  }

  private Date getCachedCodeCategoryCached(Connection connection, String id)
    throws SQLException
  {
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      statement = connection.prepareStatement(getCachedCodeCategoryCachedSQL);
      statement.setString(1, id);

      rs = statement.executeQuery();

      if (rs.next())
      {
        return rs.getTimestamp(1);
      }
      else
      {
        return null;
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the cached date and time for the cached code"
          + " category (" + id + ") from the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
    }
  }

  private Code getCode(ResultSet rs)
    throws SQLException
  {
    return new Code(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
        rs.getString(5));
  }

  private CodeCategory getCodeCategory(ResultSet rs)
    throws SQLException
  {
    int cacheExpiry = rs.getInt(10);

    boolean cacheExpiryIsNull = rs.wasNull();

    return new CodeCategory(rs.getString(1), rs.getString(2),
        CodeCategoryType.fromCode(rs.getInt(3)), rs.getString(4), rs.getString(5), rs.getBytes(6),
        rs.getString(7), rs.getBoolean(8), rs.getBoolean(9), cacheExpiryIsNull
        ? null
        : new Integer(cacheExpiry), rs.getTimestamp(11), rs.getString(12), rs.getTimestamp(13),
        rs.getString(14));
  }

  private Integer getCodeCategoryCacheExpiry(Connection connection, String id)
    throws SQLException
  {
    PreparedStatement statement = null;
    ResultSet rs = null;

    try
    {
      statement = connection.prepareStatement(getCodeCategoryCacheExpirySQL);
      statement.setString(1, id);

      rs = statement.executeQuery();

      if (rs.next())
      {
        int cacheExpiry = rs.getInt(1);

        boolean cacheExpiryIsNull = rs.wasNull();

        if (cacheExpiryIsNull)
        {
          return null;
        }
        else
        {
          return new Integer(cacheExpiry);
        }
      }
      else
      {
        return null;
      }
    }
    catch (DAOException e)
    {
      throw e;
    }
    catch (Throwable e)
    {
      throw new DAOException("Failed to retrieve the cache expiry for the code category (" + id
          + ") from the database", e);
    }
    finally
    {
      DAOUtil.close(rs);
      DAOUtil.close(statement);
    }
  }

  private CodeCategory getCodeCategoryNoData(ResultSet rs)
    throws SQLException
  {
    int cacheExpiry = rs.getInt(9);

    boolean cacheExpiryIsNull = rs.wasNull();

    return new CodeCategory(rs.getString(1), rs.getString(2),
        CodeCategoryType.fromCode(rs.getInt(3)), rs.getString(4), rs.getString(5), rs.getString(6),
        rs.getBoolean(7), rs.getBoolean(8), cacheExpiryIsNull
        ? null
        : new Integer(cacheExpiry), rs.getTimestamp(10), rs.getString(11), rs.getTimestamp(12),
        rs.getString(13));
  }
}



#endif