﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace MMP.Application.Codes
{
  /// <summary>
  ///   The <b>CodeProviderConfig</b> class stores the configuration information for a code provider.
  /// </summary>
  public sealed class CodeProviderConfig
  {
    /// <summary>Constructs a new <b>CodeProviderConfig</b>.</summary>
    /// <param name="name">The name of the code provider.</param>
    /// <param name="className">The fully qualified name of the class that implements the code provider.</param>
    public CodeProviderConfig(string name, string className)
    {
      Name = name;
      ClassName = className;
    }

    /// <summary>The fully qualified name of the class that implements the code provider.</summary>
    public string ClassName { get; set; }

    /// <summary>The name of the code provider.</summary>
    public string Name { get; set; }
  }
}