﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;

namespace MMP.Application.Codes
{
  /// <summary>
  ///   The <b>ICodeProvider</b> interface defines the interface that must be implemented by all custom
  ///   code providers.
  /// </summary>
  [CLSCompliant(true)]
  public interface ICodeProvider
  {
    /// <summary>Retrieve the code category.</summary>
    /// <param name="codeCategory">The code provider code category.</param>
    /// <param name="lastRetrieved">The date and time the code category was last retrieved.</param>
    /// <param name="returnCodesIfCurrent">
    ///   Should the <b>Standard</b> codes and/or <b>Custom</b> code data be retrieved even if the code
    ///   category has not been updated after the date and time specified by the <b>lastRetrieved</b>
    ///   parameter.
    /// </param>
    /// <returns>
    ///   The code provider code category including the <b>Standard</b> codes and/or <b>Custom</b>
    ///   code data or <b>null</b> if the code category could not be found.
    /// </returns>
    /// <exception cref="MMP.Application.Codes.CodeProviderException" />
    CodeCategory GetCodeCategory(CodeCategory codeCategory, DateTime lastRetrieved, bool returnCodesIfCurrent);

    /// <summary>Retrieve the code category.</summary>
    /// <param name="codeCategory">The code provider code category.</param>
    /// <param name="parameters">The parameters.</param>
    /// <param name="lastRetrieved">The date and time the code category was last retrieved.</param>
    /// <param name="returnCodesIfCurrent">
    ///   Should the <b>Standard</b> codes and/or <b>Custom</b> code data be retrieved even if the code
    ///   category has not been updated after the date and time specified by the <b>lastRetrieved</b>
    ///   parameter.
    /// </param>
    /// <returns>
    ///   The code provider code category including the <b>Standard</b> codes and/or <b>Custom</b>
    ///   code data or <b>null</b> if the code category could not be found.
    /// </returns>
    /// <exception cref="MMP.Application.Codes.CodeProviderException" />
    CodeCategory GetCodeCategoryWithParameters(CodeCategory codeCategory, IDictionary<string, string> parameters,
      DateTime lastRetrieved, bool returnCodesIfCurrent);
  }
}