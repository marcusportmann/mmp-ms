﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MMP.Application.Codes;
using MMP.Common.Persistence;

namespace MMP.Application.Tests
{
  /// <summary>
  ///   The <b>CodesTests</b> class implements the unit tests for the codes functionality provided by the
  ///   <b>MMP.Application</b> library.
  /// </summary>
  [TestClass]
  public class CodesServiceTests
  {
    private const string DATABASE_NAME = "MMP.Application.Tests.CodesServiceTests";

    [ClassCleanup]
    public static void AfterTests()
    {
      PersistenceUtil.DropLocalDbDatabase(DATABASE_NAME);
    }

    [ClassInitialize]
    public static void BeforeTests(TestContext context)
    {
      PersistenceUtil.CreateLocalDbDatabase(context.TestDeploymentDir, DATABASE_NAME);

      using (
        var resourceStream =
          typeof (Registry.Registry).Assembly.GetManifestResourceStream(
            "MMP.Application.Persistence.MMP.Application.MSSQL.sql"))
      {
        PersistenceUtil.InitLocalDbDatabase(DATABASE_NAME, resourceStream);
      }
    }

    [TestMethod]
    public void TestCreateCodeCategory()
    {
      var codesService = GetCodesService();

      codesService.CreateCodeCategory(GetUncachedLocalStandardCodeCategory());
      codesService.CreateCodeCategory(GetCachedLocalStandardCodeCategory());
      codesService.CreateCodeCategory(GetUncachedLocalCustomCodeCategory());
      codesService.CreateCodeCategory(GetCachedLocalCustomCodeCategory());
      codesService.CreateCodeCategory(GetUncachedRemoteHttpServiceCodeCategory());
      codesService.CreateCodeCategory(GetCachedRemoteHttpServiceCodeCategory());
      codesService.CreateCodeCategory(GetUncachedRemoteWebServiceCodeCategory());
      codesService.CreateCodeCategory(GetCachedRemoteWebServiceCodeCategory());
    }

    [TestMethod]
    public void TestDeleteCodeCategory()
    {
      var codesService = GetCodesService();

      var codeCategory = GetCachedLocalStandardCodeCategory();

      codesService.CreateCodeCategory(codeCategory);

      Assert.IsTrue(codesService.DeleteCodeCategory(codeCategory.Id));
    }

    private static void CompareCodeCategories(CodeCategory codeCategory1, CodeCategory codeCategory2)
    {
      Assert.AreEqual(codeCategory1.CacheExpiry.HasValue, codeCategory2.CacheExpiry.HasValue);

      if ((codeCategory1.CacheExpiry.HasValue) && (codeCategory2.CacheExpiry.HasValue))
      {
        Assert.AreEqual(codeCategory1.CacheExpiry.Value, codeCategory2.CacheExpiry.Value);
      }

      Assert.AreEqual((codeCategory1.Codes == null), (codeCategory2.Codes == null));

      if ((codeCategory1.Codes != null) && (codeCategory2.Codes != null))
      {
        // TODO: Compare codes
      }

      Assert.AreEqual((codeCategory1.Data == null), (codeCategory2.Data == null));

      if ((codeCategory1.Data != null) && (codeCategory2.Data != null))
      {
        Assert.AreEqual((codeCategory1.Data.SequenceEqual(codeCategory2.Data)), true);
      }

      Assert.AreEqual(codeCategory1.Description, codeCategory2.Description);
      Assert.AreEqual(codeCategory1.EndPoint, codeCategory2.EndPoint);
      Assert.AreEqual(codeCategory1.Id, codeCategory2.Id);
      Assert.AreEqual(codeCategory1.IsCacheable, codeCategory2.IsCacheable);
      Assert.AreEqual(codeCategory1.IsEndPointSecure, codeCategory2.IsEndPointSecure);
      Assert.AreEqual(codeCategory1.Name, codeCategory2.Name);
      Assert.AreEqual(codeCategory1.OrganisationId, codeCategory2.OrganisationId);
      Assert.AreEqual(codeCategory1.Type, codeCategory2.Type);
      Assert.AreEqual(codeCategory1.Updated, codeCategory2.Updated);
    }

    private CodeCategory GetCachedLocalCustomCodeCategory()
    {
      return new CodeCategory
      {
        Id = Guid.NewGuid(),
        OrganisationId = 1,
        Type = CodeCategoryType.LocalCustom,
        Name = "Cached Local Custom Code Category Name",
        Description = "Cached Local Custom Code Category Description",
        Data = null,
        IsEndPointSecure = false,
        EndPoint = null,
        IsCacheable = true,
        CacheExpiry = CodeCategory.DEFAULT_CACHE_EXPIRY,
        Updated = DateTime.Now
      };
    }

    private CodeCategory GetCachedLocalStandardCodeCategory()
    {
      return new CodeCategory
      {
        Id = Guid.NewGuid(),
        OrganisationId = 1,
        Type = CodeCategoryType.LocalStandard,
        Name = "Cached Local Standard Code Category Name",
        Description = "Cached Local Standard Code Category Description",
        Data = null,
        IsEndPointSecure = false,
        EndPoint = null,
        IsCacheable = true,
        CacheExpiry = CodeCategory.DEFAULT_CACHE_EXPIRY,
        Updated = DateTime.Now
      };
    }

    private CodeCategory GetCachedRemoteHttpServiceCodeCategory()
    {
      return new CodeCategory
      {
        Id = Guid.NewGuid(),
        OrganisationId = 1,
        Type = CodeCategoryType.RemoteHttpService,
        Name = "Cached Remote HTTP Service Code Category Name",
        Description = "Cached Remote HTTP Service Code Category Description",
        Data = null,
        IsEndPointSecure = false,
        EndPoint = null,
        IsCacheable = true,
        CacheExpiry = CodeCategory.DEFAULT_CACHE_EXPIRY,
        Updated = DateTime.Now
      };
    }

    private CodeCategory GetCachedRemoteWebServiceCodeCategory()
    {
      return new CodeCategory
      {
        Id = Guid.NewGuid(),
        OrganisationId = 1,
        Type = CodeCategoryType.RemoteWebService,
        Name = "Cached Remote Web Service Code Category Name",
        Description = "Cached Remote Web Service Code Category Description",
        Data = null,
        IsEndPointSecure = false,
        EndPoint = null,
        IsCacheable = true,
        CacheExpiry = CodeCategory.DEFAULT_CACHE_EXPIRY,
        Updated = DateTime.Now
      };
    }

    private ICodesService GetCodesService()
    {
      ICodesDAO codesDAO =
        new CodesDAO($@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog={DATABASE_NAME};Integrated Security=SSPI;");

      return new CodesService(codesDAO);
    }

    private CodeCategory GetUncachedLocalCustomCodeCategory()
    {
      return new CodeCategory
      {
        Id = Guid.NewGuid(),
        OrganisationId = 1,
        Type = CodeCategoryType.LocalCustom,
        Name = "Uncached Local Custom Code Category Name",
        Description = "Uncached Local Custom Code Category Description",
        Data = null,
        IsEndPointSecure = false,
        EndPoint = null,
        IsCacheable = false,
        CacheExpiry = null,
        Updated = DateTime.Now
      };
    }

    private CodeCategory GetUncachedLocalStandardCodeCategory()
    {
      return new CodeCategory
      {
        Id = Guid.NewGuid(),
        OrganisationId = 1,
        Type = CodeCategoryType.LocalStandard,
        Name = "Uncached Local Standard Code Category Name",
        Description = "Uncached Local Standard Code Category Description",
        Data = null,
        IsEndPointSecure = false,
        EndPoint = null,
        IsCacheable = false,
        CacheExpiry = null,
        Updated = DateTime.Now
      };
    }

    private CodeCategory GetUncachedRemoteHttpServiceCodeCategory()
    {
      return new CodeCategory
      {
        Id = Guid.NewGuid(),
        OrganisationId = 1,
        Type = CodeCategoryType.RemoteHttpService,
        Name = "Uncached Remote HTTP Service Code Category Name",
        Description = "Uncached Remote HTTP Service Code Category Description",
        Data = null,
        IsEndPointSecure = false,
        EndPoint = null,
        IsCacheable = false,
        CacheExpiry = null,
        Updated = DateTime.Now
      };
    }

    private CodeCategory GetUncachedRemoteWebServiceCodeCategory()
    {
      return new CodeCategory
      {
        Id = Guid.NewGuid(),
        OrganisationId = 1,
        Type = CodeCategoryType.RemoteWebService,
        Name = "Uncached Remote Web Service Code Category Name",
        Description = "Uncached Remote Web Service Code Category Description",
        Data = null,
        IsEndPointSecure = false,
        EndPoint = null,
        IsCacheable = false,
        CacheExpiry = null,
        Updated = DateTime.Now
      };
    }
  }
}