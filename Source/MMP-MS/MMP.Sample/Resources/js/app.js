'use strict';

var app = angular.module('xenon-app', [
	'ngCookies',

	'ui.router',
	'ui.bootstrap',

	'oc.lazyLoad',

	'xenon.controllers',
	'xenon.directives',
	'xenon.factory',
	'xenon.services',

	// Added in v1.3
	'FBAngular'
]);

app.run(function()
{
	// Page Loading Overlay
	public_vars.$pageLoadingOverlay = jQuery('.page-loading-overlay');

	jQuery(window).load(function()
	{
		public_vars.$pageLoadingOverlay.addClass('loaded');
	})
});


app.config(function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, ASSETS){

	$urlRouterProvider.otherwise('/app/dashboard-variant-1');

	$stateProvider.
		// Main Layout Structure
		state('app', {
			abstract: true,
			url: '/app',
			templateUrl: applicationPathHelper.mvcTemplatePath('Layout/ApplicationBody/'),
			controller: function($rootScope){
				$rootScope.isLoginPage        = false;
				$rootScope.isLightLoginPage   = false;
				$rootScope.isLockscreenPage   = false;
				$rootScope.isMainPage         = true;
			}
		}).

		// Dashboards
		state('app.dashboard-variant-1', {
			url: '/dashboard-variant-1',
			templateUrl: applicationPathHelper.applicationTemplatePath('dashboards/variant-1'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
					]);
				},
			}
		}).

		// Update Highlights
		state('app.update-highlights', {
			url: '/update-highlights',
			templateUrl: applicationPathHelper.mvcTemplatePath('Home/UpdateHighlights/'),
		}).

		// Layouts
		state('app.layout-and-skins', {
			url: '/layout-and-skins',
			templateUrl: applicationPathHelper.applicationTemplatePath('layout-and-skins'),
		}).

		// UI Elements
		state('app.ui-panels', {
			url: '/ui-panels',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/panels'),
		}).
		state('app.ui-buttons', {
			url: '/ui-buttons',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/buttons')
		}).
		state('app.ui-tabs-accordions', {
			url: '/ui-tabs-accordions',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/tabs-accordions')
		}).
		state('app.ui-modals', {
			url: '/ui-modals',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/modals'),
			controller: 'UIModalsCtrl'
		}).
		state('app.ui-breadcrumbs', {
			url: '/ui-breadcrumbs',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/breadcrumbs')
		}).
		state('app.ui-blockquotes', {
			url: '/ui-blockquotes',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/blockquotes')
		}).
		state('app.ui-progress-bars', {
			url: '/ui-progress-bars',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/progress-bars')
		}).
		state('app.ui-navbars', {
			url: '/ui-navbars',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/navbars')
		}).
		state('app.ui-alerts', {
			url: '/ui-alerts',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/alerts')
		}).
		state('app.ui-pagination', {
			url: '/ui-pagination',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/pagination')
		}).
		state('app.ui-typography', {
			url: '/ui-typography',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/typography')
		}).
		state('app.ui-other-elements', {
			url: '/ui-other-elements',
			templateUrl: applicationPathHelper.applicationTemplatePath('ui/other-elements')
		}).

		// Tables
		state('app.tables-basic', {
			url: '/tables-basic',
			templateUrl: applicationPathHelper.applicationTemplatePath('tables/basic'),
		}).
		state('app.tables-responsive', {
			url: '/tables-responsive',
			templateUrl: applicationPathHelper.applicationTemplatePath('tables/responsive'),
			resolve: {
				deps: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.tables.rwd,
					]);
				}
			}
		}).
		state('app.tables-datatables', {
			url: '/tables-datatables',
			templateUrl: applicationPathHelper.applicationTemplatePath('tables/datatables'),
			resolve: {
				deps: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.tables.datatables,
					]);
				},
			}
		}).

		// Forms
		state('app.forms-native', {
			url: '/forms-native',
			templateUrl: applicationPathHelper.applicationTemplatePath('forms/native-elements'),
		}).
		state('app.forms-advanced', {
			url: '/forms-advanced',
			templateUrl: applicationPathHelper.applicationTemplatePath('forms/advanced-plugins'),
			resolve: {
			    formsAdvancedDependencies: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
                        ASSETS.forms.jQueryUITimepicker,
						ASSETS.forms.select2,
						ASSETS.forms.jQuerySelectBoxIt,
						ASSETS.forms.jQueryMultiSelect,
						ASSETS.forms.bootstrapTagsInput,
						ASSETS.forms.bootstrapDatepicker,
						ASSETS.forms.bootstrapTimepicker
					], { serie: true });
			    },
			}
		}).
		state('app.forms-wizard', {
			url: '/forms-wizard',
			templateUrl: applicationPathHelper.applicationTemplatePath('forms/form-wizard'),
			resolve: {
				formsWizardDependencies: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.bootstrap,
						ASSETS.core.jQueryUI,
						ASSETS.forms.jQueryValidate,
						ASSETS.forms.jQueryInputmask,
						ASSETS.forms.jQueryMultiSelect,
						ASSETS.forms.jQuerySelectBoxIt,
						ASSETS.forms.jQueryBootstrapWizard,
					]);
				},
			},
		}).
		state('app.forms-validation', {
			url: '/forms-validation',
			templateUrl: applicationPathHelper.applicationTemplatePath('forms/form-validation'),
			resolve: {
				jQueryValidate: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
					]);
				},
			},
		}).
		state('app.forms-input-masks', {
			url: '/forms-input-masks',
			templateUrl: applicationPathHelper.applicationTemplatePath('forms/input-masks'),
			resolve: {
				inputmask: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryInputmask,
					]);
				},
			},
		}).
		state('app.forms-file-upload', {
			url: '/forms-file-upload',
			templateUrl: applicationPathHelper.applicationTemplatePath('forms/file-upload'),
			resolve: {
				dropzone: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.dropzone,
					]);
				},
			}
		}).
		state('app.forms-sliders', {
			url: '/forms-sliders',
			templateUrl: applicationPathHelper.applicationTemplatePath('forms/sliders'),
			resolve: {
				sliders: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.core.jQueryUI,
					]);
				},
			},
		}).
		state('app.forms-icheck', {
			url: '/forms-icheck',
			templateUrl: applicationPathHelper.applicationTemplatePath('forms/icheck'),
			resolve: {
				iCheck: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.icheck,
					]);
				},
			}
		}).

		// Extra
		state('app.extra-search', {
			url: '/extra-search',
			templateUrl: applicationPathHelper.applicationTemplatePath('extra/search')
		}).
		state('app.extra-invoice', {
			url: '/extra-invoice',
			templateUrl: applicationPathHelper.applicationTemplatePath('extra/invoice')
		}).
		state('app.extra-page-404', {
			url: '/extra-page-404',
			templateUrl: applicationPathHelper.applicationTemplatePath('extra/page-404')
		}).
		state('app.extra-loading-progress', {
			url: '/extra-loading-progress',
			templateUrl: applicationPathHelper.applicationTemplatePath('extra/loading-progress')
		}).
		state('app.extra-notifications', {
			url: '/extra-notifications',
			templateUrl: applicationPathHelper.applicationTemplatePath('extra/notifications'),
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.extra.toastr,
					]);
				},
			}
		}).
		state('app.extra-scrollable', {
			url: '/extra-scrollable',
			templateUrl: applicationPathHelper.applicationTemplatePath('extra/scrollable')
		}).
		state('app.extra-blank-page', {
			url: '/extra-blank-page',
			templateUrl: applicationPathHelper.applicationTemplatePath('extra/blank-page')
		}).

		// Members
		state('app.extra-members-list', {
			url: '/extra-members-list',
			templateUrl: applicationPathHelper.applicationTemplatePath('extra/members-list')
		}).

		// Logins and Lockscreen
		state('login', {
			url: '/login',
			templateUrl: applicationPathHelper.applicationTemplatePath('login'),
			controller: 'LoginCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
			}
		}).
		state('login-light', {
			url: '/login-light',
			templateUrl: applicationPathHelper.applicationTemplatePath('login-light'),
			controller: 'LoginLightCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
					]);
				},
			}
		}).
		state('lockscreen', {
			url: '/lockscreen',
			templateUrl: applicationPathHelper.applicationTemplatePath('lockscreen'),
			controller: 'LockscreenCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
			}
		});
});


app.constant('ASSETS', {
	'core': {
		'bootstrap': applicationPathHelper.templateResourcePath('thirdparty/bootstrap/bootstrap.min.js'), // Some plugins which do not support angular needs this

		'jQueryUI': [
			applicationPathHelper.templateResourcePath('thirdparty/jquery-ui/jquery-ui.min.js'),
			applicationPathHelper.templateResourcePath('thirdparty/jquery-ui/jquery-ui.structure.min.css'),
		],

		'moment': applicationPathHelper.templateResourcePath('thirdparty/moment/moment.min.js')
	},

	'tables': {
		'rwd': applicationPathHelper.templateResourcePath('thirdparty/rwd-table/js/rwd-table.min.js'),

		'datatables': [
            applicationPathHelper.templateResourcePath('thirdparty/angular-datatables/angular-datatables.min.js'),
			applicationPathHelper.templateResourcePath('thirdparty/jquery-datatables/css/jquery.dataTables.min.css'),
			applicationPathHelper.templateResourcePath('thirdparty/jquery-datatables/js/jquery.dataTables.min.js'),
			applicationPathHelper.templateResourcePath('thirdparty/jquery-datatables/js/jquery.dataTables.yadcf.min.js'),
		],

	},

	'forms': {

		'select2': [
			applicationPathHelper.templateResourcePath('thirdparty/select2/select2.css'),
			applicationPathHelper.templateResourcePath('thirdparty/select2/select2-bootstrap.css'),
			applicationPathHelper.templateResourcePath('thirdparty/select2/select2.min.js'),
		],

		'jQuerySelectBoxIt': applicationPathHelper.templateResourcePath('thirdparty/jquery-selectboxit/jquery.selectBoxIt.min.js'),

		'bootstrapTagsInput': applicationPathHelper.templateResourcePath('thirdparty/bootstrap-tagsinput/bootstrap-tagsinput.min.js'),

		'bootstrapDatepicker': applicationPathHelper.templateResourcePath('thirdparty/bootstrap-datepicker/bootstrap-datepicker.min.js'),

		'bootstrapTimepicker': applicationPathHelper.templateResourcePath('thirdparty/bootstrap-timepicker/bootstrap-timepicker.min.js'),

		'jQueryInputmask': applicationPathHelper.templateResourcePath('thirdparty/jquery-inputmask/jquery.inputmask.bundle.min.js'),

		'jQueryBootstrapWizard': applicationPathHelper.templateResourcePath('thirdparty/jquery-bootstrap-wizard/jquery.bootstrap.wizard.min.js'),

		'jQueryUITimepicker': applicationPathHelper.templateResourcePath('thirdparty/jquery-ui-timepicker/jquery-ui-timepicker-addon.min.js'),

		'jQueryValidate': applicationPathHelper.templateResourcePath('thirdparty/jquery-validate/jquery.validate.min.js'),

		'dropzone': [
			applicationPathHelper.templateResourcePath('thirdparty/dropzone/dropzone.css'),
			applicationPathHelper.templateResourcePath('thirdparty/dropzone/dropzone.min.js'),
		],

		'jQueryMultiSelect': [
			applicationPathHelper.templateResourcePath('thirdparty/jquery-multiselect/css/multi-select.css'),
			applicationPathHelper.templateResourcePath('thirdparty/jquery-multiselect/js/jquery.multi-select.js'),
		],

		'icheck': [
			applicationPathHelper.templateResourcePath('thirdparty/icheck/skins/all.css'),
			applicationPathHelper.templateResourcePath('thirdparty/icheck/icheck.min.js'),
		],
	},

	'extra': {

		'toastr': applicationPathHelper.templateResourcePath('thirdparty/toastr/toastr.min.js'),
	}
});