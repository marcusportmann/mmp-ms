﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Web.Mvc;
using System.Web.Routing;
using MMP.Application.Mvc.Template;
using MMP.Application.Mvc.Template.Navigation;

namespace MMP.Sample
{
  /// <summary>
  ///   The <b>SampleApplication</b> class implements the application-specific web application class for
  ///   the Sample application.
  /// </summary>
  public class SampleApplication : TemplateWebApplication<SampleSession>
  {
    /// <summary>The user-friendly name that should be displayed for the application.</summary>
    public override string ApplicationDisplayName => "Sample";

    /// <summary>Returns the name of the application.</summary>
    /// <remarks>
    ///   The application name is used to retrieve the configuration for the application stored in the
    ///   registry under the key
    ///   <b>HKLM\SOFTWARE\{ProviderName}\{ApplicationName}</b>.
    /// </remarks>
    public override string ApplicationName => "Sample";

    /// <summary>Returns the name of the application provider.</summary>
    /// <remarks>
    ///   The application provider name is used to retrieve the configuration for the application stored in
    ///   the registry under the key
    ///   <b>HKLM\SOFTWARE\{ProviderName}\{ApplicationName}</b>.
    /// </remarks>
    public override string ProviderName => "MMP";

    /*
    private IList<Assembly> GetWebApplicationAssemblies()
    {
      var webApplicationAssemblies = new List<Assembly>();

      var webApplicationAssembly = GetWebApplicationAssembly();


      var referencedAssemblies = webApplicationAssembly.GetReferencedAssemblies();

      referencedAssemblies

      foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
      {
        
      }



      return webApplicationAssemblies;
    }
    */

    /// <summary>Initialise the application.</summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>Application_Start</b> method, as part of the
    ///     global initialisation of the ASP.NET application.
    ///   </para>
    /// </remarks>
    protected override void InitApplication()
    {
      base.InitApplication();
    }

    /// <summary>
    ///   This method must be implemented by all application-specific <b>TemplateWebApplication</b>
    ///   subclasses to setup the navigation hierarchy for the application.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off initialisation of the
    ///     navigation hierarchy.
    ///   </para>
    /// </remarks>
    /// <param name="navigation">The root of the navigation hierarchy.</param>
    protected override void InitNavigation(NavigationGroup navigation)
    {
      navigation.AddItem(new NavigationLink("Dashboard", "Index", "Home"));

      var formsGroup = new NavigationGroup("Forms");
      formsGroup.AddItem(new NavigationLink("Native Elements", "NativeFormElements", "Home"));
      formsGroup.AddItem(new NavigationLink("Advanced Controls", "AdvancedFormControls", "Home"));
      formsGroup.AddItem(new NavigationLink("Wizard", "FormWizard", "Home"));

      navigation.AddItem(formsGroup);
    }

    /// <summary>Register the ASP.NET MVC web application routes.</summary>
    /// <remarks>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off initialisation of the
    ///     ASP.NET MVC web application routes.
    ///   </para>
    /// </remarks>
    protected override void RegisterRoutes()
    {
      RouteTable.Routes.MapRoute("AdvancedFormControls", "{controller}/{action}/{id}",
        new {controller = "Home", action = "AdvancedFormControls", id = UrlParameter.Optional});
    }

    /// <summary>Register the application-specific types with the IOC container.</summary>
    /// <remarks>
    ///   <para>
    ///     You cannot use the logging infrastructure while registering the default and
    ///     application-specific types with the IOC container.
    ///   </para>
    ///   <para>
    ///     Types should be registered using the <b>MMP.Common.IOC.Container</b> static wrapper around the
    ///     IOC container.
    ///   </para>
    ///   <para>
    ///     This method will only be called once by the <b>InitApplication</b> method, as part of the
    ///     global initialisation of the ASP.NET application, to perform the once-off registration of the
    ///     application-specific types with the IOC container.
    ///   </para>
    /// </remarks>
    protected override void RegisterTypes()
    {
    }
  }
}