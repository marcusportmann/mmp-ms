﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace MMP.Workflow
{
  /// <summary>The <b>ExecutingWorkflow</b> stores the information for an executing workflow.</summary>
  [Serializable]
  [CLSCompliant(true)]
  public sealed class ExecutingWorkflow
  {
    /// <summary>Constructs a new <b>ExecutingWorkflow</b>.</summary>
    /// <param name="id">The ID uniquely identifying the executing workflow.</param>
    /// <param name="definitionName">The name of the workflow definition for the workflow.</param>
    /// <param name="definitionId">
    ///   The Universally Unique Identifier (UUID) used to identify the workflow definition the executing
    ///   workflow is associated with.
    /// </param>
    /// <param name="definitionVersion">The version of the workflow definition for the workflow.</param>
    public ExecutingWorkflow(string id, string definitionName, string definitionId, int definitionVersion)
    {
      Id = id;
      DefinitionName = definitionName;
      DefinitionId = definitionId;
      DefinitionVersion = definitionVersion;
    }

    /// <summary>
    ///   The Universally Unique Identifier (UUID) used to identify the workflow definition the executing
    ///   workflow is associated with.
    /// </summary>
    public string DefinitionId { get; set; }

    /// <summary>The name of the workflow definition for the executing workflow.</summary>
    public string DefinitionName { get; set; }

    /// <summary>
    ///   The version of the workflow definition the executing workflow is associated with.
    /// </summary>
    public int DefinitionVersion { get; set; }

    /// <summary>The ID uniquely identifying the executing workflow.</summary>
    public string Id { get; set; }
  }
}