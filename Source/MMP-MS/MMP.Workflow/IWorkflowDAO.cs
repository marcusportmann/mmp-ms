﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;

namespace MMP.Workflow
{
  /// <summary>The <b>IWorkflowDAO</b> interface defines the workflow persistence operations.</summary>
  public interface IWorkflowDAO
  {
    /// <summary>Create the entry for the workflow in the database.</summary>
    /// <param name="workflow">The <b>Workflow</b> instance containing the information for the workflow.</param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    void CreateWorkflow(Workflow workflow);

    /// <summary>Delete the workflow with the specified ID.</summary>
    /// <param name="id">The ID uniquely identifying the workflow.</param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    void DeleteWorkflow(string id);

    /// <summary>Retrieve the latest version of the workflow definition with the specified ID.</summary>
    /// <param name="id">The ID uniquely identifying the workflow definition.</param>
    /// <returns>
    ///   The latest version of the workflow definition with the specified ID or <b>null</b> if the
    ///   workflow definition could not be found.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    WorkflowDefinition GetLatestWorkflowDefinition(string id);

    /// <summary>
    ///   Retrieve the next workflow that should be executed.
    ///   <para>The workflow will be locked to prevent duplicate execution.</para>
    /// </summary>
    /// <param name="lockName">
    ///   The name of the lock that should be applied to the workflow when it is retrieved.
    /// </param>
    /// <returns>
    ///   The next workflow that should be executed or <b>null</b> if no  workflows are currently awaiting
    ///   execution.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    Workflow GetNextPendingWorkflow(string lockName);

    /// <summary>Retrieve the workflow with the specified ID.</summary>
    /// <param name="id">The ID uniquely identifying the workflow.</param>
    /// <returns>
    ///   The workflow with the specified ID or <b>null</b> if the workflow could not be found.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    Workflow GetWorkflow(string id);

    /// <summary>Retrieve the workflow definition with the specified ID and version.</summary>
    /// <param name="id">The ID uniquely identifying the workflow definition.</param>
    /// <param name="version">The version for the workflow definition.</param>
    /// <returns>
    ///   The workflow definition with the specified ID and version or <b>null</b> if the workflow
    ///   definition could not be found.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    WorkflowDefinition GetWorkflowDefinition(string id, int version);

    /// <summary>
    ///   Returns the workflow definition "key" used to uniquely identify a combination of workflow
    ///   definition ID and workflow definition version.
    /// </summary>
    /// <param name="definitionId">The UUID identifying the workflow definition.</param>
    /// <param name="definitionVersion">The version of the workflow definition.</param>
    /// <returns>
    ///   The workflow definition "key" used to uniquely identify a combination of workflow definition ID
    ///   and workflow definition version.
    /// </returns>
    string GetWorkflowDefinitionKey(string definitionId, int definitionVersion);

    /// <summary>Returns the list of active and in-active workflow definitions.</summary>
    /// <returns>The list of active and in-active workflow definitions.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    IList<WorkflowDefinition> GetWorkflowDefinitions();

    /// <summary>
    ///   Refresh the keep-alive for the lock with the specified name used to lock workflows for
    ///   processing.
    /// </summary>
    /// <param name="lockName">The name of the lock.</param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    void RefreshLockKeepAlive(string lockName);

    /// <summary>Reset the workflow locks.</summary>
    /// <param name="lockName">The name of the lock applied by the entity that has locked the workflows.</param>
    /// <param name="status">The current status of the workflows that have been locked.</param>
    /// <param name="newStatus">The new status for the workflows that have been unlocked.</param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    void ResetWorkflowLocks(string lockName, WorkflowStatus status, WorkflowStatus newStatus);

    /// <summary>Unlock the workflow.</summary>
    /// <param name="id">The ID uniquely identifying the workflow to unlock.</param>
    /// <param name="status">The new status for the unlocked workflow.</param>
    /// <param name="pendingTimer">The pending timer for the workflow.</param>
    /// <param name="resetLastProcessedTime">
    ///   <b>True</b> if the last processed time for the workflow should be reset or
    ///   <b>false</b> otherwise.
    /// </param>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    void UnlockWorkflow(string id, WorkflowStatus status, DateTime? pendingTimer, bool resetLastProcessedTime);

    /// <summary>Update the workflow in the database.</summary>
    /// <param name="id">The ID uniquely identifying the workflow to update.</param>
    /// <param name="status">The new status for the workflow.</param>
    /// <param name="data">The updated data for the workflow.</param>
    /// <param name="pendingTimer">The date and time the workflow must next be run.</param>
    /// <param name="lockName">The name of the entity that has locked the workflow for processing.</param>
    /// <returns><b>True</b> if the workflow was updated or <b>False</b> otherwise.</returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    bool UpdateWorkflow(string id, WorkflowStatus status, byte[] data, DateTime? pendingTimer, string lockName);

    /// <summary>Update the data for the workflow in the database.</summary>
    /// <param name="id">The ID uniquely identifying the workflow whose data should be updated.</param>
    /// <param name="data">The updated data for the workflow.</param>
    /// <returns>
    ///   <b>True</b> if the workflow data was updated or <b>False</b> otherwise.
    /// </returns>
    /// <exception cref="MMP.Common.Persistence.DAOException" />
    bool UpdateWorkflowData(string id, byte[] data);
  }
}