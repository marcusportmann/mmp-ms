﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMP.Workflow
{
  /// <summary>The enumeration giving the possible statuses for a workflow.</summary>
  [CLSCompliant(true)]
  public enum WorkflowStatus
  {
    /// <summary>New.</summary>
    New = 0x00,

    /// <summary>Idle.</summary>
    Idle = 0x01,

    /// <summary>Executing.</summary>
    Executing = 0x02,

    /// <summary>Faulted.</summary>
    Faulted = 0x03,

    /// <summary>Cancelled.</summary>
    Cancelled = 0x04,

    /// <summary>Completed.</summary>
    Completed = 0x05
  }

  /// <summary>The <b>Workflow</b> class holds the information for a workflow definition.</summary>
  [Serializable]
  [CLSCompliant(true)]
  [Table("Workflow", Schema = "MMP")]
  public sealed class Workflow
  {
    /// <summary>The date and time the workflow was created.</summary>
    [Required]
    public DateTime Created { get; set; }

    /// <summary>The data giving the serialized state of the workflow.</summary>
    [Required]
    public byte[] Data { get; set; }

    /// <summary>
    ///   The Universally Unique Identifier (UUID) used to identify the workflow definition the workflow is
    ///   associated with.
    /// </summary>
    [Required]
    public Guid DefinitionId { get; set; }

    /// <summary>The version of the workflow definition the workflow is associated with.</summary>
    [Required]
    public int DefinitionVersion { get; set; }

    /// <summary>The Universally Unique Identifier (UUID) used to uniquely identify the workflow.</summary>
    [Key]
    public Guid Id { get; set; }

    /// <summary>The date and time the last attempt was made to process the workflow.</summary>
    public DateTime? LastProcessed { get; set; }

    /// <summary>The name of the entity that has locked the workflow for processing.</summary>
    [StringLength(100)]
    public string LockName { get; set; }

    /// <summary>The date and time the workflow must next be run.</summary>
    public DateTime? PendingTimer { get; set; }

    /// <summary>
    ///   The code giving the status of the workflow e.g. 0=New, 1=Idle, 2=Executing, 3=Faulted,
    ///   4=Cancelled, 5=Completed, etc.
    /// </summary>
    [Required]
    public WorkflowStatus Status { get; set; }

    /// <summary>The date and time the workflow was updated</summary>
    public DateTime? Updated { get; set; }
  }
}