﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMP.Workflow
{
  /// <summary>
  ///   The <b>WorkflowDefinition</b> class holds the information for a workflow definition.
  /// </summary>
  [Serializable]
  [CLSCompliant(true)]
  [Table("WorkflowDefinition", Schema = "MMP")]
  public sealed class WorkflowDefinition
  {
    /// <summary>The date and time the workflow definition was created.</summary>
    [Required]
    public DateTime Created { get; set; }

    /// <summary>The user ID identifying the user that created the workflow definition.</summary>
    [Required]
    [StringLength(40)]
    public string CreatedBy { get; set; }

    /// <summary>
    ///   The Universally Unique Identifier (UUID) used to uniquely identify the workflow definition.
    /// </summary>
    [Key, Column(Order = 0)]
    public Guid Id { get; set; }

    /// <summary>
    ///   Is the workflow definition active or historical i.e. can new workflow instances be created from
    ///   this workflow definition.
    /// </summary>
    [Required]
    public bool IsActive { get; set; }

    /// <summary>The name of the workflow definition.</summary>
    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    /// <summary>The date and time the workflow definition was updated.</summary>
    public DateTime? Updated { get; set; }

    /// <summary>The user ID identifying the user that updated the workflow definition.</summary>
    [StringLength(40)]
    public string UpdatedBy { get; set; }

    /// <summary>The version of the workflow definition.</summary>
    [Key, Column(Order = 1)]
    public int Version { get; set; }

    /// <summary>The XAML implementation of the workflow for the workflow definition.</summary>
    [Required]
    public string Xaml { get; set; }
  }
}