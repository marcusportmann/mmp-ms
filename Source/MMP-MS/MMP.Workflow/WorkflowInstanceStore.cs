﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Activities.DurableInstancing;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.DurableInstancing;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using MMP.Common.IOC;
using NLog;

namespace MMP.Workflow
{
  /// <summary>
  ///   The <b>WorkflowInstanceStore</b> class implements the Workflow Foundation persistence provider.
  /// </summary>
  internal class WorkflowInstanceStore : InstanceStore
  {
    /// <summary>The logger.</summary>
    private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public static readonly XNamespace WORKFLOW_NAMESPACE =
      XNamespace.Get("urn:schemas-microsoft-com:System.Activities/4.0/properties");

    private readonly Guid _ownerInstanceId;
    private XName _statusInstanceValueName;
    private XName _timerExpirationTimeInstanceValueName;
    private IWorkflowDAO _workflowDAO;
    //private XName _workflowInstanceValueName;

    /// <summary>Constructs a new <b>WorkflowInstanceStore</b>.</summary>
    public WorkflowInstanceStore() : this(Guid.NewGuid())
    {
    }

    /// <summary>Constructs a new <b>WorkflowInstanceStore</b>.</summary>
    /// <param name="ownerInstanceId">The owner instance ID.</param>
    public WorkflowInstanceStore(Guid ownerInstanceId)
    {
      _ownerInstanceId = ownerInstanceId;
    }

    private XName StatusInstanceValueName
    {
      get { return _statusInstanceValueName ?? (_statusInstanceValueName = WORKFLOW_NAMESPACE.GetName("Status")); }
    }

    private XName TimerExpirationTimeInstanceValueName
    {
      get
      {
        return _timerExpirationTimeInstanceValueName ??
               (_timerExpirationTimeInstanceValueName = WORKFLOW_NAMESPACE.GetName("TimerExpirationTime"));
      }
    }

    /// <summary>The DAO providing workflow persistence capabilities.</summary>
    private IWorkflowDAO WorkflowDAO
    {
      get { return _workflowDAO ?? (_workflowDAO = Container.Resolve<IWorkflowDAO>()); }
    }

    //private XName WorkflowInstanceValueName
    //{
    //  get
    //  {
    //    return _workflowInstanceValueName ?? (_workflowInstanceValueName = WORKFLOW_NAMESPACE.GetName("Workflow"));
    //  }
    //}

    /// <summary>
    ///   Determines whether the persistence command can be executed and if the command can be executed
    ///   executes it asynchronously.
    /// </summary>
    /// <param name="context">The instance context.</param>
    /// <param name="command">The command to be executed.</param>
    /// <param name="timeout">Timeout value for the operation.</param>
    /// <param name="callback">
    ///   The asynchronous callback delegate that receives notification of the completion of the
    ///   asynchronous operation.
    /// </param>
    /// <param name="state">The state information.</param>
    /// <returns>The state of the asynchronous operation.</returns>
    protected override IAsyncResult BeginTryCommand(InstancePersistenceContext context,
      InstancePersistenceCommand command, TimeSpan timeout, AsyncCallback callback, object state)
    {
      //The CreateWorkflowOwner command instructs the instance store to create a new instance owner bound to the instanace handle
      if (command is CreateWorkflowOwnerCommand)
      {
        context.BindInstanceOwner(_ownerInstanceId, Guid.NewGuid());
      }
      //The SaveWorkflow command instructs the instance store to modify the instance bound to the instance handle or an instance key
      else if (command is SaveWorkflowCommand)
      {
        SaveWorkflow(context.InstanceView.InstanceId, ((SaveWorkflowCommand) command).InstanceData);
      }
      //The LoadWorkflow command instructs the instance store to lock and load the instance bound to the identifier in the instance handle
      else if (command is LoadWorkflowCommand)
      {
        var instanceData = LoadWorkflow(context.InstanceView.InstanceId);

        context.LoadedInstance(InstanceState.Initialized, instanceData, null, null, null);
      }

      return new CompletedAsyncResult<bool>(true, callback, state);
    }

    /// <summary>Ends an asynchronous operation.</summary>
    /// <param name="result">The result of the operation.</param>
    /// <returns>
    ///   <b>False</b> if it doesn’t support the command passed to the BeginTryCommand method. Otherwise it
    ///   returns <b>True</b>
    ///   or throws an exception.
    /// </returns>
    protected override bool EndTryCommand(IAsyncResult result)
    {
      return CompletedAsyncResult<bool>.End(result);
    }

    /// <summary>
    ///   Determines whether the persistence command can be executed and if the command can be executed
    ///   executes it synchronously.
    ///   <para />
    ///   NOTE: This implementation provides a synchronous version of the Begin/EndTryCommand functions.
    /// </summary>
    /// <param name="context">The instance context.</param>
    /// <param name="command">The command to be executed.</param>
    /// <param name="timeout">Timeout value for the operation.</param>
    /// <returns>
    ///   <b>False</b> if the persistence provider doesn’t support the command passed as a parameter;
    ///   otherwise returns
    ///   <b>True</b> or throws an exception.
    /// </returns>
    protected override bool TryCommand(InstancePersistenceContext context, InstancePersistenceCommand command,
      TimeSpan timeout)
    {
      return EndTryCommand(BeginTryCommand(context, command, timeout, null, null));
    }

    private object DeserializeObject(NetDataContractSerializer serializer, XmlElement xmlElement)
    {
      using (var memoryStream = new MemoryStream())
      {
        using (var writer = XmlDictionaryWriter.CreateTextWriter(memoryStream))
        {
          xmlElement.WriteContentTo(writer);

          writer.Flush();

          memoryStream.Position = 0;

          return serializer.Deserialize(memoryStream);
        }
      }
    }

    private IDictionary<XName, InstanceValue> LoadWorkflow(Guid id)
    {
      try
      {
        var workflow = WorkflowDAO.GetWorkflow(id.ToString());

        if (workflow == null)
        {
          throw new WorkflowInstanceStoreException("The workflow does not exist");
        }

        if (workflow.Data == null)
        {
          throw new WorkflowInstanceStoreException("The workflow data could not be loaded");
        }

        IDictionary<XName, InstanceValue> data = new Dictionary<XName, InstanceValue>();

        using (var memoryStream = new MemoryStream(workflow.Data.ToArray()))
        {
          var serializer = new NetDataContractSerializer();

          var xmlReader = XmlReader.Create(memoryStream);

          var xmlDocument = new XmlDocument();

          xmlDocument.Load(xmlReader);

          var instances = xmlDocument.GetElementsByTagName("InstanceValue");

          foreach (XmlElement instanceElement in instances)
          {
            var keyElement = (XmlElement) instanceElement.SelectSingleNode("descendant::Key");

            var key = (XName) DeserializeObject(serializer, keyElement);

            var valueElement = (XmlElement) instanceElement.SelectSingleNode("descendant::Value");

            var value = DeserializeObject(serializer, valueElement);

            data.Add(key, new InstanceValue(value));
          }
        }

        return data;
      }
      catch (Exception ex)
      {
        Logger.Error(string.Format("Failed to load the workflow ({0}): {1}", id, ex.Message), ex);

        throw new WorkflowInstanceStoreException(
          string.Format("Failed to load the workflow ({0}): {1}", id, ex.Message), ex);
      }
    }

    private void SaveWorkflow(Guid id, IDictionary<XName, InstanceValue> instanceData)
    {
      try
      {
        // Retrieve any pending timer value
        DateTime? pendingTimer = null;

        InstanceValue timerExpirationTimeValue;

        if (instanceData.TryGetValue(TimerExpirationTimeInstanceValueName, out timerExpirationTimeValue))
        {
          pendingTimer = ((DateTime) timerExpirationTimeValue.Value).ToLocalTime();
        }

        // Retrieve the status value
        string status = null;

        InstanceValue statusValue;

        if (instanceData.TryGetValue(StatusInstanceValueName, out statusValue))
        {
          status = (string) statusValue.Value;
        }

        Logger.Debug("Saving the workflow ({0}) with status ({1}) and pending timer ({2})", id, status, pendingTimer);

        // Serialize the workflow instance data to XML bytes using the UTF-8 encoding
        var xmlDocument = new XmlDocument();

        xmlDocument.InsertBefore(xmlDocument.CreateXmlDeclaration("1.0", "utf-8", null), xmlDocument.DocumentElement);

        xmlDocument.AppendChild(xmlDocument.CreateElement("InstanceData"));

        foreach (var keyValuePair in instanceData)
        {
          var instanceValueElement = xmlDocument.CreateElement("InstanceValue");

          instanceValueElement.AppendChild(SerializeObject("Key", keyValuePair.Key, xmlDocument));
          instanceValueElement.AppendChild(SerializeObject("Value", keyValuePair.Value.Value, xmlDocument));

          Debug.Assert(xmlDocument.DocumentElement != null, "xmlDocument.DocumentElement != null");
          xmlDocument.DocumentElement.AppendChild(instanceValueElement);
        }

        //xmlDocument.Save(string.Format("C:\\Temp\\Workflows\\{0}.xml", id.ToString()));

        byte[] xmlData;

        using (var memoryStream = new MemoryStream())
        {
          xmlDocument.Save(memoryStream);

          xmlData = memoryStream.ToArray();
        }

        // Update the workflow in the database
        if ((status != null) && (status.Equals("Idle")))
        {
          WorkflowDAO.UpdateWorkflow(id.ToString(), WorkflowStatus.Idle, xmlData, pendingTimer, null);
        }
        else
        {
          WorkflowDAO.UpdateWorkflowData(id.ToString(), xmlData);
        }
      }
      catch (Exception ex)
      {
        Logger.Error(string.Format("Failed to save the workflow ({0}): {1}", id, ex.Message), ex);

        throw new WorkflowInstanceStoreException(
          string.Format("Failed to save the workflow ({0}): {1}", id, ex.Message), ex);
      }
    }

    private XmlElement SerializeObject(string elementName, object o, XmlDocument xmlDocument)
    {
      var serializer = new NetDataContractSerializer();

      var xmlElement = xmlDocument.CreateElement(elementName);

      using (var memoryStream = new MemoryStream())
      {
        serializer.Serialize(memoryStream, o);

        memoryStream.Position = 0;

        using (var reader = new StreamReader(memoryStream))
        {
          xmlElement.InnerXml = reader.ReadToEnd();

          return xmlElement;
        }
      }
    }
  }


  internal abstract class AsyncResult : IAsyncResult
  {
    private static AsyncCallback _asyncCompletionWrapperCallback;
    private readonly AsyncCallback _callback;
    private readonly object _state;
    private readonly object _thisLock;
    private bool _endCalled;
    private Exception _exception;
    private volatile ManualResetEvent _manualResetEvent;
    private AsyncCompletion _nextAsyncCompletion;

    protected AsyncResult(AsyncCallback callback, object state)
    {
      _callback = callback;
      _state = state;
      _thisLock = new object();
    }

    public bool HasCallback
    {
      get { return _callback != null; }
    }

    private object ThisLock
    {
      get { return _thisLock; }
    }

    public object AsyncState
    {
      get { return _state; }
    }

    public WaitHandle AsyncWaitHandle
    {
      get
      {
        if (_manualResetEvent == null)
        {
          lock (ThisLock)
          {
            if (_manualResetEvent == null)
            {
              _manualResetEvent = new ManualResetEvent(IsCompleted);
            }
          }
        }

        return _manualResetEvent;
      }
    }

    public bool CompletedSynchronously { get; private set; }
    public bool IsCompleted { get; private set; }

    public static bool IsFatal(Exception exception)
    {
      while (exception != null)
      {
        if ((exception is OutOfMemoryException && !(exception is InsufficientMemoryException)) ||
            exception is ThreadAbortException || exception is AccessViolationException || exception is SEHException)
        {
          return true;
        }

        // These exceptions aren't themselves fatal, but since the CLR uses them to wrap other exceptions,
        // we want to check to see whether they've been used to wrap a fatal exception.  If so, then they
        // count as fatal.
        if (exception is TypeInitializationException || exception is TargetInvocationException)
        {
          exception = exception.InnerException;
        }
        else
        {
          break;
        }
      }

      return false;
    }

    protected void Complete(bool completedSynchronously)
    {
      if (IsCompleted)
      {
        // It's a bug to call Complete twice.
        throw new InvalidProgramException();
      }

      CompletedSynchronously = completedSynchronously;

      if (completedSynchronously)
      {
        // If we completedSynchronously, then there's no chance that the manualResetEvent was created so
        // we don't need to worry about a race
        IsCompleted = true;
      }
      else
      {
        lock (ThisLock)
        {
          IsCompleted = true;
          if (_manualResetEvent != null)
          {
            _manualResetEvent.Set();
          }
        }
      }

      if (_callback != null)
      {
        try
        {
          _callback(this);
        }
        catch (Exception e)
        {
          throw new InvalidProgramException("Async callback threw an Exception", e);
        }
      }
    }

    protected void Complete(bool completedSynchronously, Exception exception)
    {
      _exception = exception;
      Complete(completedSynchronously);
    }

    protected static TAsyncResult End<TAsyncResult>(IAsyncResult result) where TAsyncResult : AsyncResult
    {
      if (result == null)
      {
        throw new ArgumentNullException("result");
      }

      var asyncResult = result as TAsyncResult;

      if (asyncResult == null)
      {
        throw new ArgumentException("Invalid AsyncResult", "result");
      }

      if (asyncResult._endCalled)
      {
        throw new InvalidOperationException("AsyncResult already ended");
      }

      asyncResult._endCalled = true;

      if (!asyncResult.IsCompleted)
      {
        asyncResult.AsyncWaitHandle.WaitOne();
      }

      if (asyncResult._manualResetEvent != null)
      {
        asyncResult._manualResetEvent.Close();
      }

      if (asyncResult._exception != null)
      {
        throw asyncResult._exception;
      }

      return asyncResult;
    }

    protected AsyncCallback PrepareAsyncCompletion(AsyncCompletion callback)
    {
      _nextAsyncCompletion = callback;
      if (_asyncCompletionWrapperCallback == null)
      {
        _asyncCompletionWrapperCallback = AsyncCompletionWrapperCallback;
      }
      return _asyncCompletionWrapperCallback;
    }

    private static void AsyncCompletionWrapperCallback(IAsyncResult result)
    {
      if (result.CompletedSynchronously)
      {
        return;
      }

      var thisPtr = (AsyncResult) result.AsyncState;
      var callback = thisPtr.GetNextCompletion();

      bool completeSelf;
      Exception completionException = null;
      try
      {
        completeSelf = callback(result);
      }
      catch (Exception e)
      {
        if (IsFatal(e))
        {
          throw;
        }
        completeSelf = true;
        completionException = e;
      }

      if (completeSelf)
      {
        thisPtr.Complete(false, completionException);
      }
    }

    private AsyncCompletion GetNextCompletion()
    {
      var result = _nextAsyncCompletion;
      _nextAsyncCompletion = null;
      return result;
    }

    // can be utilized by subclasses to write core completion code for both the sync and async paths
    // in one location, signaling chainable synchronous completion with the boolean result,
    // and leveraging PrepareAsyncCompletion for conversion to an AsyncCallback.
    // NOTE: requires that "this" is passed in as the state object to the asynchronous sub-call being used with a completion routine.
    protected delegate bool AsyncCompletion(IAsyncResult result);
  }


  //An AsyncResult that completes as soon as it is instantiated.
  internal class CompletedAsyncResult : AsyncResult
  {
    public CompletedAsyncResult(AsyncCallback callback, object state) : base(callback, state)
    {
      Complete(true);
    }

    public static void End(IAsyncResult result)
    {
      End<CompletedAsyncResult>(result);
    }
  }

  internal class CompletedAsyncResult<T> : AsyncResult
  {
    private readonly T _data;

    public CompletedAsyncResult(T data, AsyncCallback callback, object state) : base(callback, state)
    {
      _data = data;
      Complete(true);
    }

    public static T End(IAsyncResult result)
    {
      var completedResult = End<CompletedAsyncResult<T>>(result);
      return completedResult._data;
    }
  }
}