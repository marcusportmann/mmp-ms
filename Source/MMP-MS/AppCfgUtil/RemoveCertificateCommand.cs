﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Security.Cryptography.X509Certificates;
using ManyConsole;

namespace MMP.AppCfgUtil
{
  /// <summary>
  ///   The <b>RemoveCertificateCommand</b> class implements the ManyConsole command-line argument parser
  ///   (ConsoleCommand) for the <b>removeCertificate</b> command.
  /// </summary>
  public class RemoveCertificateCommand : ConsoleCommand
  {
    private string _certificate;
    private bool _force;
    private string _storeLocationName;
    private string _storeName;

    public RemoveCertificateCommand()
    {
      IsCommand("removeCertificate", "Remove the certificate from the X.509 certificate store");

      HasOption("force", "Force the operation", b => _force = true);

      HasRequiredOption("location=",
        "The location of the store to add the certificate to i.e. LocalMachine or CurrentUser",
        s => _storeLocationName = s);
      HasRequiredOption("store=",
        "The name of the store to add the certificate to i.e. Root, TrustedPeople, TrustedPublishers or Personal",
        s => _storeName = s);
      HasRequiredOption("certificate=", "The subject or thumbprint for the certificate", s => _certificate = s);
    }

    public override int Run(string[] remainingArguments)
    {
      StoreLocation storeLocation;

      if (_storeLocationName == "LocalMachine")
      {
        storeLocation = StoreLocation.LocalMachine;
      }
      else if (_storeLocationName == "CurrentUser")
      {
        storeLocation = StoreLocation.CurrentUser;
      }
      else
      {
        Console.WriteLine("ERROR: The value '{0}' is not a valid X.509 certificate store location.",
          _storeLocationName);
        return -1;
      }

      StoreName store;

      if (_storeName == "TrustedPeople")
      {
        store = StoreName.TrustedPeople;
      }
      else if (_storeName == "TrustedPublishers")
      {
        store = StoreName.TrustedPublisher;
      }
      else if (_storeName == "Personal")
      {
        store = StoreName.My;
      }
      else if (_storeName == "Root")
      {
        store = StoreName.Root;
      }
      else
      {
        Console.WriteLine("ERROR: The value '{0}' is not a valid name of an X.509 certificate store.", _storeName);
        return -1;
      }

      try
      {
        if (RemoveCertificate(store, storeLocation, _certificate, _force))
        {
          Console.WriteLine("Successfully removed the certificate \"{0}\" from the store {1}\r\n", _certificate,
            _storeName);
        }
        else
        {
          Console.WriteLine("ERROR: Failed to find the certificate \"{0}\" in the store {1}\r\n", _certificate,
            _storeName);

          return -1;
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("ERROR: Failed to remove the certificate from the store \"{0}\"\r\n", _storeName);
        Console.WriteLine("   {0}\r\n", ex.Message);
        Console.WriteLine("{0}", ex.StackTrace);
        return -1;
      }

      return 0;
    }

    private bool RemoveCertificate(StoreName storeName, StoreLocation storeLocation, string certificate, bool force)
    {
      var store = new X509Store(storeName, storeLocation);
      try
      {
        store.Open(OpenFlags.ReadWrite);

        foreach (var existingCertificate in store.Certificates)
        {
          if ((existingCertificate.Thumbprint == certificate) || (existingCertificate.SubjectName.Name == certificate))
          {
            store.Remove(existingCertificate);
            return true;
          }
        }
      }
      finally
      {
        store.Close();
      }

      return force;
    }
  }
}