﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using ManyConsole;

namespace MMP.AppCfgUtil
{
  internal class Program
  {
    private const string VERSION = "1.0.0";

    private static IEnumerable<ConsoleCommand> GetCommands()
    {
      return ConsoleCommandDispatcher.FindCommandsInSameAssemblyAs(typeof (Program));
    }

    private static int Main(string[] args)
    {
      Console.WriteLine("Application Configuration Utility. Version " + VERSION);
      Console.WriteLine("Copyright (c) Marcus Portmann 2015.  All rights reserved.");

      // Locate any commands in the assembly (or use an IoC container, or whatever source)
      var commands = GetCommands();

      // Then run them.
      return ConsoleCommandDispatcher.DispatchCommand(commands, args, Console.Out);
    }
  }
}