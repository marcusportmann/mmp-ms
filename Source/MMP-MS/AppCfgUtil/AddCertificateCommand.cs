﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using ManyConsole;

namespace MMP.AppCfgUtil
{
  /// <summary>
  ///   The <b>AddCertificateCommand</b> class implements the ManyConsole command-line argument parser
  ///   (ConsoleCommand) for the <b>addCertificate</b> command.
  /// </summary>
  public class AddCertificateCommand : ConsoleCommand
  {
    private bool _force;
    private string _keystorePassword;
    private string _keystorePath;
    private string _storeLocationName;
    private string _storeName;

    public AddCertificateCommand()
    {
      IsCommand("addCertificate", "Add or replace the certificate in the X.509 certificate store");

      HasOption("force", "Force the operation", b => _force = true);

      HasRequiredOption("location=",
        "The location of the store to add the certificate to i.e. LocalMachine or CurrentUser",
        s => _storeLocationName = s);
      HasRequiredOption("store=",
        "The name of the store to add the certificate to i.e. Root, TrustedPeople, TrustedPublishers or Personal",
        s => _storeName = s);
      HasRequiredOption("keystore=", "The path to the PKCS12 keystore containing the certificate",
        s => _keystorePath = s);
      HasRequiredOption("storepass=", "The password for PKCS12 keystore containing the certificate",
        s => _keystorePassword = s);
    }

    public override int Run(string[] remainingArguments)
    {
      StoreLocation storeLocation;

      if (_storeLocationName == "LocalMachine")
      {
        storeLocation = StoreLocation.LocalMachine;
      }
      else if (_storeLocationName == "CurrentUser")
      {
        storeLocation = StoreLocation.CurrentUser;
      }
      else
      {
        Console.WriteLine("ERROR: The value '{0}' is not a valid X.509 certificate store location.",
          _storeLocationName);
        return -1;
      }

      StoreName store;

      if (_storeName == "TrustedPeople")
      {
        store = StoreName.TrustedPeople;
      }
      else if (_storeName == "TrustedPublishers")
      {
        store = StoreName.TrustedPublisher;
      }
      else if (_storeName == "Personal")
      {
        store = StoreName.My;
      }
      else if (_storeName == "Root")
      {
        store = StoreName.Root;
      }
      else
      {
        Console.WriteLine("ERROR: The value '{0}' is not a valid name of an X.509 certificate store.", _storeName);
        return -1;
      }

      if (!File.Exists(_keystorePath))
      {
        Console.WriteLine("ERROR: The specified PKCS12 keystore file does not exist. '{0}'", _keystorePath);
        return -1;
      }

      try
      {
        AddCertificate(store, storeLocation, _keystorePath, _keystorePassword, _force);
      }
      catch (Exception ex)
      {
        Console.WriteLine("ERROR: Failed to add the certificate in the PKCS12 keystore to the store {0}\r\n",
          _storeName);
        Console.WriteLine("   {0}\r\n", ex.Message);
        Console.WriteLine("{0}", ex.StackTrace);
        return -1;
      }

      return 0;
    }

    private void AddCertificate(X509Store store, string filename, string password, bool force)
    {
      var certificate = LoadFromFile(filename, password, false);

      if (store.Certificates.Contains(certificate))
      {
        if (force)
        {
          Console.WriteLine("Removing existing certificate: '{0}'", certificate.Subject);
          store.Remove(certificate);
        }
        else
        {
          Console.WriteLine("Certificate '{0}' already exists in '{1}'", certificate.Subject, store.Name);
          return;
        }
      }

      certificate = LoadFromFile(filename, password, true);
      Console.WriteLine("Adding certificate: ");
      Console.WriteLine("    Subject: {0}", certificate.Subject);
      Console.WriteLine("    FriendlyName: {0}", certificate.FriendlyName);
      Console.WriteLine("    Thumbprint: {0}",
        certificate.Thumbprint?.Replace(" ", "").ToUpper().Trim() ?? "N/A");

      Console.WriteLine();

      store.Add(certificate);
    }

    private void AddCertificate(StoreName storeName, StoreLocation storeLocation, string filename, string password,
      bool force)
    {
      var store = new X509Store(storeName, storeLocation);
      try
      {
        store.Open(OpenFlags.ReadWrite);
        AddCertificate(store, filename, password, force);
      }
      finally
      {
        store.Close();
      }
    }

    private X509Certificate2 LoadFromFile(string filename, string password, bool persistKey)
    {
      var result = new X509Certificate2();

      if (string.IsNullOrEmpty(password))
      {
        result.Import(filename);
      }
      else
      {
        var flags = X509KeyStorageFlags.DefaultKeySet;
        if (persistKey)
        {
          flags = X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet;
        }

        result.Import(filename, password, flags);

        if ((persistKey) && (result.HasPrivateKey))
        {
          var privateKey = result.PrivateKey as RSACryptoServiceProvider;
          if (privateKey != null)
          {
            var uniqueKeyContainerName = privateKey.CspKeyContainerInfo.UniqueKeyContainerName;

            var dir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
              @"Microsoft\Crypto\RSA\MachineKeys\");

            if (!Directory.Exists(dir))
            {
              Directory.CreateDirectory(dir);
            }

            var file = Path.Combine(dir, uniqueKeyContainerName);

            if (File.Exists(file))
            {
              var fileSec = File.GetAccessControl(file);

              var faRule = new FileSystemAccessRule("Everyone", FileSystemRights.Read, InheritanceFlags.None,
                PropagationFlags.None, AccessControlType.Allow);

              fileSec.AddAccessRule(faRule);

              File.SetAccessControl(file, fileSec);
            }
          }
        }
      }

      return result;
    }
  }
}