﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Security.AccessControl;
using ManyConsole;
using Microsoft.Win32;

namespace MMP.AppCfgUtil
{
  /// <summary>
  ///   The <b>RegisterServiceCommand</b> class implements the ManyConsole command-line argument parser
  ///   (ConsoleCommand) for the <b>registerService</b> command.
  /// </summary>
  public class RegisterServiceCommand : ConsoleCommand
  {
    private string _applicationName;
    private bool _enableCompression;
    private bool _enableKeepAlive;
    private bool _enableStreaming;
    private string _providerName;
    private string _serviceCertificate;
    private string _serviceName;

    public RegisterServiceCommand()
    {
      IsCommand("registerService", "Register the service");

      HasRequiredOption("providerName=", "The name of the application provider", s => _providerName = s);
      HasRequiredOption("applicationName=", "The name of the application", s => _applicationName = s);
      HasRequiredOption("serviceName=", "The name of the service", s => _serviceName = s);
      HasRequiredOption("serviceCertificate=",
        "The subject or certificate thumbprint identifying the services's" +
        " certificate in the TrustedPeople X.509 certificate store", s => _serviceCertificate = s);
      HasOption("enableCompression=", "Enable compression for the service", b => _enableCompression = false);
      HasOption("enableKeepAlive=", "Enable keep-alives for the service", b => _enableKeepAlive = false);
      HasOption("enableStreaming=", "Enable streaming for the service", b => _enableStreaming = false);
    }

    public override int Run(string[] remainingArguments)
    {
      try
      {
        CreateServiceRegistryConfiguration(_providerName, _applicationName, _serviceName, _serviceCertificate,
          _enableCompression, _enableKeepAlive, _enableStreaming);
      }
      catch (Exception ex)
      {
        Console.WriteLine("ERROR: Failed to register the service {0} for the application {1}\r\n", _serviceName,
          _applicationName);
        Console.WriteLine("   {0}\r\n", ex.Message);
        Console.WriteLine("{0}", ex.StackTrace);
        return -1;
      }
      return 0;
    }

    private void CreateRegistryKeyIfRequired(RegistryKey parentKey, string name)
    {
      // Check that the registry subkey exists if not create it
      using (var key = parentKey.OpenSubKey(name))
      {
        if (key == null)
        {
          using (var subKey = parentKey.CreateSubKey(name))
          {
            if (subKey != null)
            {
              var rs = subKey.GetAccessControl();

              rs.AddAccessRule(new RegistryAccessRule(@"BUILTIN\IIS_IUSRS", RegistryRights.FullControl,
                AccessControlType.Allow));

              subKey.SetAccessControl(rs);
            }
          }
        }
      }
    }

    private void CreateServiceRegistryConfiguration(string providerName, string applicationName, string serviceName,
      string serviceCertificate, bool enableCompression, bool enableKeepAlive, bool enableStreaming)
    {
      CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\{providerName}");
      CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\{providerName}\{applicationName}");
      CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\{providerName}\{applicationName}\Services");
      CreateRegistryKeyIfRequired(Registry.LocalMachine,
        $@"SOFTWARE\{providerName}\{applicationName}\Services\{serviceName}");

      using (
        var serviceRegistryKey =
          Registry.LocalMachine.OpenSubKey($@"SOFTWARE\{providerName}\{applicationName}\Services\{serviceName}", true)
        )
      {
        if (serviceRegistryKey != null)
        {
          serviceRegistryKey.SetValue("ServiceCertificate", serviceCertificate);
          serviceRegistryKey.SetValue("EnableCompression", enableCompression ? 1 : 0, RegistryValueKind.DWord);
          serviceRegistryKey.SetValue("EnableKeepAlive", enableKeepAlive ? 1 : 0, RegistryValueKind.DWord);
          serviceRegistryKey.SetValue("EnableStreaming", enableStreaming ? 1 : 0, RegistryValueKind.DWord);

          Console.WriteLine();

          Console.WriteLine(
            @"Successfully initialized the registry entries for the service {0} for the application {1} under the" +
            @" registry key SOFTWARE\{2}\{3}\Services\{4}", serviceName, applicationName, providerName,
            applicationName, serviceName);

          Console.WriteLine("    ServiceCertificate: {0}", serviceCertificate);
          Console.WriteLine("    EnableCompression: {0}", enableCompression ? 1 : 0);
          Console.WriteLine("    EnableKeepAlive: {0}", enableKeepAlive ? 1 : 0);
          Console.WriteLine("    EnableStreaming: {0}", enableStreaming ? 1 : 0);
        }
      }

      using (var wow6432NodeKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node"))
      {
        if (wow6432NodeKey != null)
        {
          CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\Wow6432Node\{providerName}");
          CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\Wow6432Node\{providerName}\{applicationName}");
          CreateRegistryKeyIfRequired(Registry.LocalMachine,
            $@"SOFTWARE\Wow6432Node\{providerName}\{applicationName}\Services");
          CreateRegistryKeyIfRequired(Registry.LocalMachine,
            $@"SOFTWARE\Wow6432Node\{providerName}\{applicationName}\Services\{serviceName}");

          using (
            var serviceRegistryKey =
              Registry.LocalMachine.OpenSubKey(
                $@"SOFTWARE\Wow6432Node\{providerName}\{applicationName}\Services\{serviceName}", true))
          {
            if (serviceRegistryKey != null)
            {
              serviceRegistryKey.SetValue("ServiceCertificate", serviceCertificate);
              serviceRegistryKey.SetValue("EnableCompression", enableCompression ? 1 : 0, RegistryValueKind.DWord);
              serviceRegistryKey.SetValue("EnableKeepAlive", enableKeepAlive ? 1 : 0, RegistryValueKind.DWord);
              serviceRegistryKey.SetValue("EnableStreaming", enableStreaming ? 1 : 0, RegistryValueKind.DWord);

              Console.WriteLine();

              Console.WriteLine(
                @"Successfully initialized the registry entries for the service {0} for the application {1} under the" +
                @" registry key SOFTWARE\{2}\{3}\Services\{4}", serviceName, applicationName, providerName,
                applicationName, serviceName);

              Console.WriteLine("    ServiceCertificate: {0}", serviceCertificate);
              Console.WriteLine("    EnableCompression: {0}", enableCompression ? 1 : 0);
              Console.WriteLine("    EnableKeepAlive: {0}", enableKeepAlive ? 1 : 0);
              Console.WriteLine("    EnableStreaming: {0}", enableStreaming ? 1 : 0);
            }
          }
        }
      }
    }
  }
}