﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Security.Cryptography.X509Certificates;
using ManyConsole;

namespace MMP.AppCfgUtil
{
  /// <summary>
  ///   The <b>ListCertificatesCommand</b> class implements the ManyConsole command-line argument parser
  ///   (ConsoleCommand) for the <b>listCertificates</b> command.
  /// </summary>
  public class ListCertificatesCommand : ConsoleCommand
  {
    private string _storeLocationName;
    private string _storeName;

    public ListCertificatesCommand()
    {
      IsCommand("listCertificates", "List the X.509 certificates in the store");

      HasRequiredOption("location=",
        "The location of the store to add the certificate to i.e. LocalMachine or CurrentUser",
        s => _storeLocationName = s);
      HasRequiredOption("store=",
        "The name of the store to add the certificate to i.e. Root, TrustedPeople, TrustedPublishers or Personal",
        s => _storeName = s);
    }

    public override int Run(string[] remainingArguments)
    {
      StoreLocation storeLocation;

      if (_storeLocationName == "LocalMachine")
      {
        storeLocation = StoreLocation.LocalMachine;
      }
      else if (_storeLocationName == "CurrentUser")
      {
        storeLocation = StoreLocation.CurrentUser;
      }
      else
      {
        Console.WriteLine("ERROR: The value '{0}' is not a valid X.509 certificate store location.",
          _storeLocationName);
        return -1;
      }

      StoreName store;

      if (_storeName == "TrustedPeople")
      {
        store = StoreName.TrustedPeople;
      }
      else if (_storeName == "TrustedPublishers")
      {
        store = StoreName.TrustedPublisher;
      }
      else if (_storeName == "Personal")
      {
        store = StoreName.My;
      }
      else if (_storeName == "Root")
      {
        store = StoreName.Root;
      }
      else
      {
        Console.WriteLine("ERROR: The value '{0}' is not a valid name of an X.509 certificate store.", _storeName);
        return -1;
      }


      try
      {
        ListCertificates(store, storeLocation);
      }
      catch (Exception ex)
      {
        Console.WriteLine("ERROR: Failed to list the certificates in the store {0}\r\n", _storeName);
        Console.WriteLine("   {0}\r\n", ex.Message);
        Console.WriteLine("{0}", ex.StackTrace);
        return -1;
      }

      return 0;
    }

    private void ListCertificates(StoreName storeName, StoreLocation storeLocation)
    {
      var store = new X509Store(storeName, storeLocation);
      try
      {
        store.Open(OpenFlags.ReadWrite);

        foreach (var existingCertificate in store.Certificates)
        {
          Console.WriteLine("Found certificate \"{0}\" with thumbprint \"{1}\"", existingCertificate.SubjectName.Name,
            existingCertificate.Thumbprint);
        }
      }
      finally
      {
        store.Close();
      }
    }
  }
}