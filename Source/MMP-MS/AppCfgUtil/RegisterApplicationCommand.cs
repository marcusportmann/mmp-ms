﻿/*
 * Copyright 2015 Marcus Portmann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Security.AccessControl;
using ManyConsole;
using Microsoft.Win32;

namespace MMP.AppCfgUtil
{
  /// <summary>
  ///   The <b>RegisterApplicationCommand</b> class implements the ManyConsole command-line argument
  ///   parser (ConsoleCommand) for the <b>registerApplication</b> command.
  /// </summary>
  public class RegisterApplicationCommand : ConsoleCommand
  {
    private string _applicationName;
    private string _certificate;
    private string _connectionString;
    private string _providerName;

    public RegisterApplicationCommand()
    {
      IsCommand("registerApplication", "Register the application");

      HasRequiredOption("providerName=", "The name of the application provider", s => _providerName = s);
      HasRequiredOption("applicationName=", "The name of the application", s => _applicationName = s);
      HasRequiredOption("certificate=",
        "The subject or the certificate thumbprint identifying the application's" +
        " certificate in the TrustedPeople X.509 certificate store", s => _certificate = s);
      HasRequiredOption("connectionString=", "The connection string used to connect to the application's database",
        s => _connectionString = s);
    }

    public override int Run(string[] remainingArguments)
    {
      try
      {
        CreateApplicationRegistryConfiguration(_providerName, _applicationName, _certificate, _connectionString);
      }
      catch (Exception ex)
      {
        Console.WriteLine("ERROR: Failed to register the application {0}\r\n", _applicationName);
        Console.WriteLine("   {0}\r\n", ex.Message);
        Console.WriteLine("{0}", ex.StackTrace);
        return -1;
      }
      return 0;
    }

    private void CreateApplicationRegistryConfiguration(string providerName, string applicationName,
      string certificate, string connectionString)
    {
      CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\{providerName}");
      CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\{providerName}\{applicationName}");

      using (
        var applicationRegistryKey = Registry.LocalMachine.OpenSubKey($@"SOFTWARE\{providerName}\{applicationName}",
          true))
      {
        if (applicationRegistryKey != null)
        {
          applicationRegistryKey.SetValue("ApplicationCertificate", certificate);
          applicationRegistryKey.SetValue("ApplicationConnectionString", connectionString);

          Console.WriteLine();

          Console.WriteLine(
            @"Successfully initialized the registry entries for the application {0} under the" +
            @" registry key SOFTWARE\{1}\{2}", applicationName, providerName, applicationName);

          Console.WriteLine("    ApplicationCertificate: {0}", certificate);
          Console.WriteLine("    ApplicationConnectionString: {0}", connectionString);
        }
      }

      using (var wow6432NodeKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node"))
      {
        if (wow6432NodeKey != null)
        {
          CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\Wow6432Node\{providerName}");
          CreateRegistryKeyIfRequired(Registry.LocalMachine, $@"SOFTWARE\Wow6432Node\{providerName}\{applicationName}");

          using (
            var applicationRegistryKey =
              Registry.LocalMachine.OpenSubKey($@"SOFTWARE\Wow6432Node\{providerName}\{applicationName}", true))
          {
            if (applicationRegistryKey != null)
            {
              applicationRegistryKey.SetValue("ApplicationCertificate", certificate);
              applicationRegistryKey.SetValue("ApplicationConnectionString", connectionString);

              Console.WriteLine();

              Console.WriteLine(
                @"Successfully initialized the registry entries for the application {0} under the" +
                @" registry key SOFTWARE\Wow6432Node\{1}\{2}", applicationName, providerName, applicationName);

              Console.WriteLine("    ApplicationCertificate: {0}", certificate);
              Console.WriteLine("    ApplicationConnectionString: {0}", connectionString);
            }
          }
        }
      }
    }

    private void CreateRegistryKeyIfRequired(RegistryKey parentKey, string name)
    {
      // Check that the registry subkey exists if not create it
      using (var key = parentKey.OpenSubKey(name))
      {
        if (key == null)
        {
          using (var subKey = parentKey.CreateSubKey(name))
          {
            if (subKey != null)
            {
              var rs = subKey.GetAccessControl();

              rs.AddAccessRule(new RegistryAccessRule(@"BUILTIN\IIS_IUSRS", RegistryRights.FullControl,
                AccessControlType.Allow));

              subKey.SetAccessControl(rs);
            }
          }
        }
      }
    }
  }
}