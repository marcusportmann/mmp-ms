set MMP_MS_HOME=..

%MMP_MS_HOME%\Build\Debug\AppCfgUtil removeCertificate --location=LocalMachine --store=TrustedPeople --certificate "CN=Sample, OU=Applications, O=MMP"

%MMP_MS_HOME%\Build\Debug\AppCfgUtil removeCertificate --location=LocalMachine --store=Root --certificate "CN=MMP Root Certificate Authority (1), O=MMP"



%MMP_MS_HOME%\Build\Debug\AppCfgUtil addCertificate --force --location=LocalMachine --store=TrustedPeople --keystore=Sample.p12 --storepass Password1

%MMP_MS_HOME%\Build\Debug\AppCfgUtil addCertificate --force --location=LocalMachine --store=Root --keystore=%MMP_MS_HOME%\Deployment\MMP-CA-Public.p12 --storepass MMP



%MMP_MS_HOME%\Build\Debug\AppCfgUtil registerApplication --providerName="MMP" --applicationName="Sample" --certificate="CN=Sample, OU=Applications, O=MMP" --connectionString="Data Source=(local);Initial Catalog=Sample;Integrated Security=True"




 